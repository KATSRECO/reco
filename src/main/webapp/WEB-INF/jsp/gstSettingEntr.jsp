<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Push Off</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/pushoff.css">
<link rel="stylesheet" href="css/main.css">

</head>
<body>
    <div style="width:100%;height:100%">
  <header>
      <div style="height:100%;width:8%;float:left;">
      <img src="Icons\Item-Master-2.png" style="width: 60px;position: relative;left: 20%;">
      </div>
    <div class="left" style="padding-top:6px;position:relative;left: -1%">
     <button id="menu" title="Menu"><i class="fa fa-bars fa-2x" style="color:black;font-size:2em; "></i></button>
    </div>
    <div class="left" style="height:100%;width:12%;">
           <button style="width:60%;height:50%;border: 1px solid black;margin-top:10%;border-radius:4px;display: none">
             <i class="fa fa-plus" aria-hidden="true" style="color:black;float:left;position:relative;left:8%;font-weight:bold"></i>
             <span style="float:left;color:black;position:relative;left:20%;text-align:center;">Add New</span>
           </button>
    </div>

   <div class="controls">
       <label for="search" class="labeclass"><i class="fa fa-search" style="color:black"></i></label>
       <input type="search" id="search" name="search" style="height: 35px;width: 90%;border-radius: 6px;border:1px solid black;" />
    </div>
  </header>
  <!--//Header-->
  
  <div class="content" style="width:100%;height:100%">
      <aside>
       <nav class='site-nav'> 
           <div>
               <div class="ul_1"><div style="" class="ul_1_1"><img src="Icons\home.png" class="width0p"/></div><div style="" class="ul_1_2">Home</div></div>
               <div class="ul_1">
               <div style="" class="ul_1_1"><img src="Icons\dashboard.png" class="width0p"/></div><div class="ul_1_2">Dashboard</div>
               </div>
               
               <div class="ul_1" id="opensubmen1">
                   <div class="submodule" id="submen1">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item Category</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Party</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Party Wise Rate</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\1.png" class="width0p"/></div><div class="ul_1_2">Master</div>
               </div>
               <div class="ul_1" id="opensubmen2">
                   <div class="submodule" id="submen2">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\2.png" class="width0p"/></div><div class="ul_1_2">Purchase</div>
               </div>
               <div class="ul_1" id="opensubmen3">
                   <div class="submodule" id="submen3">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\3.png" class="width0p"/></div><div class="ul_1_2">Sales</div>
               </div>
               <div class="ul_1" id="opensubmen4">
                   <div class="submodule" id="submen4">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Stock in Hand</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Weekly Stock</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item Movement</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\4.png" class="width0p"/></div><div class="ul_1_2">Stock</div>
               </div>
               <div class="ul_1" id="opensubmen5">
                   <div class="submodule" id="submen5">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\5.png" class="width0p"/></div><div class="ul_1_2">GST</div>
               </div>
               <div class="ul_1" id="opensubmen6">
                   <div class="submodule" id="submen6">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Company Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Invoice</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\6.png" class="width0p"/></div><div class="ul_1_2">Setting</div>
               </div>
               <div class="ul_1" id="opensubmen7">
                   <div class="submodule" id="submen7">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Account Group</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Journal Voucher</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Trial Balance</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Profit & Loss</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Balance Sheet</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Ledgers</a></div>    
                       </div>

                   </div>
                   <div class="ul_1_1"><img src="Icons\7.png" class="width0p"/></div><div class="ul_1_2">Account</div>
               </div>
               <div class="ul_1" id="opensubmen8">
                   <div class="submodule" id="submen8">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Create User</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Modify User</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\8.png" class="width0p"/></div><div class="ul_1_2">User</div>
               </div>
               </div>
       </nav>
      <footer> </footer>
      </aside>  
      <!--//sidebar-->
<section style="width:100%;height:100%">     
      <article>
<div style="width:100%;height:100%;">
<div class="substrp"></div>
 </div>
 <jsp:include page="gstSettingEnt.jsp"></jsp:include>
</article>
</section>  
     <!--- <div style="background-color:red;width:100%;height:10%;float:left;padding-top:2%">sdafasd</div>---->
    <div class="site-footer">
     </div>       
    <!--//body content--> 
  </div>
  <!--//Main Content--> 
</div>
    
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script> 
<script>
$(document).ready(function(){
  $("#menu").on("click" , function(){
    $('#submen1').hide();  
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $(this).toggleClass("active");
    $("body").toggleClass("nav-open");   
  });
 });
    $(document).ready(function() {
    $("#opensubmen1").click(function(){    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen1").toggle();});

    $("#opensubmen2").click(function(){
    $('#submen1').hide();    
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen2").toggle();});

    $("#opensubmen3").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen3").toggle();});

    $("#opensubmen4").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen4").toggle();});

    $("#opensubmen5").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen5").toggle();});

    $("#opensubmen6").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen7').hide();
    $('#submen8').hide();        
    $("#submen6").toggle();});

    $("#opensubmen7").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen8').hide();        
    $("#submen7").toggle();});

    $("#opensubmen8").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();        
    $("#submen8").toggle();});
});

</script>

</body>
</html>
