<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>

<html>
<head>
<title>AJAX in java web application using jQuery</title>


<script src="jquery-3.2.1.min (1).js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<script>

	$(document).ready(function(){
		  $('#b1').click(function(){
		           sendData();
		    });
		   });
		function sendData(){
		   var mge = $('#userName').val();
		    alert(mge);
		    $.ajax({
		        type: "POST",
		        url: "helloji",
		        data: { message : mge  }
		      }).done(function( msg ) {
		        alert( "Data Saved: " + msg );
		      });
		}</script>
</head>
<body>
<form name="fo1">
  <fieldset>
    <legend>AJAX implementation in JSP and Servlet using JQuery</legend>
    <br /> Enter your Name: <input type="text" id="userName" name="name"/>
    
 </fieldset>

 <fieldset>
   <legend>Response from jQuery Ajax Request on Blur event</legend>
   <div id="ajaxResponse"></div>
 </fieldset>
</form>
<button id="b1">hello</button>
</body>
