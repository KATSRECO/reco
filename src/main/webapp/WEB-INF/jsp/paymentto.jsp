<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/paymenttype.css"/>
<style>
.paytexdiv{
  width:10%;
  height:100%;
  font-size:14px;
  cursor:pointer;
  color:white !important;
  font-family:calibri;
  font-weight:bold;
  float:left;
  padding-top:1%;
  padding-left:2%;
}
.paytexdiv:hover{
 background-color:#1d1d1d;
}

.paytexdiv_2{
     width: 11%;
    height:44px;
    padding-top: 0.5%;
    font-family:calibri;
    font-weight:bold;
    font-size: 20px;
    float: left;
    text-align: center;
}
.inpuumndiv{
 width:100%;
 height:20%;
 float:left;
 margin-top:1%;
}

.slctdiv{
    width:33%;
    border-style: solid;
    float: left;
    margin-top: -6px;
    border-radius: 4px;
    border-width: 1px;
    margin-left: 2%;
    height: 37px;
}
.inpuumntexdiv{
font-size: 16px;
    float: left;
    font-family: calibri;
    font-weight: bold;
    margin-left: 5%;
    width:8%;
    }
    .inptdiv{
   float: left !important;
    margin-left: 4.4% !important;
    height: 37px !important;
    width: 33% !important;
  border: 1px solid #a3a3a3 !important;
    } 
    
    .button_pay{
    border: none !important;
    width: 6% !important;
    height: 33px !important;
    border-radius: 6px !important;
    color: white !important;
    padding: 5px 23px !important;
    text-align: center !important;
    text-decoration: none !important;
    font-family: calibri !important;
    font-weight: bold !important;
    display: inline-block !important;
    font-size: 17px !important;
    margin: 0.6% 0% 0% 31%; !important
    cursor: pointer !important;  
    }   
</style>
</head>
<body>
<jsp:include page="layout.jsp"></jsp:include>
<div class='substrp'>
<div class='paytexdiv' id="tobutton">Payment To</div>
<div class='paytexdiv' id="typebutton">Payment Type</div>
<div></div>
</div>
<div style="display:none" id="paymento">
<input type="hidden" id="ideditflag" value="<%=Util.utils.intSaveFlag%>">
<div style="display:none" id="addnewpayment">
<div class='paytexdiv_2'>Payment To</div>

<div class='inpuumndiv'>
<span class='inpuumntexdiv'>Party
</span>
<select class='slctdiv' id="partyname"></select>

<span class='inpuumntexdiv' style='margin-left:6%'>Amount
</span>
<input type="text" class='inptdiv' id="amount">
</div>

<div class='inpuumndiv' style='height:32px;margin-top: 1%;'>
<span class='inpuumntexdiv' style='width:9%;margin-top:0.8%'>Mode</span>
<div style='width:40%;float:left'>
<input type="radio" name="radi" id="pro"  value="<%=Util.utils.strCashVoucher%>" style='width:7%'>
<label style='margin-right:3%'>Cash</label>
<input type="radio" name="radi" id="pro1" value="<%=Util.utils.strBankVoucher%>" style='width:7%'>
<label style='margin-right:3%'>Bank</label>
<input type="radio" name="radi" id="pro2" value="<%=Util.utils.strCardVoucher%>" style='width:7%'>
<label style='margin-right:3%'>C&D</label>
</div>
</div>
<div class='inpuumndiv'>
<span class='inpuumntexdiv'>Date
</span>
<input type="text" class='inptdiv' id="date">

<span class='inpuumntexdiv' id="checklabel" style=margin-left:4%>Cheque No
</span>
<input type="text" class='inptdiv' id="chequenum">

</div>


<span style="font-size: x-large;float: left;display: none">Voucher
</span>
<input type="hidden" style="border-style: solid;float: left;border-width: 1px;margin-left: 10%;height: 40px" id="voucher" value="0" >

<div id="viewonly">
<div class='inpuumndiv'>
<span class='inpuumntexdiv'>Select Bank
</span>
<select class= "inptdiv" id="bankname">

</select>
<span class='inpuumntexdiv' style=margin-left:4%>Naration
</span>
<input type="text" class='inptdiv' id="naration">
</div>

<div class='inpuumndiv'>
<span class='inpuumntexdiv' id="checkdate1" >Cheque Date
</span>
<input type="text" class='inptdiv' id="chequedate">

</div>

</div>

<div style='height:240px;width:100%;float:left'></div>
   <div style="width:100%;height:50px;float:left;background-color:#f7f7f7;border:1px solid #adadad"> 
   <button class="button_pay" style="float:right;margin-right:2%;margin-left:0px;background-color:#3845ae" id="submitdatd"><%=Util.utils.strSaveCaption%></button>
   </div>
</div>
<div style="display:block" id="viewpayment">

<table id="tblitemlist">
  <thead>
    <tr>
    <th style="display:none"></th>
      <th style="background-color: white;color: black">Voucher Number</th>
      <th style="background-color: white;color: black">Voucher Date</th>
      <th style="background-color: white;color: black">Party</th>
      <th style="background-color: white;color: black">Mode</th>
    </tr>
    </thead>
    <tbody id="tblitemlistbody">
    
    </tbody>
  </table>
 <div id="custom-menusupnew" class="contmenu" style="display:none;height:8%;;width:6%;z-index:1;background-color: white;  box-shadow: 4px 4px 5px #888888;">
    
    <div style="height: 50%;width: 100%" id="paymentedit">
      Edit
 </div>
 <div style="height: 50%;width: 100%" id="paymentdelete">
      
    Delete
</div></div>

</div></div>
<div style="display:block" id="paymenttype">

<div>




</div>


<div style="display: block;">
 <jsp:include page="payment_type.jsp"></jsp:include>
 
 </div>
 <div style="display: none"><jsp:include page="paymenttypetable.jsp"></jsp:include></div>
 
 
 
 
 </div>
<input type="hidden" id="v_number">

<script type="text/javascript">
var operation="";
var voucherno="";
var itemid=0;
$(document).ready(function() {
	
	
	loaddataintoTb(0);
	
    $("#opensubmen").click(function(){
    $("#submen1").toggle();
        });
});


$.ajax({
    type: "GET",
    url: "banklist",
    data: { 
    	
    },success:function(data){
   
    	var m=$.parseJSON(data); 
    	var $select = $('#bankname');
    	$(m.data).each(function (index, o) {    
    	    var $option = $("<option/>").attr("value", o.BANK_ID).text(o.BANK_BRANCH);
    	    $select.append($option);
    	});
    }
});












$(document).ready(function() {
    $('input[type=radio][name=radi]').change(function() {
        if (this.value == '<%=Util.utils.strCashVoucher%>') {
        	 $("#viewonly").css({'display':'none'});
        }
        else if (this.value == '<%=Util.utils.strBankVoucher%>') {
        	  $('#checklabel').html('Check Number');
              $('#checkdate1').html('Check Date');
        	 $("#viewonly").css({'display':'block'});
        }
        else if (this.value == '<%=Util.utils.strCardVoucher%>') {
      
       $('#checklabel').html('Card Number');
       $('#checkdate1').html('CVV');
       }
    });
});

$("#addnewitemmaster").on("click" , function(){
	 $("#viewpayment").css({'display':'none'});
	  $("#addnewpayment").css({'display':'block'});
	   $("#addnewpayment").css({ 'z-index': 1 });
});


$("#paymentedit").click(function () {
	 $.ajax({
	        type: "GET",
	        url: "editloadname",
	        data: { id : itemid  }
	      }).done(function( msg ) {
	    	  var m =JSON.parse(msg);
	    	  $.each(m.data,function(index,row){
	    		  $("#itemedit").val(row.ITEMGROUP_NAME_VC);
	    	  })
	    	  $('#itemedit').css('border-color','black');
	    	  $('#editgrp').css({'display':'block'});
	          $('#submitcreate').css({'display':'none'});
	          $('#deletedata').css({'display':'none'});
	          $('#submitedit').css({'display':'block'});
	          
	          $("#editgrp").css({ 'z-index': 1 });})
	      });



var strtable="";
$("#substrip").css({'display':'none'});

$.ajax({
    type: "GET",
    url: "fillComboPartyCustomer",
    data: { 
    	
    },success:function(data){
    	
    	var m=$.parseJSON(data); 
    	var $select = $('#partyname');
    	$(m.data).each(function (index, o) {    
    	    var $option = $("<option/>").attr("value", o.SUBGROUP_ID).text(o.COMPANY_NAME_VC);
    	    $select.append($option);
    	});
    }
});
  
$(document).on('click','#tblitemlist tbody td',function(){
	  
	  if($(this).index()==0){
		  var tr=$(this).closest('tr')
  	  $('#v_number').val($(tr).find('td').eq(0).find('a').attr('id'));
  	menusho('custom-menusupnew', $('#v_number').val());
	  }
  })
  function menusho(id, ids) {
  var x = $("#" + ids).position();
  var elm = $('#viewpayment');
  var y = elm.position();
 
  $('#' + id).css({
      position: 'absolute',
      top: x.top + y.top+0,
      left: x.left + y.left + 10
  }).slideToggle('slow');
} 
  
$("#paymentedit").hover(function(){
    $(this).css("background-color", "#e5e5e5");
    }, function(){
    $(this).css("background-color", "white");
});
$("#paymentdelete").hover(function(){
    $(this).css("background-color", "#e5e5e5");
    }, function(){
    $(this).css("background-color", "white");
});
$("#paymentedit").click(function(){
	$('#ideditflag').val('<%=Util.utils.intUpdateFlag%>')
	$('#submitdatd').text('<%=Util.utils.strUpdateCaption%>')
	//var checked = $('[name="radi"]:radio:checked').val();
	  var V_NO_N=voucherno.replace('ab_',"");
	  $.ajax({
		  type:'GET',
		  url:'getPaymentToEdit',
		  data:{
			  V_NO_N:$('#v_number').val()
		  },success:function(data){
			  var getData= $.parseJSON(data); 
			  $.each(getData.data,function(index,row){
				  $(':radio[value='+row.MODE_VC+']').prop('checked',true);
				  $('#partyname').val(row.SUBGROUP_ID_N);
				  $('#date').val(row.V_DATE_D);
				  $('#V_NO_N').val(row.V_NO_N);
				  $('#amount').val(row.AMOUNT_N);
				  $('#bankname').val(row.BANK_ID_N);
				  $('#chequenum').val(row.CHEQUE_NO_VC);
				  $('#chequedate').val(row.CHEQUE_DATE_VC);
				  $('#naration').val(row.NARRATION_VC);
				  $('#addnewitemmaster').trigger('click');
			  })
		  }
	  })
})

$("#paymentdelete").click(function(){
	$('#ideditflag').val('<%=Util.utils.intDeleteFlag%>')
	$('#submitdatd').text('<%=Util.utils.strDeleteCaption%>')
	alert($('#v_number').val());
	  $.ajax({
		  type:'GET',
		  url:'getPaymentToEdit',
		  data:{
			  V_NO_N:$('#v_number').val()
		  },success:function(data){
			  var getData= $.parseJSON(data); 
			  $.each(getData.data,function(index,row){
				  $(':radio[value='+row.MODE_VC+']').prop('checked',true);
				  $('#partyname').val(row.SUBGROUP_ID_N);
				  $('#date').val(row.V_DATE_D);
				  $('#V_NO_N').val(row.V_NO_N);
				  $('#amount').val(row.AMOUNT_N);
				  $('#bankname').val(row.BANK_ID_N);
				  $('#chequenum').val(row.CHEQUE_NO_VC);
				  $('#chequedate').val(row.CHEQUE_DATE_VC);
				  $('#naration').val(row.NARRATION_VC);
				  $('#addnewitemmaster').trigger('click');
			  })
		  }
	  })
	$('#addnewitemmaster').trigger('click');
})
$("#iditemgroupedit").click(function(){
    $(this).css("background-color", "#e5e5e5");
  
});




$('#typebutton').click(function(){
	$('#tobutton').css('background-color','#303030');
	$('#typebutton').css('background-color','black');
	$('#paymento').css('display','none');
	$('#paymenttype').css('display','block');
});
$('#tobutton').click(function(){
	
	$('#typebutton').css('background-color','#303030');
	$('#tobutton').css('background-color','black');
	$('#paymenttype').css('display','none');
	$('#paymento').css('display','block');
});
  
  
$('#submitdatd').click(function(){
	  
	var checked = $('[name="radi"]:radio:checked').val();
	
	 var partyname = numCorrect($('#partyname').val());
	  var voucher =$('#voucher').val();
	  var date =$('#date').val();
	  var amount = numCorrect($('#amount').val());
	  var bankname = numCorrect($('#bankname').val());

	  var checknum = numCorrect($('#chequenum').val());
	  
	  var checkdate =$('#chequedate').val();
	  
	  var naration =$('#naration').val();  
	  if(naration=="")
		  {
		  naration="null";
		  
		  
		  }
			  
	  if($(this).text()=='<%=Util.utils.strSaveCaption%>'){
	  $.ajax({
	        type: "POST",
	        url: "paymentto",
	        data: { 
	        	checked : checked,
	        	partyname : partyname,
	        	voucher : 0,
	        	date : date,
	        	amount : amount,
	        	bankname : bankname,
	        	checknum : checknum,
	        	 checkdate :checkdate,
	        	 naration :naration
	        
	        } ,success:function(data){
	        	alert("successfully Added");
	        	$('#voucher').val("");
	        	$('#date').val("");
	        	$('#amount').val("");
	        	$('#bankname').val("");
	        	$('#chequenum').val("");
	        	$('#chequedate').val("");
	        	$('#naration').val("");
	        }
	});
	  }
	  else if($(this).text()=='<%=Util.utils.strUpdateCaption%>'){
		  $.ajax({
		        type: "POST",
		        url: "paymentedit",
		        data: { 
		        	id:0,
		        	checked : checked,
		        	partyname : partyname,
		        	voucher : $('#v_number').val(),
		        	date : date,
		        	amount : amount,
		        	bankname : bankname,
		        	checknum : checknum,
		        	 checkdate :checkdate,
		        	 naration :naration
		        
		        } ,success:function(data){
		        	alert("successfully Added");
		        	$('#voucher').val("");
		        	$('#date').val("");
		        	$('#amount').val("");
		        	$('#bankname').val("");
		        	$('#chequenum').val("");
		        	$('#chequedate').val("");
		        	$('#naration').val("");
		        }
		});
		  
		  
	  }else if($(this).text()=='<%=Util.utils.strDeleteCaption%>'){
		  alert(voucherno);
		  $.ajax({
		        type: "POST",
		        url: "paymentdelete",
		        data: { 
		        	id:$('#v_number').val()
		     
		        
		        } ,success:function(data){
		        	alert("successfully Added");
		        	$('#voucher').val("");
		        	$('#date').val("");
		        	$('#amount').val("");
		        	$('#bankname').val("");
		        	$('#chequenum').val("");
		        	$('#chequedate').val("");
		        	$('#naration').val("");
		        }
		});
		  
	  }
});
var a=0;
var table;
function loaddataintoTb(val){
	if(val==3){
		a=0;
		
		
	}
	if(val==1){
		
			
		
		
	a+=50;
	}
	
	if(val==2)
		{
		if(a<1){
			return;
		
		}else{
		a-=50;
		}
		}
	$('#tblitemlist').dataTable().fnDestroy();
$(document).ready(function() {
	table=  $('#tblitemlist').DataTable( {
    	"ajax": {
    	    "url": "getdatapayment",
    	   
    	    "data": {
    	        "id": a
    	    }},
    	    
        "columnDefs": [{
        	"targets": [ 0 ],
        	"visible": false,
        	"searchable": true
        	},
        	],
       
        "columns": [
            { "data": "ID" },
            {
                'data': "voucherno",
                'render': function (data, type, row) {
                    return '<a id=' + row.voucherno +'>'+row.voucherno+'</a>'
                }},
            { "data": "voucherdate" },
            { "data": "COMPANY_NAME_VC" },
            { "data": "MODE_VC" }
         
           
        ]
    
    } );
    
} );}




</script>
</body>
</html>