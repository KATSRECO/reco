<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Push Off</title>
<jsp:include page="linkall.jsp"></jsp:include>
<style>

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}
html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 60px;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-bottom: 15px;
  width:30%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}

header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover, header button.active { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #303030;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  padding: 8px;
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  padding: 0px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}
.contmenu {
visibility: visible;
border-bottom: thin;



}
aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #4f4f4f;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media only screen and (max-width: 640px){
    body.nav-open aside{
        margin-top: 8.3%
    }  
}
@media only screen and (min-width: 768px){
    body.nav-open aside{
        margin-top: 7.2%
    }  
}
@media only screen and (min-width: 1024px){
    body.nav-open aside{
        margin-top: 5.1%
    }  
}

@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
    width: 100%;
    height: 40px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}

#tbl thead tr th{
background-color:#303030 !important;
color:white !important;
}
.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-align:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}

.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}

.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
 .submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left: 15.4em;
    top:0.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:60%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}
.dropbtn {
    background-color: #0057a1;
    color: white;
    padding: 6px;
    font-size: 13px;
    border: none;
    cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
    background-color: #4f4f4f;
}

.dropdown {
    float: right;
    position: relative;
    display: inline-block;
    margin-top: -0.7%
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    right: 0;
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #f1f1f1}

.dataTable thead{
background-color: #e4e4e4;
}
.dataTable th{
color: #282828;
}
.dataTable td{
border-bottom:1px solid #000;
}
.dataTable td{
padding-left:10px!important;
}
.show {display:block;}
  .button_1 {
        background-color: #0e1b47;
        border: none;
        color: white;
        height: 28px;
        width: 77px;
        border-radius: 4px;
        text-align: center;
        position: relative;
        display: inline-block;
        font-size: 13px;
        font-family: arial;
        cursor: pointer;
        margin-left: 3px;
    }
   
</style>
</head>
<body>
 
<script type="text/javascript">


</script>
    <div style="width:100%;height:100%">
 
  <!--//Header-->
  
  <div class="content" style="width:100%;height:100%">
  <jsp:include page="layout.jsp"></jsp:include>
      <!--//sidebar-->
 
    <input type="hidden" id="idhiddengroupid">
   
<div id="maindiv">

<div id="editgrp" style="display: none;"><jsp:include page="editGrpPopup.jsp"></jsp:include></div>
<div id="dataTbl"><table id="tbl">
<thead><tr><th>Item Group Name</th><th>ID</th></tr>
</thead>
</table>
<div id="custom-menusupnew" class="contmenu" style="display:none;height:8%;;width:6%;z-index:1;background-color: white;  box-shadow: 4px 4px 5px #888888;">
    
    <div style="height: 50%;width: 100%" id="iditemgroupedit">
      Edit
 </div>
 <div style="height: 50%;width: 100%" id="iditemdelete">
      
    Delete
</div>
</div>

</div>
</article>
</section>
 

     <!--- <div style="background-color:red;width:100%;height:10%;float:left;padding-top:2%">sdafasd</div>---->
    <div class="site-footer">
     </div>       
    <!--//body content--> 
  </div>
  <!--//Main Content--> 
</div>


<script>
var itemid=null;
var itemname=null;
var table=null;
var a=0;

$(function(){
	loaddataintoTb(3);
function loaddataintoTb(val){
	if(val==3){
		a=0;
		
		
	}
	if(val==1){
		
			
		
		
	a+=50;
	}
	
	if(val==2)
		{
		if(a<1){
			return;
		
		}else{
		a-=50;
		}
		}
	$('#tbl').dataTable().fnDestroy();
	table=  $('#tbl').DataTable( {
    	"ajax": {
    	    "url":"JqueryServlet",
               "data": {
    	        "id": a
    	    }},
        "columnDefs": [{
        	"targets": [1],
        	"visible": false,
        	"searchable": false
        	}
        	],
       
        "columns": [
           
            {
                'data': "ITEMGROUP_NAME_VC",
                'render': function (data, type, row) {
                    return '<a id=' + row.M_ITEMGROUPID +'>'+row.ITEMGROUP_NAME_VC+'</a>'
                }},
                { "data": "M_ITEMGROUPID" }
        ]
    
    });
    
}
        $("#tblsearchsearching").on('keyup', function () {
    		
    	    table.search($(this).val()).draw();
    	})
        
        $(document).on('click','#tbl tbody tr',function(){
        	 itemid=$(this).find('td').eq(0).find('a').attr('id');
        
        	$('#idhiddengroupid').val($(this).find('td').eq(1).text())
        	
        	menusho('custom-menusupnew', itemid);
        	
        })
        function menusho(id, ids) {
        var x = $("#" + ids).position();
        var elm = $('#dataTbl');
       
       
        var y = elm.position();
       
        $('#' + id).css({
            position: 'absolute',
            top: x.top + y.top+15,
            left: x.left + y.left + 55
        }).slideToggle('slow');
    }  
});



$('#substrip').css({'display':'none'});


$(function () {
    $("#addnewitemmaster").click(function () {
    	
    	$('#itemedit').css('border-color','black');
    	
        $('#editgrp').css({'display':'block'});
        $('#deletedata').css({'display':'none'});
        $('#submitedit').css({'display':'none'});
        $('#submitcreate').css({'display':'block'});
        
        $("#editgrp").css({ 'z-index': 1 });})
    });
$(function () {
	
    $("#iditemgroupedit").click(function () {
    	 $.ajax({
		        type: "GET",
		        url: "editloadname",
		        data: { id : itemid  }
		      }).done(function( msg ) {
		    	  var m =JSON.parse(msg);
		    	  $.each(m.data,function(index,row){
		    		  $("#itemedit").val(row.ITEMGROUP_NAME_VC);
		    	  })
		    	  $('#itemedit').css('border-color','black');
		    	  $('#editgrp').css({'display':'block'});
		          $('#submitcreate').css({'display':'none'});
		          $('#deletedata').css({'display':'none'});
		          $('#submitedit').css({'display':'block'});
		          
		          $("#editgrp").css({ 'z-index': 1 });})
		      });
       
        
        
        
        
        
    });
$(function () {
	
    $("#iditemdelete").click(function () {
    	 $.ajax({
		        type: "GET",
		        url: "editloadname",
		        data: { id : itemid  }
		      }).done(function( msg ) {
		    	  var m =JSON.parse(msg);
		    	  $.each(m.data,function(index,row){
		    		  $("#itemedit").val(row.ITEMGROUP_NAME_VC);
		    	  })
		    	  $('#itemedit').css('border-color','black');
		    	  $('#editgrp').css({'display':'block'});
		          $('#submitcreate').css({'display':'none'});
		          $('#deletedata').css({'display':'block'});
		          $('#submitedit').css({'display':'none'});
		          
		          $("#editgrp").css({ 'z-index': 1 });})
		      });
       
        
        
        
        
        
    });

$("#fileclose_VIEW , #canceledit12").click(function () {
	$('#editgrp').css({'display':'none'});
	$("#itemedit").val("")
	 location.reload();
});
$("#minimize_VIEW ").click(function () {
	$('#editgrp').css({'display':'none'});
	
});



$(document).ready(function(){
	  $('#iditemdelete').click(function(){
		  $('#editgrp').css({'display':'block'});
		  $('#deletedata').css({'display':'block'});
	   });
	
});


$("#fileclose_VIEW , #canceledit12").click(function () {
	$('#editgrp').css({'display':'none'});
});
$(function(){
  $("#menu").on("click" , function(){
    $(this).toggleClass("active");
    $("body").toggleClass("nav-open");   
  });
 });
$("#iditemgroupedit").hover(function(){
    $(this).css("background-color", "#e5e5e5");
    }, function(){
    $(this).css("background-color", "white");
});
$("#iditemdelete").hover(function(){
    $(this).css("background-color", "#e5e5e5");
    }, function(){
    $(this).css("background-color", "white");
});
$("#iditemgroupedit").click(function(){
    $(this).css("background-color", "#e5e5e5");
  
});
</script>

<script>
    $(document).ready(function() {
    $("#opensubmen").click(function(){
    $("#submen1").toggle();
        });
});
</script>

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
</div>
</body>
</html>
