
<%@page buffer="10kb"%> 
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Push Off</title>
<jsp:include page="linkall.jsp"></jsp:include>
<style>
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}

html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 60px;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-bottom: 15px;
  width:30%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}


table {
    border-collapse: collapse !important;
    width: 100% !important;
    border: 1px solid #ddd !important;
}

th, td {
    border: 1px solid #b5b5b5 !important;
    text-align: left !important;
    padding: 8px !important;
}
th {
    background-color: #303030 !important;
    color: white !important;
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}

header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover, header button.active { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #303030;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  padding: 8px;
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  padding: 0px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}

aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #4f4f4f;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media only screen and (max-width: 640px){
    body.nav-open aside{
        margin-top: 8.3%
    }  
}
@media only screen and (min-width: 768px){
    body.nav-open aside{
        margin-top: 7.2%
    }  
}
@media only screen and (min-width: 1024px){
    body.nav-open aside{
        margin-top: 5.1%
    }  
}

@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
    width: 100%;
    height: 40px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}
.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-align:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}

.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}

.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
 .submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left: 15.4em;
    top:0.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:60%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}

</style>
</head>
<script type="text/javascript">
var a=0;
var table;
function loaddataintoTb(val){
	if(val==3){
		a=0;
		
		
	}
	if(val==1){
		
			
		
		
	a+=50;
	}
	
	if(val==2)
		{
		if(a<1){
			return;
		
		}else{
		a-=50;
		}
		}
	$('#tblitemlist').dataTable().fnDestroy();
$(document).ready(function() {
	table=  $('#tblitemlist').DataTable( {
    	"ajax": {
    	    "url": "getitemmasterlist",
    	   
    	    "data": {
    	        "id": a
    	    }},
    	    
        "columnDefs": [{
        	"targets": [ 0 ],
        	"visible": false,
        	"searchable": true
        	},
        	],
       
        "columns": [
            { "data": "M_ITEM_ID" },
            {
                'data': "ITEM_NAME_VC",
                'render': function (data, type, row) {
                    return '<a id=' + row.M_ITEM_ID +'>'+row.ITEM_NAME_VC+'</a>'
                }},
            { "data": "UNIT_NAME_VC" },
            { "data": "GST_DESCRIPTION_VC" },
            { "data": "PURC_RATE_D" },
            { "data": "SALE_RATE_DECI" },
            { "data": "OPEN_QTY_D" }
           
        ]
    
    } );
    
} );}
var $el = $('#tblitemlist');


$el.on('click', '.openDelete', function() {
	alert("Asda");

    var row = $(this).closest('tr')[0];
    var rowData = table.row(row).data();
    if (rowData) {
        var rowId = rowData.ID;

        alert("Asda");
    }
});

</script>
<body onLoad="loaddataintoTb()">


<input type="hidden" id="iditemid">

    <div style="width:100%;height:100%">
<jsp:include page="layout.jsp"></jsp:include>
  
      
        <div id="itemMasterpop" style="display: none">
        		<jsp:include page="itemMasterAdd.jsp"></jsp:include>
                </div>
                <div class="modelouterbody zindex" id="idsearchimage" style="display:none">
        <div><p style="text-align:center;padding-top:25%"><img src="Icons/loading.gif" /></p></div>
    </div>
<div id="maindiv" style="display:block">


<div style="overflow-x:auto;" id="viewitem" >
  <table id="tblitemlist">
  <thead>
    <tr>
    <th style="display:none"></th>
      <th>Item Name</th>
      <th>Unit</th>
      <th>GST Category</th>
      <th>Purchase Rate</th>
      <th>Sales Rate</th>
      <th>Option Stock Quantity</th>
    </tr>
    </thead>
    <tbody id="tblitemlistbody">
    
    </tbody>
  </table>
  <div id="custom-menusupnew" class="contmenu" style="display:none;height:8%;;width:6%;z-index:1;background-color: white;  box-shadow: 4px 4px 5px #888888;">
    
    <div style="height: 50%;width: 100%" id="gstedit">
      Edit
 </div>
 <div style="height: 50%;width: 100%" id="gstdelete">
      
    Delete
</div></div>
</div>
</article>
</section>  
     <!--- <div style="background-color:red;width:100%;height:10%;float:left;padding-top:2%">sdafasd</div>---->
         
    <!--//body content--> 
  </div>
  <!--//Main Content--> 

<script>
$("#substrip").css({'display':'none'});

var itemid="";

$(function(){
	  $("#addnewitemmaster").on("click" , function(){
		  $("#substrip").css({'display':'block'});
		  
		  $("#custom-menusupnew").css({'display':'none'});
		  $("#viewitem").css({'display':'none'});
		  $("#updatemmaster").css({'display':'none'}); 
		  $("#subitemmaster").css({'display':'block'}); 
		  $("#itemMasterpop").css({'display':'block'}); 
		 
		   $("#itemMasterpop").css({ 'z-index': 1 });
	  });
	 
	  
  $(document).on('click','#tblitemlist_next ',function(){
		  var txt=$(this).parents('div').find('.current').text();
		  if(txt==5){
			  
			  loaddataintoTb(1);		  
		  }

	    })
	    
	      $(document).on('click','#tblitemlist_previous ',function(){
		  var txt=$(this).parents('div').find('.current').text();
		  if(txt==1){
			  loaddataintoTb(2);		  
		  }

	    })
	  
	  $("#gstedit").on("click" , function(){
		
		  $.ajax({
		        type: "POST",
		        url: "ItemMasterlistforedit",
		        data: { id : itemid  }
		      }).done(function( msg ) {
		    	 
		    	  var m =JSON.parse(msg);
		    	  
		    	  $.each(m,function(index,row){
		    		  
		    		  
		    		 
		    		  $("#itemName").val(row.ITEM_NAME_VC);
		    		  $("#itemdesc").val(row.ITEM_DESC_VC);
		    		  $("#hsnCode").val(row.HSN_CODE_VC);
		    		  $("#itemgrp").val(row.M_ITEMGROUPID);
		    		  $("#gstcat").val(row.M_GST_CATID);
		    		  $("#salerate").val(row.SALE_RATE_DECI);
		    		  $("#salediscount").val(row.SALE_DISC_D);
		    		  $("#purchaserate").val(row.PURC_RATE_D);
		    		  $("#openqty").val(row.OPEN_QTY_D);
		    		  $("#max").val(row.MAX_LEVEL_D);
		    		  $("#min").val(row.MIN_LEVEL_D);
		    		  $("#record").val(row.REORDER_LEVEL_D);
		    		  $("#unitName").val(row.UNIT_NAME_VC);
		    		  $("#purchasediscount").val(row.PURCHASE_DISCOUNT_D);
		    		  
		    		  
		    		  
		    	  })
		    	
		    	 $("#custom-menusupnew").css({'display':'none'});
		    	  $("#substrip").css({'display':'block'});
				  $("#viewitem").css({'display':'none'});
				  $("#updatemmaster").css({'display':'block'}); 
				  $("#subitemmaster").css({'display':'none'}); 
				  $("#itemMasterpop").css({'display':'block'}); 
				 
				   $("#itemMasterpop").css({ 'z-index': 1 });
		      });
		  
		  
	
	  });
	 
	  $("#tblsearchsearching").on('keyup', function () {
  		
  	    table.search($(this).val()).draw();
  	})
	  $(document).on('click','#tblitemlist tbody td',function(){
		  if($(this).index()==0){
			  var tr=$(this).closest('tr')
	    	  itemid=$(tr).find('td').eq(0).find('a').attr('id');
	    	//alert(itemid.replace('item_',''));
	    	$('#iditemid').val(itemid)
	    	menusho('custom-menusupnew', itemid);
		  }
	    })
	    function menusho(id, ids) {
	    var x = $("#" + ids).position();
	    var elm = $('#viewitem');
	    var y = elm.position();
	    $('#' + id).css({
	        position: 'absolute',
	        top: x.top + y.top+15,
	        left: x.left + y.left + 55
	    }).slideToggle('slow');
	}  
});



$('#gstdelete').click(function(){
	
	 doConfirm("Do you Want to Delete?",function Yes(){
		 delData();
		  location.reload(); 
	 },function No(){
		 
		 
		 
	 })
          
    });
function delData(){
  var name=
   
    $.ajax({
        type: "POST",
        url: "deletitemmaster",
        data: { id : itemid  }
      }).done(function( msg ) {
     
      });

}
</script>

<script>
    $(document).ready(function() {
    $("#opensubmen").click(function(){
    $("#submen1").toggle();
        });
});
    $("#gstedit").hover(function(){
        $(this).css("background-color", "#e5e5e5");
        }, function(){
        $(this).css("background-color", "white");
    });
    $("#gstdelete").hover(function(){
        $(this).css("background-color", "#e5e5e5");
        }, function(){
        $(this).css("background-color", "white");
    });
    $("#iditemgroupedit").click(function(){
        $(this).css("background-color", "#e5e5e5");
      
    });
</script>
<script>
$(document).ready(function(){
$(function(){
$("#menu").on("click" , function(){
$(this).toggleClass("active");
$("body").toggleClass("nav-open");   
});
});
});
</script>
</div>
</body>
</html>
