<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <style>
        html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 60px;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-bottom: 15px;
  width:30%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}

header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover, header button.active { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #303030;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  padding: 8px;
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  padding: 0px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}

aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #4f4f4f;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media only screen and (max-width: 640px){
    body.nav-open aside{
        margin-top: 8.3%
    }  
}
@media only screen and (min-width: 768px){
    body.nav-open aside{
        margin-top: 7.2%
    }  
}
@media only screen and (min-width: 1024px){
    body.nav-open aside{
        margin-top: 5.1%
    }  
}

@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
    width: 100%;
    height: 40px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}
.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-align:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}

.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}

.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
 .submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left: 15.4em;
    top:0.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:60%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}


table {
    border-collapse: collapse;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: 1px solid #b5b5b5;
    text-align: left;
    padding: 8px;
}
th {
    background-color: #303030;
    color: white
}
    .modelouterbody {
     
        position: fixed;
        z-index: 999999;
        right: -50%;
        bottom:-17%;
        width: 200%;
        height: 150%;
        background-color: rgba(255,255,255,0.4);
        overflow: auto;
    }

    .modeinnerbody {
        background-color: #fefefe;
        border: 1px solid #888;
        box-shadow: 3px 14px 23px 8px rgba(171, 164, 168, 0.62);
        margin-bottom: 0;
        width: 40%; 
        margin-top:10%; 
        height:60%; 
        margin-left: 30%
    }

    .modelheader {
    background-color: #303030;
    font-family: Open Sans;
    font-size: 17px;
    height: 0px;
    width: auto;
    padding: 10px;
    padding-right: 20px;       
    }
    .button {
        background-color: #0e1b47;
        border: none;
        color: white;
        height: 28px;
        width: 77px;
        border-radius: 4px;
        text-align: center;
        position: relative;
        display: inline-block;
        font-size: 13px;
        font-family: arial;
        cursor: pointer;
        margin-left: 3px;
    }
        @media screen and (max-width: 768px){
        .modelouterbody{
        width: 100%;
        padding-top: 30px;
        }
        .modeinnerbody{
        width: 100%;
        height: 100%;
        margin-top: 10%;
        margin-left: 18%;
        }
    }
input type="text"{
}
        </style>
        
        
        
        
        
        
    </head>
    <body >
        
<div class="modelouterbody" id="idfileuploadmodal">
    <div class="modeinnerbody">
    
         <div class="content" style="width:100%;height:100%">
      
      <!--//sidebar-->
      <section style="width:100%;height:100%">     
      <article>
<div class="substrp">d</div>
<div style="width:100%;height:100%;">
<div class="sidesubdiv"> 
    <div class="sidesubinpu">
    
    <div class="textinpu">Item Name</div>
    <div style='width:60%;height:100%;float:left'>
    <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
    </div>
    </div>
    
    <div class="sidesubinpu">
    <div class="textinpu">Item Description</div>
                  <div style='width:60%;height:100%;float:left'>
                      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
                  </div>
              </div>
    
    <div class="sidesubinpu">
         <div class="textinpu">Unit Name</div>
           <div style='width:60%;height:100%;float:left'>
                   <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
                  </div>
              </div>
    
      <div class="sidesubinpu">
      <div class="textinpu">HSN Code</div>
           <div style='width:60%;height:100%;float:left'>
           <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
            </div>
              </div>
    
                 <div style='width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;float:left'>
              <div class="textinpuslect">Party Name</div>
              <div style='width:60%;height:100%;float:left;position:relative;left:7%'>
              <select style='width:100%;border-radius:4px;height:30px'>
    <option>Here is the unstyled select box</option>
    <option>The second option</option>
    <option>The third option</option>
  </select>
  </div>
  </div>
    
                    <div style='width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;float:left'>
              <div class="textinpuslect">Party Name</div>
              <div style='width:60%;height:100%;float:left;position:relative;left:7%'>
              <select style='width:100%;border-radius:4px;height:30px'>
    <option>Here is the unstyled select box</option>
    <option>The second option</option>
    <option>The third option</option>
  </select>
  </div>
  </div>
</div>

<div class="sidesubdiv" style="width:48%">
   <div class="sidesubinpu">
      <div class="textinpu" style="width:25.5%">Sales Rate</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
     <div class="sidesubinpu">
      <div style='width:25.5%' class="textinpu">Sales Discount</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
    
       <div class="sidesubinpu">
      <div style='width:25.5%' class="textinpu">Purchase Rate</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
    
      <div class="sidesubinpu">
      <div style='width:25.5%' class="textinpu">Purchase Discount</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
    
         <div class="sidesubinpu">
      <div style='width:25.5%' class="textinpu">Opening Quantity</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
    </div>
              </div>
   <div style=" width:98%;height:40%;float:left">
    <div class="midheadng">Stock Live</div>
    <div style="width:4.5%;height:2px;background-color:#303030;margin-top:7px;position: relative;left: 14px;"></div>
    <div class="bottomdiv" style=''>  
    <div class="sidesubinpu" style=''>
      <div style='width:24.5%' class="textinpu">Maximum</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
         <div class="sidesubinpu" style="padding-bottom:2.6%;">
      <div style='width:24.5%;' class="textinpu">Minimum</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
       </div>
      </div>
  </div>
    <div class="bottomdiv">
      <div class="sidesubinpu" style="padding-bottom:2.6%">
      <div style='width:24.5%' class="textinpu">Record</div>
       <div style='width:60%;height:100%;float:left'>
      <input type="text" name="FirstName" style='width:100%;border-radius:4px;height:30px;border:1px solid #adadad'>
      
       </div>
       
      </div>
    </div>
   
  </div>
<div style="width:100%;height:90px;float:left"></div>
   <div style="width:100%;height:50px;float:left;background-color:#f7f7f7;border:1px solid #adadad"> 
        <button class="button" style="float:right;margin-right:2%;margin-left:0px;background-color:#303030">Cancel</button>
         <button class="button" style="float:right;margin-right:2%;margin-left:0px;background-color:#3845ae">Update</button>
   </div>
      </article>
    </section>  
     <!--- <div style="background-color:red;width:100%;height:10%;float:left;padding-top:2%">sdafasd</div>---->
    <div class="site-footer">
     </div>       
    <!--//body content--> 
  </div>
    </div>
</div>
        <script>
            //form hide show functionality for buyer,employee,designation,department page
            
      $(document).ready(function(){
		  $('#submitedit').click(function(){
			  alert("adsa");
		           editData();
		    });
		   });
		function editData(){
		   var mge = $('#itemedit').val()+","+itemid+","+"3";
		    alert(mge);
		    $.ajax({
		        type: "POST",
		        url: "ajaxEditGrpName",
		        data: { info : mge  }
		      }).done(function( msg ) {
		        alert( "Data Saved: " + msg );
		      });
		}


		   $(document).ready(function(){
				  $('#submitcreate').click(function(){
				           createData();
				    });
				   });
				function createData(){
				   var mge = $('#itemedit').val();
				   
				    $.ajax({
				        type: "POST",
				        url: "AjaxServlet",
				        data: { message : mge  }
				      }).done(function( msg ) {
				    	  $("#itemedit").val("")
				      });
				}

        </script>
    </body>
</html>
