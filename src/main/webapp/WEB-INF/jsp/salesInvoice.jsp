<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Invoice</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/confirmBox.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="editable.dropdown.js"></script>
<script type="text/javascript" src="RECO.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="bootstrap-datetimepicker.min.css" rel="stylesheet" />
<script src="new-moment.min.js"></script>
<script src="new-bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="jquery.tabletojson.js"></script>
<script type="text/javascript" src="RECO.js"></script>
<style>
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
}
html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

.table-condensed tbody{
background-color:white;
border:1px solid black;
}
.table-condensed{
width:263px !important;
}
a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

/*-------------------------
  CSS Chapters:
  0 - Variables
  1 - General formatting
  2 - Header
  3 - Sidebar
  4 - Main Content
  5 - Main Site Footer
  6 - Media Queries
-------------------------*/
/*-------------------------
  0 - Variables
-------------------------*/
/*-------------------------
  1 - General formatting
-------------------------*/

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 5em;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 margin-top: 5.1%;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-top:1%;
  width:30%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}
.inpslctdivmedium {
    width: 54% !important;
    border-radius: 4px !important;
    height: 27px !important;
    border: 1px solid #adadad !important;
    position: relative !important;
    top: 0% !important;
    
}
.floatright{
float: right;
}
.floatnone{
float:none!important;
}
header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #2A3744;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  padding: 8px;
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  padding: 8px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}

aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px;
      width: 100%;
    height: 100%;
}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #dcdcdc;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
      width: 100%;
    height: 30px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}
.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-4lign:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}
.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}
.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
.submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left: 15.4em;
    top:-1.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:55%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}
.texmaindiv{
    width:100%;
    height:6%;
    position:relative;
    margin-top:3%;
    padding-top:0%;
    margin-left:0%
}
.textinpt{
    width:15%;
    height: 100%;
    float: left;
    font-weight:bold;
    margin-left:4%;
    margin-top:1%
}

.textinpt_2{
        width:20%;
       float:left;
       position: relative;
    top: 6px;
  }
  .maitexdiv{
   width:51.5%;
   height:100%;
   border: 1px solid #adadad;
   float:left;
   margin-left: 1%;
  }
 
 .texdiv{
   width:100%;
   height:26px;
   margin-top:3%;
   margin-left:3%   
  }
  .inpslctdiv{
      width:100% !important;border-radius:4px !important;height:36px !important;border:1px solid #adadad !important;position:relative !important;
      top:0% !important;
  }
    .inpslctdivgrid{
      width:100% !important;border-radius:4px !important;height:25px !important;border:1px solid #adadad !important;position:relative !important;
      top:0% !important;
  }
  th{
      background: #303030;
      color:white;
      font-weight:bold;
  }
  .fa-plus{
      padding-left: 30%
  }
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f6f6f6;
    min-width: 220px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}
.bootstrap-datetimepicker-widget ul{
padding-left:0px!important;
}
.displaynone{
display:none;
}
.table-condensed td{
cursor:pointer;
}
  /*---Drop Down box------*/
 .custom-combobox {
    position: relative;
    display: inline-block;
    width:100%;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
padding: 5px 10px;
width: 94%;
  } 
  
  /*table code*/
table {
    border-collapse: collapse !important;
    width: 100% !important;
}
th{
background-color:#303030 !important;
   color: #fff !important;
   font-size:13px !important;
}

td{
    font-size: 14px !important;
}
th, td {
    padding:5px !important;
    text-align: left !important;
    border-bottom: 1px solid #adadad !important;
}

tr:hover{background-color:#f5f5f5}
  
  
</style>
</head>
<body>
<div>
<jsp:include page="headersaleinvoice.jsp"></jsp:include>

</div>
   <div style="width:100%;min-height:255px">
<table style="width:99%;">
  <tr style='height:38px'>
    <th style='width:20%'>Item Name</th>
    <th style='width:20%'>Item Description</th>
    <th style='width:9%'>Quantity</th>
    <th style='width:9%'>Rate</th>
    <th style='width:9%'>Discount </th>
    <th style='width:9%'>Disc./amount </th>
    <th style='width:9%'>Amount<i id="idaddnewrow" class="fa fa-plus" aria-hidden="true"></i></th>
    <th style='width:9%' class="displaynone">SGST_P</th>
    <th style='width:9%' class="displaynone">SGST_Amount</th>
    <th style='width:9%' class="displaynone">CGST_P</th>
    <th style='width:9%' class="displaynone">CGST_Amount</th>
    <th style='width:9%' class="displaynone">IGST_P</th>
    <th style='width:9%' class="displaynone">IGST_Amount</th>
    <th style='width:9%' class="displaynone">ITEM_ID</th>    
  </tr>
  </table>
  <div style=overflow:auto;max-height:200px>   
  <table id="idtblitemdetail">
  <thead class="displaynone">
  <tr class="displaynone" style='height:38px'>
    <th style='width:20%'>Item_Name</th>
    <th style='width:20%'>Item_Description</th>
    <th style='width:9%'>Quantity</th>
    <th style='width:9%'>Rate</th>
    <th style='width:9%'>Discount </th>
    <th style='width:9%'>Disc./amount </th>
    <th style='width:9%'>Amount<i id="idaddnewrow" class="fa fa-plus" aria-hidden="true"></i></th>
    <th style='width:9%' class="displaynone">SGST_P</th>
    <th style='width:9%' class="displaynone">SGST_Amount</th>
    <th style='width:9%' class="displaynone">CGST_P</th>
    <th style='width:9%' class="displaynone">CGST_Amount</th>
    <th style='width:9%' class="displaynone">IGST_P</th>
    <th style='width:9%' class="displaynone">IGST_Amount</th>
    <th style='width:9%' class="displaynone">ITEM_ID</th>    
  </tr>
  </thead>
  <tbody id="iditemdetailbody"></tbody>
  </table>
    </div>
    </div>       
<div style="width:100%;height:59%">
<div style='width:47%;height:330px;border: 1px solid #adadad;float:left'>
<div style='width:100%;height:40px;background-color:#303030'>asd</div>  
<div class="texdiv" style='margin-top:1%'>
<div class="textinpt_2" style='width:21.5%'>Invoice Date </div>
<div style='width:70%;height:100%;float:left;position: relative;left:0.7%;z-index:9999999'>
<input type="text" class='inpslctdiv' id="dtpInvoiceDate" />
 <script type="text/javascript">
                                        $(function () {
                                            $('#dtpInvoiceDate').datetimepicker({
                                                format: 'DD-MM-YYYY',
                                                minDate: moment(),
                                               //debug:true
                                            });
          
                                        });
                            </script>
</div>
</div>
     
<div class="texdiv" style="display:none">
<div class="textinpt_2">Invoice No.</div>
<div style='width:70%;height:100%;float:left;position: relative;left:2%;'>
<input type="text" id="idinvoiceno" name="FirstName" class='inpslctdiv'>
</div>
</div>
         
<div class="texdiv clpartyname">
<div class="textinpt_2">Party Name</div>
<div style="width:4%;float:left;position:relative;top:9px;text-align:center"><i class="fa fa-clone" aria-hidden="true"></i></div>
<div style='width:49.4%;height:100%;float:left;position: relative;left:0.7%'>
<select class="inpslctdiv" id="cmbParty">
<option value="0">--Select Party--</option>
</select>
</div>
 
<div class="texdiv" style="width:20%;margin-top:0px;margin-left:3%;float:left">
<div style='width:70%;height:100%;float:left;position: relative;left:2%'>
<input id="idparty_balance" disabled="disabled" style="height:28px!important" type="text" name="FirstName" class='inpslctdiv'>
</div>
</div>
</div> 
     
<div class="texdiv clshippedto">
<div class="textinpt_2">Shipped To</div>
<div style="width:4%;float:left;position:relative;top:9px;text-align:center"><i class="fa fa-clone" aria-hidden="true"></i></div>
<div style='width:49.4%;height:100%;float:left;position: relative;left:0.7%'>
<select class="inpslctdiv" id="cmbShippedTo">
<option value="0">--Select Shipped To--</option>
</select>
</div>
 
<div class="texdiv" style="width:20%;margin-top:0px;margin-left:3%;float:left">
<div style='width:70%;height:100%;float:left;position: relative;left:2%'>
<input id="idshiftto_balance" style="height:28px!important"  disabled="disabled" type="text" name="FirstName" class='inpslctdiv'>
</div>
</div>
</div> 
<div style="padding-top:3%;">  
<table>
<thead>
  <tr style='height:38px'>
    <th style="width:13%">Taxable Value</th>
    <th style="width:12%">CGST%</th>
    <th style="width:12%">Value</th>
      <th style="width:12%">SGST%</th>
    <th style="width:12%">Value</th>      
    <th style="width:12%">IGST%</th>
    <th style="width:12%">Value</th>
  </tr>
  </thead>
  </table>  
<table id="idbilltaxsummary">
<thead style="display:none">
  <tr style='height:38px'>
    <th style="width:13%">Taxable_Value</th>
    <th style="width:12%">group_CGST_p</th>
    <th style="width:12%">group_CGST_value</th>
      <th style="width:12%">group_SGST_p</th>
    <th style="width:12%">group_CSGST_value</th>      
    <th style="width:12%">group_IGST_p</th>
    <th style="width:12%">group_ISGST_value</th>
  </tr>
  </thead>
  <tbody id="idbilltaxsummary_body">
  </tbody>
  </table>
  </div>
</div>        
<div class="maitexdiv">   
  <table>

   <tr style="height:40px;background-color:#303030;">
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold"></td>
    <td>
   </td>
   <td>
   </td>
  </tr>
  
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">Total</td>
    <td>
   </td>
   <td style="font-weight:bold" id="id_bill_subtotal">
       
   </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">Discount</td>
    <td>
   </td>
   <td style="font-weight:bold" class="floatnone">
       <input type="text" id="idinputdiscount" class='inpslctdivmedium floatnone' onkeypress='return isNumberKey(this,event)'/>
   </td>
  </tr>
    <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">Cartage</td>
    <td>
   </td>
   <td style="font-weight:bold" class="floatnone">
       <input type="text" id="idinputcartage" class='inpslctdivmedium floatnone' onkeypress='return isNumberKey(this,event)'/>
   </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">SGST</td>
    <td>
   </td>
   <td style="font-weight:bold" id="idbilsgst">
       
   </td>
  </tr>
   <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">CGST</td>
    <td>
   </td>
   <td style="font-weight:bold" id="idbilcgst">
       
   </td>
  </tr>
     <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">IGST</td>
    <td>
   </td>
   <td style="font-weight:bold" id="idbiligst">
       
   </td>
  </tr>
    <tr>
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;color:#3f8880">Round off(-/+)</td>    
    <td>
   </td>
   <td style="font-weight:bold" id="idbillroundoff">
       
   </td>
  </tr>
   <tr style="height:38px">
    <td></td>
    <td></td>
    <td style="padding-left: 25px;padding-right: 26px;position: relative;left:2%;font-weight:bold;font-size:17px;color:#3f8880">Grand Total</td>    
    <td>
		
   </td>
   <td style="font-weight:bold" id="idbilgrandtotal"></td>
  </tr>
  <tr>
	<td colspan="6"> 
 <button class="button" id="btncancel"  style='float:right;margin:0% 1.2% 0% -1%;background-color:#303030;position:initial'>Cancel</button>
<button class="button" id="btnidupdate"  style='float:right;margin:0% 2% 0% -0.4%;position:inherit'>Save</button></td>
  </tr>
</table>
</div>

</div>

<script>
var option=null;
var strdropdownitem=""
var maxTaxPercent=0;
$(function(){
	$.ajax({
	    type: "GET",
	    url: "getsaleinvoicejson",
	    success:function(data){
	    	var m=$.parseJSON(data);
	    	option="<option value=0>-------Select Item------</option>";
	    	$(m.data).each(function (index, o) {    
	    		option =option + "<option value="+ o.M_ITEM_ID+">"+o.ITEM_NAME_VC+"</option>";
	    		strdropdownitem='<div class="ui-widget"><select class="combobox comboboxitem">'+option+'</select></div>'
	    	});
	    }
	});  
	$.ajax({
	    type: "GET",
	    url: "fillComboPartyCustomer",
	    data: { 
	    	
	    },success:function(data){
	    	var m=$.parseJSON(data);
	    	var $select = $('#cmbParty');
	    	$(m.data).each(function (index, o) {    
	    	    var $option = $("<option/>").attr("value", o.SUBGROUP_ID).text(o.COMPANY_NAME_VC);
	    	    $select.append($option);
	    	});
	    	$("#cmbParty").combobox();
	    }
	});  
	$.ajax({
	    type: "GET",
	    url: "fillComboPartyCustomer",
	    data: { 
	    	
	    },success:function(data){
	    	var m=$.parseJSON(data); 
	    	var $cmbShippedTo=$('#cmbShippedTo');
	    	$(m.data).each(function (index, o) {    
	    	    var $option = $("<option/>").attr("value", o.SUBGROUP_ID).text(o.COMPANY_NAME_VC);
	    	    $cmbShippedTo.append($option);
	    	});
	    	$("#cmbShippedTo").combobox();
	    }
	}); 
	var stritemDetail="";
	var i=0;
	$('#idaddnewrow').click(function(){
		stritemDetail="<tr><td style='width:20%' class='clgriditem'>"+strdropdownitem+"</td><td style='width:20%'><input class='inpslctdivgrid' type='text'  /></td><td style='width:9%'><input  class='inpslctdivgrid clinputcal' id=qty_"+i+" type='text' onkeypress='return isNumberKey(this,event)'/></td><td style='width:9%'><input class='inpslctdivgrid clinputcal' id=rate_"+i+" type='text' onkeypress='return isNumberKey(this,event)'/></td><td style='width:9%'><select class='cldiscountperamt' style='width:85%;border-radius:4px;height:25px;border:1px solid #adadad;position:relative;top:0%;'><option value='0'>Percentage</option><option value='1'>Amount</option>select></td><td style='width:9%'><input class='inpslctdivgrid clinputcal' id=disc_"+i+" class='clinputdiscount' id='iddiscount_"+ i + "' type='text' onkeypress='return isNumberKey(this,event)'/></td><td style='width:9%'><input disabled='disabled' class='inpslctdivgrid' id=amount_"+i+" type='text' onkeypress='return isNumberKey(this,event)' /></td><td class='displaynone'></td><td class='displaynone'></td><td class='displaynone'></td><td class='displaynone'></td><td class='displaynone'></td><td class='displaynone'></td><td class='displaynone'></td>tr>"
		$('#iditemdetailbody').append(stritemDetail);
		$(".combobox").combobox();
		$('.custom-combobox').css('width',"84%")
		i=i+1;
	})
	$(document).on('change','.cldiscountperamt',function(){
		recalculateAmount();
	})
	$(document).on('keyup','.clinputcal',function(){
		recalculateAmount();
	})
	$(document).on('focusout','.clgriditem .custom-combobox-input',function(){
		var obj=$(this)
		var tr=$(this).closest('tr');
		var tditemid=$(this).closest('tr').find('td').eq(13);
		var selecteditemid=$(tr).find('td').eq(0).find('select').val();
		var prvrate=$(tr).find('td').eq(3).find('input').val();
		if($(tditemid).text()!=selecteditemid){
			$(tr).find('td').eq(3).find('input').val('')
			$(tr).find('td').eq(7).text('') //SGST %
			$(tr).find('td').eq(9).text('') // CGST %
			$(tr).find('td').eq(11).text('') // IGST %
			$(tditemid).text(selecteditemid);
			$.ajax({
				type:"GET",
				url:"getMaxPurchaseRate",
				data:{intItemId:selecteditemid},
				success:function(data){
					var json=$.parseJSON(data)
					$(json.data).each(function(index,row){
						$(tr).find('td').eq(3).find('input').val(row.Max_Rate)
						if($('#idradiocgst_sgst').prop('checked')==true){
							$(tr).find('td').eq(7).text(getNum(row.GSTPERCENT)/2) //SGST %
							$(tr).find('td').eq(9).text(getNum(row.GSTPERCENT)/2) // CGST %
						}
						else if($('#idradio_igst').prop('checked')==true){
							$(tr).find('td').eq(11).text(getNum(row.GSTPERCENT)) // CGST %
						}
						recalculateAmount();
					})
				},error:function(){
					
				}
			})
		}
		//recalculateAmount();
	})
		$(document).on('focusout','.clpartyname .custom-combobox-input',function(){
			var intPartyId=$(this).parents('div').find('.clpartyname').find('select').val();
			$('#idparty_balance').val('')
				getPartyBalance(intPartyId,function(data){
					$('#idparty_balance').val(data);
				})
	})
			$(document).on('focusout','.clshippedto .custom-combobox-input',function(){
				var intPartyId=$(this).parents('div').find('.clshippedto').find('select').val();
				$('#idshiftto_balance').val('')
				getPartyBalance(intPartyId,function(data){
					$('#idshiftto_balance').val(data);
				})
	})
	function getPartyBalance(partyid,callback){
		var balance=0;
		$.ajax({
		    type: "GET",
		    url: "getpartybalance",
		    data: { 
		    	intPartyId:partyid
		    },success:function(data){
		    	var m=$.parseJSON(data); 
		    	$(m.data).each(function (index, o) {    
		    		balance=o.CURRENT_BALANCE_N
		    		callback(getNum(balance))
		    	});
		    },error:function(){
		    	callback(balance)
		    }
		}); 
		
	}
	$('input[name="radgst"]:radio').change(function(){
		recalculateAmount();
	})
    function getmaxTaxPercent(){
		var trlength=$('#iditemdetailbody tr').length;
		var trcollection=('#iditemdetailbody tr');
		maxTaxPercent=0;
		for(j=0;j<trlength;j++){
			if($('#idradiocgst_sgst').prop('checked')==true){
				if(getNum($(trcollection).eq(j).find('td').eq(7).text())>maxTaxPercent){  // CGST %
					maxTaxPercent=getNum($(trcollection).eq(j).find('td').eq(7).text());
				}
			}
			else if($('#idradio_igst').prop('checked')==true){
				if(getNum($(trcollection).eq(j).find('td').eq(11).text())>maxTaxPercent){ // IGST %
					maxTaxPercent=getNum($(trcollection).eq(j).find('td').eq(11).text());
				}
			}
		}
	}
	$('#idinputcartage').keyup(function(){
		recalculateAmount();
	})
	function recalculateAmount(){
		var trlength=$('#iditemdetailbody tr').length;
		var trcollection=('#iditemdetailbody tr');
		var j=0;
		var totalcgstamount=0;
		var totalsgstamount=0;
		var totaligstamount=0;
		var itemtotalvalue=0;
		var billgrandtotal=0;
		var taxGroup=new Array();
		$('#idbilcgst').text('');
		$('#idbilsgst').text('');
		$('#idbiligst').text('');
		for(j=0;j<trlength;j++){
			var itemtaxamount=0;
			var itemsubtotal=0;
			var itemtaxpercent=0;
			var itemgstpercent=0;
			var quantity=$(trcollection).eq(j).find('td').eq(2).find('input').val();
			var rate=$(trcollection).eq(j).find('td').eq(3).find('input').val();
			var discount=$(trcollection).eq(j).find('td').eq(5).find('input').val();
			var cmbdiscamtpercent=$(trcollection).eq(j).find('td').eq(4).find('select').val();
			var subtotal=getNum(quantity)*getNum(rate);
			var discountvalue=0;
			if(cmbdiscamtpercent==0){
				discountvalue=getNum(subtotal) *getNum(discount)/100
			}else{
				discountvalue=discount;
			}
			itemsubtotal=getNum(subtotal)-getNum(discountvalue)
			$(trcollection).eq(j).find('td').eq(6).find('input').val(itemsubtotal);  // Item Taxable value
			if($('#idradiocgst_sgst').prop('checked')==true){
				itemtaxpercent=$(trcollection).eq(j).find('td').eq(11).text();
				itemgstpercent=getNum(itemtaxpercent)/2
				if(itemgstpercent==0)
					{
						itemgstpercent=getNum($(trcollection).eq(j).find('td').eq(7).text())
					}
				$(trcollection).eq(j).find('td').eq(7).text(itemgstpercent) //SGST %
				$(trcollection).eq(j).find('td').eq(9).text(itemgstpercent) // CGST %
				itemtaxamount=getNum(itemsubtotal)*getNum(itemgstpercent)/100
				$(trcollection).eq(j).find('td').eq(8).text(getNum(itemtaxamount))  //SGST AMOUNT
				$(trcollection).eq(j).find('td').eq(10).text(getNum(itemtaxamount)) // CGST AMOUNT
				totalcgstamount=getNum(totalcgstamount)+getNum(itemtaxamount);
				totalsgstamount=getNum(totalsgstamount)+getNum(itemtaxamount);
				if(taxGroup.indexOf($(trcollection).eq(j).find('td').eq(7).text())==-1){ //Group Tax Category store in 
					taxGroup.push($(trcollection).eq(j).find('td').eq(7).text())
				}
				$('#idbilcgst').text(totalcgstamount)
				$('#idbilsgst').text(totalsgstamount)
				$(trcollection).eq(j).find('td').eq(11).text('')
				$(trcollection).eq(j).find('td').eq(12).text('')
			}
			else if($('#idradio_igst').prop('checked')==true){
				itemtaxpercent=getNum($(trcollection).eq(j).find('td').eq(7).text())+getNum($(trcollection).eq(j).find('td').eq(9).text())
				if(itemtaxpercent==0)
					{
						itemtaxpercent=getNum($(trcollection).eq(j).find('td').eq(11).text())
					}
				$(trcollection).eq(j).find('td').eq(11).text(getNum(itemtaxpercent)) // IGST %
				itemtaxamount=getNum(itemsubtotal)*getNum(itemtaxpercent)/100
				$(trcollection).eq(j).find('td').eq(12).text(getNum(itemtaxamount)) // IGST AMOUNT
				totaligstamount=getNum(totaligstamount)+getNum(itemtaxamount);
				$('#idbiligst').text(totaligstamount)
				if(taxGroup.indexOf($(trcollection).eq(j).find('td').eq(11).text())==-1){
					taxGroup.push($(trcollection).eq(j).find('td').eq(11).text())
				}
				$(trcollection).eq(j).find('td').eq(7).text('')
				$(trcollection).eq(j).find('td').eq(9).text('')
				$(trcollection).eq(j).find('td').eq(8).text('')
				$(trcollection).eq(j).find('td').eq(10).text('')
			}
			itemtotalvalue=itemtotalvalue + getNum(itemsubtotal);//===============item total amount
		}
		getmaxTaxPercent();
		var k=0;
		var p=0;
		var arrayleng=taxGroup.length;
		var strtaxsummary="";
		$('#idbilltaxsummary_body').html('');
		for(k=0;k<arrayleng;k++){
			var taxcategory=taxGroup[k];
			var sgstTotalAmount=0;
			var cgstTotalAmount=0;
			var igstTotalAmount=0
			var taxableValue=0;
			for(p=0;p<trlength;p++){
				if($('#idradiocgst_sgst').prop('checked')==true){
					if($(trcollection).eq(p).find('td').eq(7).text()==taxcategory){
						sgstTotalAmount=getNum(sgstTotalAmount)+getNum($(trcollection).eq(p).find('td').eq(8).text())
						cgstTotalAmount=getNum(cgstTotalAmount)+getNum($(trcollection).eq(p).find('td').eq(10).text())
						taxableValue=getNum(taxableValue)+getNum($(trcollection).eq(p).find('td').eq(6).find('input').val())
					}
				}
				else if($('#idradio_igst').prop('checked')==true){
					if($(trcollection).eq(p).find('td').eq(11).text()==taxcategory){
						igstTotalAmount=getNum(igstTotalAmount)+getNum($(trcollection).eq(p).find('td').eq(12).text())
						taxableValue=getNum(taxableValue)+getNum($(trcollection).eq(p).find('td').eq(6).find('input').val())
					}
				}
			}
			if($('#idradiocgst_sgst').prop('checked')==true){
				strtaxsummary=strtaxsummary+"<tr><td style='width:13%'>"+taxableValue+"</td><td style='width:12%'>"+taxcategory+"</td><td style='width:12%'>"+cgstTotalAmount+"</td><td style='width:12%'>"+taxcategory+"</td><td style='width:12%'>"+cgstTotalAmount+"</td><td style='width:12%'></td><td style='width:12%'></td></tr>";
			}
			else if($('#idradio_igst').prop('checked')==true){
				strtaxsummary=strtaxsummary+"<tr><td>"+taxableValue+"</td><td></td><td></td><td></td><td></td><td>"+taxcategory+"</td><td>"+igstTotalAmount+"</td></tr>";
			}
		}
		//=================CARTAGE TAX CALCULATION=====================================================
		var cartageTaxAmount=0;
		cartageTaxAmount=getNum($('#idinputcartage').val()*getNum(maxTaxPercent)/100)
		if($('#idradiocgst_sgst').prop('checked')==true){
			$('#idbilcgst').text(getNum(getNum($('#idbilcgst').text())+getNum(cartageTaxAmount)))
			$('#idbilsgst').text(getNum(getNum($('#idbilsgst').text())+getNum(cartageTaxAmount)))
		}
		else if($('#idradio_igst').prop('checked')==true){
			$('#idbiligst').text(getNum(getNum($('#idbiligst').text())+getNum(cartageTaxAmount)))
		}
		$('#idbilltaxsummary_body').html(strtaxsummary);
		$('#id_bill_subtotal').text(getNum(itemtotalvalue));
		$('#idbilgrandtotal').text(getRound(getNum(itemtotalvalue)+getNum($('#idbilcgst').text())+getNum($('#idbilsgst').text())+getNum($('#idbiligst').text())+getNum($('#idinputcartage').val())));
	}
	
	$('#btnidupdate').click(function(){
        doConfirm('<%=Util.utils.strconfirmsalebill %>', function yes() {
        	//saveSaleBill();
        	updateSaleBill();
        }, function no() {

        });
	})
	
	function editSaleBill(callback){
    	$.ajax({
			type:'GET',
			url:"getSaleBillForEdit",
			data:{intInvoiceNo:53},
			success:function(data){
				var responsedata=$.parseJSON(data);
				$(responsedata.tlist).each(function(index,trow){
					if(trow.PAYMENT_TYPE_VC=='<%=Util.utils.strCashPaymentType %>'){
						$('#idradio_cash').prop('checked',true)
					}else if(trow.PAYMENT_TYPE_VC=='<%=Util.utils.strBankPaymentType %>'){
						$('#idradio_bank').prop('checked',true)
					}else if(trow.PAYMENT_TYPE_VC=='<%=Util.utils.strCreditPaymentType %>'){
						$('#idradio_credit').prop('checked',true)
					}else if(trow.PAYMENT_TYPE_VC=='<%=Util.utils.strCreditCardType %>'){
						$('#idradio_creditcard').prop('checked',true)
					}
					if(trow.GST_CATEGORY_ID_N=='<%=Util.utils.intGstScgstCat %>'){
						$('#idradiocgst_sgst').prop('checked',true)
					}else if(trow.GST_CATEGORY_ID_N=='<%=Util.utils.intIgstCat %>'){
						$('#idradio_igst').prop('checked',true)
					}else if(trow.GST_CATEGORY_ID_N=='<%=Util.utils.intComboCat %>'){
						$('#idradio_combo').prop('checked',true)
					}else if(trow.GST_CATEGORY_ID_N=='<%=Util.utils.intAfCat %>'){
						$('#idradio_af').prop('checked',true)
					}
					$('#cmbParty').val(trow.SUBGROUP_ID)
					$('#cmbParty').parents('div').find('.clpartyname').find('.custom-combobox-input').val($('#cmbParty option:selected').text());
					$('#cmbShippedTo').val(trow.SHIPTO_ID_N);
					$('#cmbShippedTo').parents('div').find('.clshippedto').find('.custom-combobox-input').val($('#cmbShippedTo option:selected').text());
					$('#idinvoiceno').val(trow.INVOICE_NO_N);
					$('#dtpInvoiceDate').val(trow.INVOICE_DATE_D);
					$('#id_bill_subtotal').text(trow.TOTAL_AMOUNT_N);
					var DISC_PERCENT_N=0;
					$('#idinputdiscount').val(trow.DISC_AMOUNT_N);
					$('#idbilcgst').text(trow.CGST_AMOUNT_N);
					var CGST_PERCENT_N=0;
					$('#idbilsgst').text(trow.SGST_AMOUNT_N);
					var SGST_PERCENT_N=0;
					$('#idinputcartage').val(trow.CARTAGE_N);
					var ROUND_OFF_PERCENT_N=0;
					$('#idbillroundoff').text(trow.ROUND_OFF_AMOUNT_N);
					$('#idbilgrandtotal').text(trow.GRAND_TOTAL_N);
					var IGST_PERCENT_N=0;
					$('#idbiligst').text(trow.IGST_AMOUNT_N);
					$('#idshiftto_balance').val('')
					getPartyBalance(trow.SHIPTO_ID_N,function(data){
						$('#idshiftto_balance').val(data);
					})
					getPartyBalance(trow.SUBGROUP_ID,function(data){
						$('#idparty_balance').val(data);
					})
				})
				var strItemDetailEdit
				$(responsedata.olist).each(function(index,orow){
					strItemDetailEdit="<tr id=itemrow_"+i+"><td style='width:20%' class='clgriditem'>"+strdropdownitem+"</td><td style='width:20%'><input   type='text' class='inpslctdivgrid' value='"+replacenullvalue(orow.ITEM_DESC_VC)+"'  /></td><td style='width:9%'><input value='"+orow.ITEM_QUANTITY_N+"' class='inpslctdivgrid clinputcal' id=qty_"+i+" type='text' onkeypress='return isNumberKey(this,event)' /></td><td style='width:9%'><input value='"+orow.RATE_N+"' class='inpslctdivgrid clinputcal' id=rate_"+i+" type='text' onkeypress='return isNumberKey(this,event)'/></td><td style='width:9%'><select class='cldiscountperamt' style='width:85%;border-radius:4px;height:25px;border:1px solid #adadad;position:relative;top:0%;'><option value='0'>Percentage</option><option value='1'>Amount</option>select></td><td style='width:9%'><input value='"+orow.DISC_AMOUNT_N+"' class='inpslctdivgrid clinputcal' id=disc_"+i+" class='clinputdiscount' id='iddiscount_"+ i + "' type='text' onkeypress='return isNumberKey(this,event)'/></td><td style='width:9%'><input disabled='disabled' value='"+orow.AMOUNT_N+"' class='inpslctdivgrid' id=amount_"+i+" type='text' onkeypress='return isNumberKey(this,event)' /></td><td class='displaynone SGST_P'>"+orow.SGST_PERCENT_N+"</td><td class='displaynone SGST_Amount'>"+orow.SGST_AMOUNT_N+"</td><td class='displaynone CGST_P'>"+orow.CGST_PERCENT_N+"</td><td class='displaynone CGST_Amount'>"+orow.CGST_AMOUNT_N+"</td><td class='displaynone IGST_P'>"+orow.IGST_PERCENT_N+"</td><td class='displaynone IGST_Amount'>"+orow.IGST_AMOUNT_N+"</td><td class='displaynone ITEM_ID'>"+orow.M_ITEM_ID+"</td>tr>"
					$('#iditemdetailbody').append(strItemDetailEdit);
					$(".combobox").combobox();
					var id="itemrow_"+i;
					$('#'+id).find('td').eq(0).find('select').val(orow.M_ITEM_ID)
					$('#'+id).find('td').eq(0).find('.custom-combobox-input').val($('#'+id).find('td').eq(0).find('select option:selected').text());
					$('.custom-combobox').css('width',"84%");
					i=i+1;
				})
				var strTaxDetailEdit="";
				$('#idbilltaxsummary_body').html('');
				$(responsedata.otaxlist).each(function(index,taxrow){
					if($('#idradiocgst_sgst').prop('checked')==true){
						strTaxDetailEdit=strTaxDetailEdit+"<tr><td style='width:13%'>"+taxrow.TAXABLE_VALUE_N+"</td><td style='width:12%'>"+taxrow.SGST_PERCENT_N+"</td><td style='width:12%'>"+taxrow.SGST_VALUE_N+"</td><td style='width:12%'>"+taxrow.CGST_PERCENT_N+"</td><td style='width:12%'>"+taxrow.CGST_VALUE_N+"</td><td style='width:12%'></td><td style='width:12%'></td></tr>";
						$('#idbilltaxsummary_body').html(strTaxDetailEdit);
					}
					else if($('#idradio_igst').prop('checked')==true){
						strTaxDetailEdit=strTaxDetailEdit+"<tr><td>"+taxrow.TAXABLE_VALUE_N+"</td><td></td><td></td><td></td><td></td><td>"+taxrow.IGST_PERCENT_N+"</td><td>"+taxrow.IGST_VALUE_N+"</td></tr>";
						$('#idbilltaxsummary_body').html(strTaxDetailEdit);
					}
				})
				callback('success')
			},error:function(){
				callback('error')
			}
    	});
	}
	function saveSaleBill(){
		var paymenttype="";
		var cgstCategoryId=0;
		if($('#idradio_cash').prop('checked')==true){
			paymenttype='<%=Util.utils.strCashPaymentType %>'
		}else if($('#idradio_bank').prop('checked')==true){
			paymenttype='<%=Util.utils.strBankPaymentType %>'
	}else if($('#idradio_credit').prop('checked')==true){
		paymenttype='<%=Util.utils.strCreditPaymentType %>'
	}else if($('#idradio_creditcard').prop('checked')==true){
		paymenttype='<%=Util.utils.strCreditCardType %>'
	}
		if($('#idradiocgst_sgst').prop('checked')==true){
			cgstCategoryId='<%=Util.utils.intGstScgstCat %>'
		}else if($('#idradio_igst').prop('checked')==true){
			cgstCategoryId='<%=Util.utils.intIgstCat %>'
	}else if($('#idradio_combo').prop('checked')==true){
		cgstCategoryId='<%=Util.utils.intComboCat %>'
	}else if($('#idradio_af').prop('checked')==true){
		cgstCategoryId='<%=Util.utils.intAfCat %>'
	}
		var GST_CATEGORY_ID_N=cgstCategoryId;
		var PAYMENT_TYPE_VC=paymenttype;
		var SUBGROUP_ID=numCorrect($('#cmbParty option:selected').val());
		var SHIPTO_ID_N=numCorrect($('#cmbShippedTo option:selected').val());
		var INVOICE_NO_N=numCorrect($('#idinvoiceno').val());
		var INVOICE_DATE_D=$('#dtpInvoiceDate').val();
		var TOTAL_AMOUNT_N=numCorrect($('#id_bill_subtotal').text());
		var DISC_PERCENT_N=0;
		var DISC_AMOUNT_N=getNum($('#idinputdiscount').val());
		var CGST_AMOUNT_N=getNum($('#idbilcgst').text());
		var CGST_PERCENT_N=0;
		var SGST_AMOUNT_N=getNum($('#idbilsgst').text());
		var SGST_PERCENT_N=0;
		var CARTAGE_N=getNum($('#idinputcartage').val());
		var ROUND_OFF_PERCENT_N=0;
		var ROUND_OFF_AMOUNT_N=getNum($('#idbillroundoff').text());
		var GRAND_TOTAL_N=getNum($('#idbilgrandtotal').text());
		var IGST_PERCENT_N=0;
		var IGST_AMOUNT_N=getNum($('#idbiligst').text());
		var objarray=$('#idtblitemdetail').tableToJSON();
		var objtax=$('#idbilltaxsummary').tableToJSON();
		$.ajax({
			type:'POST',
			url:"InsertSaleInvoice",
			data:{
				GST_CATEGORY_ID_N:GST_CATEGORY_ID_N,
				PAYMENT_TYPE_VC:PAYMENT_TYPE_VC,
				SUBGROUP_ID:SUBGROUP_ID,
				SHIPTO_ID_N:SHIPTO_ID_N,
				INVOICE_NO_N:INVOICE_NO_N,
				INVOICE_DATE_D:INVOICE_DATE_D,
				TOTAL_AMOUNT_N:TOTAL_AMOUNT_N,
				DISC_PERCENT_N:DISC_PERCENT_N,
				DISC_AMOUNT_N:DISC_AMOUNT_N,
				CGST_AMOUNT_N:CGST_AMOUNT_N,
				CGST_PERCENT_N:CGST_PERCENT_N,
				SGST_AMOUNT_N:SGST_AMOUNT_N,
				SGST_PERCENT_N:SGST_PERCENT_N,
				CARTAGE_N:CARTAGE_N,
				ROUND_OFF_PERCENT_N:ROUND_OFF_PERCENT_N,
				ROUND_OFF_AMOUNT_N:ROUND_OFF_AMOUNT_N,
				GRAND_TOTAL_N:GRAND_TOTAL_N,
				IGST_PERCENT_N:IGST_PERCENT_N,
				IGST_AMOUNT_N:IGST_AMOUNT_N,
				objarray:JSON.stringify(objarray),
				objtax:JSON.stringify(objtax)
			},success:function(data){
				var m=$.parseJSON(data); 
		    	$(m).each(function (index, o) {  
		    		successicon(o.success)
		    		if(o.error==""){
		    			successicon(o.success)	
		    			clearData();
					}   
		    	});
			}
		})
	}
	function updateSaleBill(){
		var paymenttype="";
		var cgstCategoryId=0;
		if($('#idradio_cash').prop('checked')==true){
			paymenttype='<%=Util.utils.strCashPaymentType %>'
		}else if($('#idradio_bank').prop('checked')==true){
			paymenttype='<%=Util.utils.strBankPaymentType %>'
	}else if($('#idradio_credit').prop('checked')==true){
		paymenttype='<%=Util.utils.strCreditPaymentType %>'
	}else if($('#idradio_creditcard').prop('checked')==true){
		paymenttype='<%=Util.utils.strCreditCardType %>'
	}
		if($('#idradiocgst_sgst').prop('checked')==true){
			cgstCategoryId='<%=Util.utils.intGstScgstCat %>'
		}else if($('#idradio_igst').prop('checked')==true){
			cgstCategoryId='<%=Util.utils.intIgstCat %>'
	}else if($('#idradio_combo').prop('checked')==true){
		cgstCategoryId='<%=Util.utils.intComboCat %>'
	}else if($('#idradio_af').prop('checked')==true){
		cgstCategoryId='<%=Util.utils.intAfCat %>'
	}
		var GST_CATEGORY_ID_N=cgstCategoryId;
		var PAYMENT_TYPE_VC=paymenttype;
		var SUBGROUP_ID=numCorrect($('#cmbParty option:selected').val());
		var SHIPTO_ID_N=numCorrect($('#cmbShippedTo option:selected').val());
		var INVOICE_NO_N=numCorrect($('#idinvoiceno').val());
		var INVOICE_DATE_D=$('#dtpInvoiceDate').val();
		var TOTAL_AMOUNT_N=numCorrect($('#id_bill_subtotal').text());
		var DISC_PERCENT_N=0;
		var DISC_AMOUNT_N=getNum($('#idinputdiscount').val());
		var CGST_AMOUNT_N=getNum($('#idbilcgst').text());
		var CGST_PERCENT_N=0;
		var SGST_AMOUNT_N=getNum($('#idbilsgst').text());
		var SGST_PERCENT_N=0;
		var CARTAGE_N=getNum($('#idinputcartage').val());
		var ROUND_OFF_PERCENT_N=0;
		var ROUND_OFF_AMOUNT_N=getNum($('#idbillroundoff').text());
		var GRAND_TOTAL_N=getNum($('#idbilgrandtotal').text());
		var IGST_PERCENT_N=0;
		var IGST_AMOUNT_N=getNum($('#idbiligst').text());
		var objarray=$('#idtblitemdetail').tableToJSON();
		var objtax=$('#idbilltaxsummary').tableToJSON();
		$.ajax({
			type:'POST',
			url:"updateSaleInvoice",
			data:{
				GST_CATEGORY_ID_N:GST_CATEGORY_ID_N,
				PAYMENT_TYPE_VC:PAYMENT_TYPE_VC,
				SUBGROUP_ID:SUBGROUP_ID,
				SHIPTO_ID_N:SHIPTO_ID_N,
				INVOICE_NO_N:INVOICE_NO_N,
				INVOICE_DATE_D:INVOICE_DATE_D,
				TOTAL_AMOUNT_N:TOTAL_AMOUNT_N,
				DISC_PERCENT_N:DISC_PERCENT_N,
				DISC_AMOUNT_N:DISC_AMOUNT_N,
				CGST_AMOUNT_N:CGST_AMOUNT_N,
				CGST_PERCENT_N:CGST_PERCENT_N,
				SGST_AMOUNT_N:SGST_AMOUNT_N,
				SGST_PERCENT_N:SGST_PERCENT_N,
				CARTAGE_N:CARTAGE_N,
				ROUND_OFF_PERCENT_N:ROUND_OFF_PERCENT_N,
				ROUND_OFF_AMOUNT_N:ROUND_OFF_AMOUNT_N,
				GRAND_TOTAL_N:GRAND_TOTAL_N,
				IGST_PERCENT_N:IGST_PERCENT_N,
				IGST_AMOUNT_N:IGST_AMOUNT_N,
				objarray:JSON.stringify(objarray),
				objtax:JSON.stringify(objtax)
			},success:function(data){
				var m=$.parseJSON(data); 
		    	$(m).each(function (index, o) {  
		    		successicon(o.success)
		    		if(o.error==""){
		    			successicon(o.success)	
		    			clearData()
					}   
		    	});
			}
		})
	}
	function clearData(){
		$('#idradio_cash').prop('checked',true)
		$('#idradiocgst_sgst').prop('checked',true)
		$('#cmbParty').val(0)
		$('#cmbParty').parents('div').find('.clpartyname').find('.custom-combobox-input').val($('#cmbParty option:selected').text());
		$('#cmbShippedTo').val(0);
		$('#cmbShippedTo').parents('div').find('.clshippedto').find('.custom-combobox-input').val($('#cmbShippedTo option:selected').text());
		$('#idinvoiceno').val('');
		$('#dtpInvoiceDate').val('');
		$('#id_bill_subtotal').text('');
		var DISC_PERCENT_N=0;
		$('#idinputdiscount').val('');
		$('#idbilcgst').text('');
		var CGST_PERCENT_N=0;
		$('#idbilsgst').text('');
		var SGST_PERCENT_N=0;
		$('#idinputcartage').val('');
		var ROUND_OFF_PERCENT_N=0;
		$('#idbillroundoff').text('');
		$('#idbilgrandtotal').text('');
		var IGST_PERCENT_N=0;
		$('#idbiligst').text('');
		$('#idshiftto_balance').val('')
		$('#idshiftto_balance').val('');
		$('#idparty_balance').val('');
		$('#iditemdetailbody').html('');
		$('#idbilltaxsummary_body').html('');
	}
})
</script>
 </div>

</article>
</section>  
     <!--- <div style="background-color:red;width:100%;height:10%;float:left;padding-top:2%">sdafasd</div>---->
    
    <!--//body content--> 
  </div>
  <!--//Main Content--> 
</div>
</body></html>