<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Push Off</title>

<style>
input[type='radio'] {
	-webkit-appearance: none;
	width: 25px;
	height: 25px;
	border-radius: 100%;
	outline: none;
}

input[type='radio']:before {
	content: '';
	display: block;
	width: 25px;
	height: 25px;
	margin: 20% auto;
	border-radius: 100%;
	background: radial-gradient(circle, #f0f0f0 40%, #d8d8d8 30%);
	border: 1px solid #acacac;
}

input[type='radio']:checked:before {
	background: radial-gradient(circle, #e9ffe8 40%, #0e2f44 40%);
	border: 1px solid #3980ad;
}

label {
	padding: 1%;
}

</style>
<script type="text/javascript">


	$(function() {
		$('#itemName').keyup(function() {
			var itemname = $('#itemName').val();
			if(itemname.length==0){
				
				 $("#showError").html("\<b>&#10006</b>").css('color','#D34729');
				
				
			}
			else{
				
				 $("#showError").html("\<b>&#10004</b>").css('color','#008000');	
				
				
			}
			
			
			
		});
		$('#itemdesc').keyup(function() {
			var itemdesc = $('#itemdesc').val();
			if(itemdesc.length==0){
				
				 $("#showError1").html("\<b>&#10006</b>").css('color','#D34729');
				
				
			}
			else{
				
				 $("#showError1").html("\<b>&#10004</b>").css('color','#008000');	
				
				
			}
			
			
			
		});
		$('#unitName').keyup(function() {
			var unitName = $('#unitName').val();
			if(unitName.length==0){
				
				 $("#showError2").html("\<b>&#10006</b>").css('color','#D34729');
				
				
			}
			else{
				
				 $("#showError2").html("\<b>&#10004</b>").css('color','#008000');	
				
				
			}
			
			
			
		});
		$('#hsnCode').keyup(function() {
			var hsnCode = $('#hsnCode').val();
			if(hsnCode.length==0){
				
				 $("#showError3").html("\<b>&#10006</b>").css('color','#D34729');
				
				
			}
			else{
				
				 $("#showError3").html("\<b>&#10004</b>").css('color','#008000');	
				
				
			}
			
			
			
		});
		
		$('#subitemmaster').click(function() {
			
			var checked = $('[name="radi"]:radio:checked').val();
			var itemname = $('#itemName').val();
			if(itemname.length==0){
				
				 $("#showError").html("Cannot Be Blank").css('color','#D34729');
				
				return;
			}
			else{
				
				
				
				
			}
			
			var itemdesc = $('#itemdesc').val();
			if(itemdesc.length==0){
				
				 $("#showError1").html("Cannot Be Blank").css('color','#D34729');
				 return;
				
			}
			else{
				
				 
				
				
			}
			var unitName = $('#unitName').val();
			if(unitName.length==0){
				
				 $("#showError2").html("Cannot Be Blank").css('color','#D34729');
				
				 return;
			}
			else{
				
			
				
				
			}
			var hsnCode = $('#hsnCode').val();
			if(hsnCode.length==0){
				
				 $("#showError3").html("Cannot Be Blank").css('color','#D34729');
				 return;
				
			}
			else{
				
			
				
				
			}
			var itemgrp = numCorrect($('#itemgrp').val());

			var gstcat = numCorrect($('#gstcat').val());

			var salerate = numCorrect($('#salerate').val());

			var salediscount = numCorrect($('#salediscount').val());

			var purchaserate = numCorrect($('#purchaserate').val());
			var purchasediscount = numCorrect($('#purchasediscount').val());
			var openqty = numCorrect($('#openqty').val());
			var max = numCorrect($('#max').val());
			var min = numCorrect($('#min').val());
			var record = numCorrect($('#record').val());
			$.ajax({
				type : "POST",
				url : "itemmasteradd",
				data : {
					checked : checked,
					itemname : itemname,
					itemdesc : itemdesc,
					unitName : unitName,
					hsnCode : hsnCode,
					itemgrp : itemgrp,
					gstcat : gstcat,
					salerate : salerate,
					salediscount : salediscount,
					purchaserate : purchaserate,
					purchasediscount : purchasediscount,
					openqty : openqty,
					max : max,
					min : min,
					record : record
				},
				success : function(data) {
					successicon("your data Successfully Submitted");
					
					if (data.indexOf("UNIT_NAME_VC") > -1) {

						$('#unitName').css('border-color','red');  
			    		 $('#unitName').css('border-width','0.5 px');
					}
					
					if (data.indexOf("ITEM_NAME_VC") > -1) {

						$('#itemName').css('border-color','black');  
			    		 $('#itemName').css('border-width','0.5 px');
					}
				
					
				
					if(data="[]")
						{
						$("#itemName").val("");
						$("#itemdesc").val("");
						$("#hsnCode").val("");
			
						$("#salerate").val("");
						$("#salediscount").val("");
						$("#purchaserate").val("");
						$("#openqty").val("");
						$("#max").val("");
						$("#min").val("");
						$("#record").val("");
						$("#unitName").val("");
						$("#purchasediscount").val("");
						$("#showError3").html("");	
						$("#showError2").html("");
						$("#showError1").html("");	
						$("#showError").html("");	
						$('#itemName').css('border-color','black');  
						 $('#itemName').css('border-width','1 px');
						$('#unitName').css('border-color','black');
						 $('#unitName').css('border-width','1 px');
					}

				}
			});
		});

		$('#updatemmaster').click(function() {

			var checked = $('[name="radi"]:radio:checked').val();
			var itemname = $('#itemName').val();
			
			if(itemname.length==0){
				
				 $("#showError").html("Cannot Be Blank").css('color','#D34729');
				
				return;
			}
			else{
				
				
				
				
			}
			
			var itemdesc = $('#itemdesc').val();
			if(itemdesc.length==0){
				
				 $("#showError1").html("Cannot Be Blank").css('color','#D34729');
				 return;
				
			}
			else{
				
				 
				
				
			}
			var unitName = $('#unitName').val();
			if(unitName.length==0){
				
				 $("#showError2").html("Cannot Be Blank").css('color','#D34729');
				
				 return;
			}
			else{
				
			
				
				
			}
			var hsnCode = $('#hsnCode').val();
			if(hsnCode.length==0){
				
				 $("#showError3").html("Cannot Be Blank").css('color','#D34729');
				 return;
				
			}
			else{
				
			
				
				
			}
			var itemgrp = numCorrect($('#itemgrp').val());

			var gstcat = numCorrect($('#gstcat').val());

			var salerate = numCorrect($('#salerate').val());

			var salediscount = numCorrect($('#salediscount').val());

			var purchaserate = numCorrect($('#purchaserate').val());
			var purchasediscount = numCorrect($('#purchasediscount').val());
			var openqty = numCorrect($('#openqty').val());
			var max = numCorrect($('#max').val());
			var min = numCorrect($('#min').val());
			var record = numCorrect($('#record').val());
			
			$.ajax({
				type : "POST",
				url : "itemmasteredit",
				data : {
					id : itemid,
					checked : checked,
					itemname : itemname,
					itemdesc : itemdesc,
					unitName : unitName,
					hsnCode : hsnCode,
					itemgrp : itemgrp,
					gstcat : gstcat,
					salerate : salerate,
					salediscount : salediscount,
					purchaserate : purchaserate,
					purchasediscount : purchasediscount,
					openqty : openqty,
					max : max,
					min : min,
					record : record
				},
				success : function(data) {
					 $("#viewitem").css({'display':'block'});
					 
					 $("#substrip").css({'display':'none'});
					loaddataintoTb(3);
					$("#itemName").val("");
					$("#itemdesc").val("");
					$("#hsnCode").val("");
					$("#itemgrp").val("");
					$("#gstcat").val("");
					$("#salerate").val("");
					$("#salediscount").val("");
					$("#purchaserate").val("");
					$("#openqty").val("");
					$("#max").val("");
					$("#min").val("");
					$("#record").val("");
					$("#unitName").val("");
					$("#purchasediscount").val("");
					
					
				
					 
					
					  $("#itemMasterpop").slideUp(); 
				}
			});
		});

	});

	$(document).ready(
			function() {

				$.ajax({
					type : "GET",
					url : "JqueryServlet",
					data : {

					},
					success : function(data) {

						var m = $.parseJSON(data);

						var $select = $('#itemgrp');
						$(m.data).each(
								function(index, o) {
									var $option = $("<option/>").attr("value",
											o.M_ITEMGROUPID).text(
											o.ITEMGROUP_NAME_VC);
									$select.append($option);
								});
						$('#itemgrp').combobox();

					}
				});
			});

	$(document).ready(
			function() {

				$.ajax({
					type : "GET",
					url : "getgstlist",
					data : {

					},
					success : function(data) {

						var m = $.parseJSON(data);

						var $select = $('#gstcat');
						$(m.data).each(
								function(index, o) {
									var $option = $("<option/>").attr("value",
											o.ID).text(o.GST_DESCRIPTION_VC);
									$select.append($option);
								});
						$('#gstcat').combobox();

					}
				});
			});
</script>
</head>
<body>

	
			<input type="radio" name="radi" id="pro" checked="checked" value="1"><label>Product</label>
			<input type="radio" name="radi" id="ser" value="2"><label>Services</label>
			<div>
				<div class="sidesubdiv">
					<div class="sidesubinpu">
						<div class="textinpu">Item Name</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="text" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="itemName" required="required" placeholder="Item Name">
						</div>
						<span id="showError" style=""> </span>
					</div>

					<div class="sidesubinpu">
						<div class="textinpu">Item Description</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="text" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="itemdesc" required="required" placeholder="Item Description">
						</div>
						<span id="showError1"> </span>
					</div>

					<div class="sidesubinpu">
						<div class="textinpu">Unit Name</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="text" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="unitName" required="required" placeholder="Unit Name">
						</div>
							<span id="showError2"> </span>
					</div>

					<div class="sidesubinpu">
						<div class="textinpu">HSN Code</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="text" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="hsnCode" required="required" placeholder="HsnCode">
						</div>
							<span id="showError3"> </span>
					</div>

					<div
						style='width: 100%; height: 4%; position: relative; top: 3%; padding-bottom: 1.6%; float: left'>
						<div class="textinpuslect" style='margin-left:7.9%;width: 10%;'>Item Group</div>
						<div
							style='width: 60%; height: 100%; float: left; position: relative; left: 8.3%'>
							<select style='width: 100%; height: 30px; margin-left: -14px;' id="itemgrp"></select>

						</div>
					</div>

					<div
						style='width: 100%; height: 4%; position: relative; top: 3%; padding-bottom: 1.6%; float: left'>
						<div class="textinpuslect" style='margin-left:7.9%;width: 10%;'>GST Catogery</div>
						<div
							style='width: 60%; height: 100%; float: left; position: relative; left: 8%'>
							<select style='width: 100%; height: 30px;margin-left: -14px;' id="gstcat"></select>
						</div>
					</div>
				</div>

				<div class="sidesubdiv" style="width: 48%">
					<div class="sidesubinpu">
						<div class="textinpu" style="width: 25.5%">Sales Rate</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="salerate" placeholder="Sale Rate">
						</div>
					</div>
					<div class="sidesubinpu">
						<div style='width: 25.5%' class="textinpu">Sales Discount</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="salediscount" placeholder="Sale Discount">
						</div>
					</div>

					<div class="sidesubinpu">
						<div style='width: 25.5%' class="textinpu">Purchase Rate</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="purchaserate" placeholder="Purchase Rate">
						</div>
					</div>

					<div class="sidesubinpu">
						<div style='width: 25.5%' class="textinpu">Purchase Discount</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="purchasediscount" placeholder="Purchase Discount">
						</div>
					</div>

					<div class="sidesubinpu">
						<div style='width: 25.5%' class="textinpu">Opening Quantity</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="openqty" placeholder="Opening Quantity">
						</div>
					</div>
				</div>
			</div>
			<div style="width: 98%; height: 40%; float: left">
				<div class="midheadng">Stock Level</div>
				<div
					style="width: 4.5%; height: 2px; background-color: #303030; margin-top: 7px; position: relative; left: 14px;"></div>
				<div class="bottomdiv" style=''>
					<div class="sidesubinpu" style=''>
						<div style='width: 24.5%' class="textinpu">Maximum</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="max" placeholder="Maximum">
						</div>
					</div>
					<div class="sidesubinpu" style="padding-bottom: 2.6%;">
						<div style='width: 24.5%;' class="textinpu">Minimum</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="min" placeholder="Minimum">
						</div>
					</div>
				</div>
				<div class="bottomdiv">
					<div class="sidesubinpu" style="padding-bottom: 2.6%">
						<div style='width: 24.5%' class="textinpu">Record</div>
						<div style='width: 60%; height: 100%; float: left'>
							<input type="number" name="FirstName"
								style='width: 100%; border-radius: 4px; height: 30px; border: 1px solid #adadad'
								id="record" placeholder="Record">
						</div>
					</div>
				</div>

			</div>
			<div style="width: 100%; height: 90px; float: left"></div>
			<div
				style="width: 100%; height: 50px; float: left; background-color: #f7f7f7; border: 1px solid #adadad">
				<button class="button"style="float: right; margin-right: 2%; margin-left: 0px;background-color:#303030;margin-top:0.6% !important"
					id="cancel">Cancel</button>
				<button class="button"
					style="display: none; float: right; margin-right:2%; margin-left: 0px; background-color: #3845ae;margin-top:0.6% !important;"
					id="updatemmaster">Update</button>
				<button class="button"
					style="display: none; float: right; margin-right:2%; margin-left: 0px;margin-top:0.6% !important; background-color: #3845ae"
					id="subitemmaster">Create</button>
			</div>
	



</body>
</html>
