<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>master-2</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/pushoff.css">
<link rel="stylesheet" href="css/mains.css">
<script type="text/javascript" src="jquery-3.1.1.min.js"></script>
<style>
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}

html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 60px;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-bottom: 15px;
  width:30%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}

header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover, header button.active { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #303030;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  padding: 8px;
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  padding: 0px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}

aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #4f4f4f;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media only screen and (max-width: 640px){
    body.nav-open aside{
        margin-top: 8.3%
    }  
}
@media only screen and (min-width: 768px){
    body.nav-open aside{
        margin-top: 7.2%
    }  
}
@media only screen and (min-width: 1024px){
    body.nav-open aside{
        margin-top: 5.1%
    }  
}

@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
    width: 100%;
    height: 40px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}

.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-4lign:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}

.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}

.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
 .submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left: 15.4em;
    top:-1.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:55%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}
.texmaindiv{
    width:100%;
    height:6%;
    position:relative;
    margin-top:3%;
    padding-top:0%;
    margin-left:0%
}
.textinpt{
    width:17%;
    height: 100%;
    float: left;
    text-align: right;
    font-weight:bold;
    margin-left:4%;
    margin-top:1%;
    margin-right: 2%;
}

@media only screen and (max-width:640px){
    body.nav-open aside{
        margin-top:8.4%
    }
}
</style>
</head>
<body>

<div  class="mainscrn">
<jsp:include page="layout.jsp"></jsp:include>
    <div  class="mainscrn_1"> 
        <div  class="mainscrn_2">
        <div style='width:100%;height:6%;margin-top:3.5%;margin-left:0%'>
        <div class="textinpt">Name</div>
        <div style='width:70%;height:100%;float:left;position: relative;left:2%'>
       <input type="text" name="company_name_vc" id="company_name_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
              
        <div class="texmaindiv">
        <div class="textinpt">Contact Person</div>
        <div style='width:70%;height:100%;float:left;position: relative;left:0.7%'>
        <input type="text" name="contact_person_vc" id="contact_person_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
             </div>
        </div>
    
        <div class="texmaindiv">
        <div class="textinpt" style="margin-right:2.5%">Opening Balance</div>
        <div style="width:60.3%;height:100%;float:left">
        <input type="text" name="opening_balance_n" id="opening_balance_n" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
     </div>
     <div style="width:10.3%;height:100%;float:left">
     <select name="op_bal_type_vc" id = "op_bal_type_vc"><option value="0">Dr</option value="1"><option>Cr</option></select>
     </div>
              </div>
         
        <div class="texmaindiv">
                <div class='textinpt' style='margin-right:3.9%'>GST Number</div>
        <div style="width:70.2%;height:100%;float:left">
         <input type="text" name="gst_no_vc" id="gst_no_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
              
        <div class="texmaindiv">
        <div class="textinpt">Credit Days</div>
        <div style='width:70%;height:100%;float:left;position: relative;left:0.7%'>
         <input type="text" name="credit_days_n" id="credit_days_n" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
                </div>
        </div>  
              

            
        </div>
      
        <div style="width: 49.5%;height: 100%;background-color:#ffff;float:left;">
        <div style='width:100%;height:6%;margin-top:3.5%;margin-left:0%'>
        <div class="textinpt">Address</div>
        <div style='width:70%;height:100%;float:left;position: relative;left:2%'>
         <input type="text" name="address1_vc" id="address1_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
              
        <div class="texmaindiv">
            <div class="textinpt" style="margin-right:1%">City</div>
        <div style='width:30%;height:100%;float:left;position:relative;left:2%'>
         <select  name="city_vc" id="city_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;position:relative;top:0%;'>
        <option>Here</option>
        <option>The</option>
        <option>The</option>
        </select>
        </div>
         
           <div class="textinpt" style="width:6%">State</div>
        <div style='width:28%;height:100%;float:left;position:relative;left:2%'>
        <select  name="state_vc" id="state_vc"  style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;position:relative;top:0%;'>
        <option>Here</option>
        <option>The</option>
        <option>The</option>
        </select>
        </div>
        </div>
          
        <div class="texmaindiv">
        <div class="textinpt">Phone No.</div>
        <div style='width:70%;height:100%;float:left;position:relative;left:2%'>
        <input type="text" name="phone_no_vc" id="phone_no_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
          
        <div class="texmaindiv">
        <div class="textinpt">Mobile No.</div>
        <div style='width:70%;height:100%;float:left;position:relative;left:2%'>
       <input type="text" name="mobile_no_vc" id="mobile_no_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
          
                
        <div class="texmaindiv">
        <div class="textinpt">Email</div>
        <div style='width:70%;height:100%;float:left;position:relative;left:2%'>
      <input type="text" name="email_id_vc" id="email_id_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
        </div>  
    </div>                     
</div>
        <div style="width:100%;height:28%;position:relative;top:-8%">
        <div class="texmaindiv">
        <div class="textinpt" style="margin-left: 2%;width: 7.2%;font-family:Calibri">Remarks</div>
        <div style='width:85%;height:100%;float:left;position:relative;left:2%'>
       <input type="text" name="remarks_vc" id="remarks_vc" style='width:100%;border-radius:4px;height:28px;border:1px solid #adadad;positon:relative;top:0%'>
        </div>
        </div>
        </div>
        <div style="width:100%;height:8%;background-color:#f7f7f7;border:1px solid #bfbbbb">
        <button class="button" style="float:right;position:relative;left:0px;margin-right:2%;top:8px;background-color:#303030">Cancel</button>
        <button class="button" style="float:right;position:relative;top:8px;background-color:#0057a1" id="btn-submit">Update</button>
        </div>

<script >

    $(document).ready(function(){
		  $('#btn-submit').click(function(){
			 alert("invoked btnsubmit");
			 var company_name_vc=$('#company_name_vc').val();
			 var contact_person_vc=$('#contact_person_vc').val();
			 var opening_balance_n=$('#opening_balance_n').val();
			 var gst_no_vc=$('#gst_no_vc').val();
			 var credit_days_n=$('#credit_days_n').val();
			 var address1_vc=$('#address1_vc').val();
			 var city_vc=$('#city_vc').val();
			 var state_vc=$('#state_vc').val();
			 var phone_no_vc=$('#phone_no_vc').val();
			 var mobile_no_vc=$('#mobile_no_vc').val();
			 var email_id_vc=$('#email_id_vc').val();
			 var remarks_vc=$('#remarks_vc').val();
			 var op_bal_type_vc=$('#op_bal_type_vc').val();
			 alert("invoked btnsubmit2");
			
			    $.ajax({
			        type: "POST",
			        url: "ajaxledger",
			        data: { 
			        	company_name_vc : company_name_vc,
					   contact_person_vc : contact_person_vc,
					   opening_balance_n : opening_balance_n,
					   gst_no_vc : gst_no_vc,
					   credit_days_n : credit_days_n,
					   address1_vc : address1_vc,
					   city_vc : city_vc,
					   state_vc : state_vc,
					   phone_no_vc :phone_no_vc,
					   mobile_no_vc :mobile_no_vc,
					   email_id_vc : email_id_vc,
					   remarks_vc : remarks_vc,
					   op_bal_type_vc : op_bal_type_vc
			         
			        }
			      }).done(function( msg ) {
			    	  location.reload();
			    	 
			      });
			    alert("invoked btnsubmit2");
    });
    });

    
   
   
    </script>    
</body>

</html>
