<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <style>
    .modelouterbody {
     
        position: fixed;
        z-index: 2;
        left: 0;
        top: 0;
        width: 100%;
        height: 90%;
        background-color: rgba(255,255,255,0.4);
        overflow: auto;
    }

    .modeinnerbody {
        background-color: #fefefe;
        border: 1px solid #888;
        box-shadow: 3px 14px 23px 8px rgba(171, 164, 168, 0.62);
        margin-bottom: 0;
        width: 40%; 
        margin-top:10%; 
        height:60%; 
        margin-left: 30%
    }

    .modelheader {
    background-color: #303030;
    font-family: Open Sans;
    font-size: 17px;
    height: 35px;
    width: auto;
    padding: 10px;
    padding-right: 20px;       
    }
    .button {
        background-color: #0e1b47;
        border: none;
        color: white;
        height: 28px;
        width: 77px;
        border-radius: 4px;
        text-align: center;
        position: relative;
        display: inline-block;
        font-size: 13px;
        font-family: arial;
        cursor: pointer;
        margin-left: 3px;
    }
        @media screen and (max-width: 768px){
        .modelouterbody{
        width: 100%;
        padding-top: 30px;
        }
        .modeinnerbody{
        width: 66%;
        height: 40%;
        margin-top: 10%;
        margin-left: 18%;
        }
    }
     .confirmationbox{
     z-index:10 !important;
     }
        </style>
        
        
        
        
        
        
    </head>
    <body>
        
<div class="modelouterbody" id="idfileuploadmodal">
    <div class="modeinnerbody" style='height:28%'>
        <div class="modelheader">
            <div>
            
                <a href="#" id="btnClose" style="color:white; float: right; font-weight: bold; text-decoration: none"><span id="fileclose_VIEW">X</span></a>
                <a href="#" id="btnClose" style="color:white; float: right; font-weight: bold; text-decoration: none;margin-right: 8%"><span id="minimize_VIEW">--</span></a>
            </div>
        </div>
        <div style="padding:25px">
        Item Group Name
       
        <input type="text" style="width:50%; border-radius: 5px; height: 25px;margin-right: 8%;margin-left: 1%;border:1px solid" placeholder="Enter Group Name" id="itemedit" tabindex=1>  <span id="showError"> </span>
        </div>
        <div style="float: right; padding-right: 35px;padding-top:2% ">
         
             <button class="button" id="submitedit" style="display: none" tabindex=2>Update</button>
             
              <button class="button" id="submitcreate" style="display: none" tabindex=3>Create</button>
        <button class="button" id="deletedata" style="display: none" tabindex=4 >Delete</button>
        
        

        
        <button class="button" id="canceledit12">Cancel</button>            
        </div>
    </div>
</div>
        <script>
            //form hide show functionality for buyer,employee,designation,department page
            
      $(document).ready(function(){
    	  $('#showErro').css({'display':'none'});
    	  
    	  
    	  
    	  
		  $('#submitedit').click(function(){
			  
		           editData();
		           
		    });

		function editData(){
		   var itemname = $('#itemedit').val();
		   
		   $('#itemedit').css('border-color','black');
		   
		   
		    $.ajax({
		        type: "POST",
		        url: "ajaxEditGrpName",
		        data: { name : itemname,
		        	id:itemid
		        	}
		      }).done(function( msg ) {
		    	  if (msg.indexOf("[ITEM_NAME_VC]") > -1) {
			    		 $('#itemedit').css('border-color','red');  
			    		 $('#itemedit').css('border-width','1.5px');
			    		} else {
			    			 $('#itemedit').css('border-color','black');
			    			 location.reload();
			    		}
		
		      });
		}
		 $('#deletedata').click(function(){
			
			 doConfirm("Do you Want to Delete?",function Yes(){
				 delData();
				  location.reload(); 
			 },function No(){})
				  
			          
			    });
			function delData(){
			  var name=
			   
			    $.ajax({
			        type: "POST",
			        url: "ajaxDeleteGrpName",
			        data: { id : itemid  }
			      }).done(function( msg ) {
			      
			      });
		
			}
				  $('#submitcreate').click(function(){
				           createData();
				          
				          
				    });
				function createData(){
				   var mge =$('#itemedit').val();
				   if(mge=="")
					   {
					  
					   $("#showError").html("\<b>&#10006</b>");
					   
					   
					   }
				   else{
					   $("#showError").html("\<b>&#10004</b>");
				   }
				    $.ajax({
				        type: "POST",
				        url: "AjaxServlet",
				        data: { message : mge  }
				      }).done(function( msg ) {
				    	
				    	 if (msg.indexOf("[ITEM_NAME_VC]") > -1) {
				    		 $('#itemedit').css('border-color','red');  
				    		 $('#itemedit').css('border-width','1px');
				    		} else {
				    			successicon("your data Successfully Submitted");
				    			 $('#itemedit').css('border-color','black');
				    			 $("#itemedit").val("");
				    		}
				    	  
				      });
				}
	   });
        </script>
    </body>
</html>
