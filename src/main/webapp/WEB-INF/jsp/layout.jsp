<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.ul {
    margin: 0;
    padding: 0;
    background: #2d383e;
    width: 100%;
    height: 100%;
    font-size: 13px;
    font-weight: bold
}

.ul_1{
    width:100%;
    height: 8%;
    float:left;
    padding-bottom:5%;
    cursor: pointer
}
.ul_1:hover{
    background-color: #1d1d1d;
    }
.ul_1_1{
    float:left;
    width: 25%;
    height:100%;
    padding-left: 10%;
    padding-top: 5%;
    margin-right: 8%;
}
.ul_1_2{
    float:left;
    width:50%;
    height:100%;
    padding-top:10%;
    color: white;
    font-size: 13px;
    font-weight: bold;
}
.width0p{
    width:100%;
}

</style>

<jsp:include page="linkall.jsp"></jsp:include>
<script>
$(document).ready(function(){
	$(function(){
	  $(document).on("click","#menu" , function(){
	    $(this).toggleClass("active");
	    $("body").toggleClass("nav-open");   
	  });
	 });
	 });
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="alert alert-info messageshow" id="globalmessage">
                                <span class="close colorwhite" id="iderrorclose" style="margin-top:-14px; color:#0f6aaf;">x</span>            
                                <div>
                                    <div id="msgsucer" style="font-size:16px;text-align:left;padding-left:0px;"></div>
                                </div>
                            </div>
                            <div class="ownmodalconfirm" id="md1">
                                <div class="ownmodal-content-confirmation" id="confirmbody">
                                    <div id="confirmBox" class="confirmationbox">
                                        <div class="confirmationboxheader">Confirmation</div>
                                        <div class="sessionpopbody"><span class="message"></span></div>
                                        <div class="confirmationboxfooter">
                                 <div style="float:left;" class="confirmbutton_margin">
                                 <button class="yes button btnchkbox" style='border-radius:2px'>Yes</button></div>
                               <div style="float:left; padding-left: 14px;margin-top:-3%">
                               <button class="no button btnchkbox" style=border-radius:2px>No</button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    <div style="width:100%;height:100%">
  <div style="width:100%;height:100%">
  <header>
  
      <div style="height:100%;width:8%;float:left;">
      <img src="Icons\Item-Master-2.png" style="width: 60px;position: relative;left: 20%;">
      </div>
    <div class="left" style="padding-top:6px;position:relative;left: -1%">
     <button id="menu" title="Menu0"><i class="fa fa-bars fa-2x" style="color:black;font-size:2em; "></i></button>
    </div>
    <div class="left" style="height:100%;width:12%;">
           <button style="width:60%;height:50%;border: 1px solid black;margin-top:10%;border-radius:4px" id="addnewitemmaster">
             <i class="fa fa-plus" aria-hidden="true" style="color:black;float:left;position:relative;left:8%;font-weight:bold;margin-top:9%"></i>
             <span style="float:left;color:black;position:relative;left:20%;text-align:center;margin-top:-14%;width:60%">Add New</span>
           </button>
    </div>

   <div class="controls">
     
       <input type="text" placeholder="Search" id="tblsearchsearching" name="search" style="height: 35px;width: 90%;border-radius: 6px;border:1px solid black;color: black" tabindex="0" />
    </div>
  </header>
  <!--//Header-->
  
  <div class="content" style="width:100%;height:100%">
      <aside>
       <nav class='site-nav'> 
           <div>
               <div class="ul_1"><div style="" class="ul_1_1"><img src="Icons\home.png" class="width0p"/></div><div style="" class="ul_1_2">Home</div></div>
               <div class="ul_1">
               <div style="" class="ul_1_1"><img src="Icons\dashboard.png" class="width0p"/></div><div class="ul_1_2">Dashboard</div>
               </div>
               
               <div class="ul_1" id="opensubmen1">
                   <div class="submodule" id="submen1">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/">Item Category</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/item">Item</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="">Party</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/party">Party Wise Rate</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\1.png" class="width0p"/></div><div class="ul_1_2">Master</div>
               </div>
               <div class="ul_1" id="opensubmen2">
                   <div class="submodule" id="submen2">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\2.png" class="width0p"/></div><div class="ul_1_2">Purchase</div>
               </div>
               <div class="ul_1" id="opensubmen3">
                   <div class="submodule" id="submen3">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\3.png" class="width0p"/></div><div class="ul_1_2">Sales</div>
               </div>
               <div class="ul_1" id="opensubmen4">
                   <div class="submodule" id="submen4">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Stock in Hand</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Weekly Stock</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item Movement</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\4.png" class="width0p"/></div><div class="ul_1_2">Stock</div>
               </div>
               <div class="ul_1" id="opensubmen5">
                   <div class="submodule" id="submen5">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/sa">Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/gst">Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\5.png" class="width0p"/></div><div class="ul_1_2">GST</div>
               </div>
               <div class="ul_1" id="opensubmen6">
                   <div class="submodule" id="submen6">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="jjhas">Company Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Invoice</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\6.png" class="width0p"/></div><div class="ul_1_2">Setting</div>
               </div>
               <div class="ul_1" id="opensubmen7">
                   <div class="submodule" id="submen7">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Account Group</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Journal Voucher</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Trial Balance</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Profit & Loss</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Balance Sheet</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Ledgers</a></div>    
                       </div>

                   </div>
                   <div class="ul_1_1"><img src="Icons\7.png" class="width0p"/></div><div class="ul_1_2">Account</div>
               </div>
               <div class="ul_1" id="opensubmen8">
                   <div class="submodule" id="submen8">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Create User</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Modify User</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\8.png" class="width0p"/></div><div class="ul_1_2">User</div>
               </div>
               </div>
       </nav>
  
      </aside>  
      <!--//sidebar-->
<section style="width:100%;height:100%">     
      <article>
 <div class="substrp" id="substrip"></div>

<script>
$(document).ready(function(){
	$(function(){
	  $("#menu").on("click" , function(){
	    $(this).toggleClass("active");
	    $("body").toggleClass("nav-open");   
	  });
	 });
    $("#opensubmen1").click(function(){    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen7').hide();
        $('#submen8').hide();
        $("#submen1").toggle();});

        $("#opensubmen2").click(function(){
        $('#submen1').hide();    
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen7').hide();
        $('#submen8').hide();
        $("#submen2").toggle();});

        $("#opensubmen3").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen7').hide();
        $('#submen8').hide();
        $("#submen3").toggle();});

        $("#opensubmen4").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen7').hide();
        $('#submen8').hide();
        $("#submen4").toggle();});

        $("#opensubmen5").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen6').hide();
        $('#submen7').hide();
        $('#submen8').hide();
        $("#submen5").toggle();});

        $("#opensubmen6").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen7').hide();
        $('#submen8').hide();        
        $("#submen6").toggle();});

        $("#opensubmen7").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen8').hide();        
        $("#submen7").toggle();});

        $("#opensubmen8").click(function(){
        $('#submen1').hide();    
        $('#submen2').hide();
        $('#submen3').hide();
        $('#submen4').hide();
        $('#submen5').hide();
        $('#submen6').hide();
        $('#submen7').hide();        
        $("#submen8").toggle();});
	 });
</script>
