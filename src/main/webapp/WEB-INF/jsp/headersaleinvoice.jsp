<style>
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}

html { line-height: 1; }

ol, ul { list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0;
}

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle;
}

q, blockquote { quotes: none; }

q:before, q:after, blockquote:before, blockquote:after {
  content: "";
  content: none;
}

a img { border: none; }

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary { display: block; }

/*-------------------------
  CSS Chapters:
  0 - Variables
  1 - General formatting
  2 - Header
  3 - Sidebar
  4 - Main Content
  5 - Main Site Footer
  6 - Media Queries
-------------------------*/
/*-------------------------
  0 - Variables
-------------------------*/
/*-------------------------
  1 - General formatting
-------------------------*/

* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body, html { height: 100%; }

strong {
  font-weight: 400;
  display: inline-block;
  background-color: #FFD480;
  padding: 0 8px;
}

body {
  padding-top: 60px;
}

body.nav-open section { margin-left: 0; }

body.nav-open aside { left: 0;
 z-index: 1;
 margin-top: 5.1%;
 height:100%;
}

body, h1, h2, h3, p { font-family: "roboto", sans-serif; }

h2 { margin-bottom: 15px; }

.right { float: right; }

.left { float: left; }

.controls {
  position: relative;
  margin-top:1%;
  width:23%;
  float:right;
}

.labeclass{
     position: relative;
    left:8%;
}
a { text-decoration: none; }

.sep {
  content: '';
  border-right: 1px solid rgba(0, 0, 0, 0.13);
}

/*-------------------------
  2 - Header
-------------------------*/

header {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  background:#ffffff;
  height: 70px;
  line-height: 60px;
  color: #fff;
  background-size: 100%;
}

header h1, header button { display: inline-block; }

header h1 {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

header .utils { margin-right: 15px; }

header .utils a {
  display: inline-block;
  padding: 0 5px;
  margin-right: -3px;
}

header .utils a {
  color: rgba(0, 0, 0, 0.3);
  -moz-transition: color 0.3s linear;
  -o-transition: color 0.3s linear;
  -webkit-transition: color 0.3s linear;
  transition: color 0.3s linear;
}

header .utils a:hover { color: rgba(0, 0, 0, 0.6); }

header button {
  cursor: pointer;
  color: #fff;
  -webkit-appearance: none;
  margin: 0;
  padding: 0;
  border: none;
  height: 60px;
  width: 60px;
  vertical-align: top;
  background: transparent;
  margin-right: 15px;
  -moz-transition: background-color 0.3s linear;
  -o-transition: background-color 0.3s linear;
  -webkit-transition: background-color 0.3s linear;
  transition: background-color 0.3s linear;
}

header button:hover, header button:focus, header button.active { outline: none; }

header button:hover { background-color: rgba(0, 0, 0, 0.1); }

/*-------------------------
  3 - Sidebar
-------------------------*/

aside {
  position: fixed;
  height: 100%;
  width: 100%;
  color: #fff;
  left: -100%;
  background-color: #2A3744;
  -moz-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -o-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  -webkit-transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
  transition: left 0.4s ease, width 0.5s cubic-bezier(0.525, -0.35, 0.115, 1.335);
}

aside a {
  color: rgba(255, 255, 255, 0.7);
  font-weight: 300;
  -moz-transition: background-color 0.3s, color 0.3s;
  -o-transition: background-color 0.3s, color 0.3s;
  -webkit-transition: background-color 0.3s, color 0.3s;
  transition: background-color 0.3s, color 0.3s;
}

aside a:hover { color: #fff; }

aside a i, aside a img {
  width: 20px;
  text-align: center;
  margin-right: 6px;
}

aside a, aside input[type="search"] {
  color: #fff;
  display: block;
  font-weight: 300;
  width: 100%;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
}

aside input[type="search"] {
  -webkit-appearance: none;
  border: 1px solid rgba(255, 255, 255, 0.1);
  background-color: #344454;
  width: 100%;
  font-size: 14px;
  padding: 8px;
  padding-left: 25px;
  -moz-transition: background-color 0.3s, border 0.3s;
  -o-transition: background-color 0.3s, border 0.3s;
  -webkit-transition: background-color 0.3s, border 0.3s;
  transition: background-color 0.3s, border 0.3s;
}

aside input[type="search"]:hover, aside input[type="search"]:focus {
  outline: none;
  border: 1px solid rgba(255, 255, 255, 0.2);
  background-color: #3a4b5d;
}

aside input[type="search"] + label {
  position: absolute;
  left: 10px;
  top: 8px;
  color: rgba(0, 0, 0, 0.5);
}

aside .site-nav a { margin-bottom: 3px; }

aside .site-nav a.active, aside .site-nav a:hover { background-color: rgba(0, 0, 0, 0.3); }

aside .site-nav a.active i { color: #24FFCE; }

aside footer {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  position: absolute;
  bottom: 40px;
  left: 0;
  padding-left: 10px;
}

aside footer a { padding: 8px; }

aside footer .avatar img {
  max-width: 20px;
  -moz-border-radius: 100px;
  -webkit-border-radius: 100px;
  border-radius: 100px;
  display: inline-block;
  vertical-align: -3px;
  margin-right: 10px;
}

/*-------------------------
  4 - Main Content
-------------------------*/

section {
  -moz-transition: margin-left 0.4s ease;
  -o-transition: margin-left 0.4s ease;
  -webkit-transition: margin-left 0.4s ease;
  transition: margin-left 0.4s ease;
}

section article { 
    padding: 10px;padding-left:0px;
    padding-right:0px;
      width: 100%;
    height: 100%;
}

section article h2 {
  font-weight: 300;
  font-size: 24px;
}

section article p {
  line-height: 1.5;
  margin-bottom: 10px;
}

/*-------------------------
  5 - Main Site Footer
-------------------------*/

.site-footer {
  background: #dcdcdc;
  width: 100%;
  padding: 0;
  margin: 0;
  height: 30px;
  line-height: 30px;
  padding: 0 10px;
  border-top: 1px solid #ddd;
  font-size: 12px;
}

.site-footer a {
  color: #2A3744;
  display: inline-block;
  margin-right: -4px;
  padding: 0 8px;
}

.site-footer a.feedback { color: #FF870E; }

/*-------------------------
  6 - Media Queries
-------------------------*/
@media (min-width: 500px) {

body.nav-open section { margin-left: 200px; }

aside {
  position: fixed;
  top: 0;
  padding-top: 0px;
  width: 200px;
}

.site-footer {
  position: fixed;
  z-index: 1;
  bottom: 0;
  left: 0;
}
}

.substrp{
      width: 100%;
    height: 30px;
    background-color: #303030;  
}

.sidesubdiv{
    width:50%;
    height:60%;
    float:left;
    position:relative;
    top:20px;
}
.sidesubinpu{
width:100%;height:4%;position:relative;top:3%;padding-bottom:1.6%;position:relative;left:2.8%;float:left
}
.button {
background-color: #0e1b47;
    border: none;
    color: white;
    height: 28px;
    width: 77px;
    border-radius: 8px;
    top:4px;
    text-align: center;
    position: relative;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    font-family: arial;
    cursor: pointer;
    left: 10px;
    float:left;
    margin-left:3%;
}
.textinpu{
 width:22.5%;
 height: 100%;
 float: left;
 padding-left:0%;
 text-4lign:center;
 font-weight:bold;
 position:relative;
 top:7px;
 font-family:Calibri;color:#444242
}
.textinpuslect{
width:17%;
height: 100%;
float: left;
text-align:center;
font-weight:bold;
font-family: Calibri;
position:relative;
top:7px;
color:#444242
}

.midheadng{
font-family:Calibri;font-weight:bold;font-size:19px;color:#3845ae;margin-left:1%;margin-top:4%
}

.bottomdiv{
  width:50%;
  height:30%;
  margin-top:2%;
  float:left  
}
 .submodule{ 
    width: 12em;
    height: 58em;
    float: left;
    position:absolute;
    left:12.5em;
    top:0.5px;
    background-color:#1d1d1d;
    display: none
}
.submen_maindiv{
width:100%;
height:7%
}
.submen_imgdiv{
 width:27%;
 height:100%;
 float:left;
 position:relative;
 left:12%;
 top:1.4em;
}
.submen_texdiv{
width:72%;
height:100%;
float:left;
position:relative;
top:1.4em;
}
.submen_maindiv:hover{
    background-color:#111111
}
.texmaindiv{
    width:100%;
    height:6%;
    position:relative;
    margin-top:3%;
    padding-top:0%;
    margin-left:0%
}
.textinpt{
    width:15%;
    height: 100%;
    float: left;
    font-weight:bold;
    margin-left:4%;
    margin-top:1%
}
    input[type='radio'] {
    -webkit-appearance:none;
    width:25px;
    height:25px;
    border-radius:100%;
    outline:none;
}

input[type='radio']::before {
    content: '';
    display: block;
    width: 25px;
    height: 25px;
    margin: 20% auto;
    border-radius: 100%;
    background: radial-gradient(circle, #f0f0f0 40%, #d8d8d8 30%);
    border: 1px solid #acacac;
}
input[type='radio']:checked::before {
    background: radial-gradient(circle, #e9ffe8 40%, #0e2f44 40%);
    border: 1px solid #3980ad;
}
.checkbxtex{
    width:8.5%;height:100%;float:left
}
.chkbcmain{
    padding-top:6px;
    position:relative;
    left:1%;
    width:63%;
    height:100%
}
</style>
						<div class="alert alert-info messageshow" id="globalmessage">
                                <span class="close colorwhite" id="iderrorclose" style="margin-top:-14px; color:#0f6aaf;">x</span>            
                                <div>
                                    <div id="msgsucer" style="font-size:16px;text-align:left;padding-left:0px;"></div>
                                </div>
                            </div>
                            <div class="ownmodalconfirm" id="md1">
                                <div class="ownmodal-content-confirmation" id="confirmbody">
                                    <div id="confirmBox" class="confirmationbox">
                                        <div class="confirmationboxheader">Confirmation</div>
                                        <div class="sessionpopbody"><span class="message"></span></div>
                                        <div class="confirmationboxfooter">
                                 <div style="float:left;" class="confirmbutton_margin">
                                 <button class="yes button btnchkbox" style='border-radius:2px'>Yes</button></div>
                               <div style="float:left; padding-left: 14px;margin-top:-3%">
                               <button class="no button btnchkbox" style=border-radius:2px>No</button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    <div style="width:100%;height:100%">
  <header>
       
      <div style="height:100%;width:8%;float:left;">
      <img src="Icons\Item-Master-2.png" style="width: 60px;position: relative;left: 20%;">
      </div>
    <div class="left" style="padding-top:6px;position:relative;left: -1%">
     <button id="menu" title="Menu"><i class="fa fa-bars fa-2x" style="color:black;font-size:2em; "></i></button>
    </div>
 
      <!---check box code  -->
    <div class="left chkbcmain">
        <div style="width:13%;height:100%;float: left;MARGIN-RIGHT: 2%;color: black;margin-top:0.7%;font-size:16px;font-weight: bold">GST Category</div>
        <div class="checkbxtex">
               <input type="radio" name="radgst" id="idradiocgst_sgst" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">CGST/SGST</div> 
        </div>
          <div class="checkbxtex">
               <input type="radio" name="radgst" id="idradio_igst" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">IGST</div> 
        </div>
              <div class="checkbxtex">
               <input type="radio" name="radgst" id="idradio_combo" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">Combo</div> 
        </div>
              <div class="checkbxtex">
               <input type="radio" name="radgst" id="idradio_af" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">A/F</div> 
        </div>
        
        
        <div style="width:14%;height:100%;float: left;margin-right:1%;color: black;margin-top:0.7%;font-size:16px;font-weight: bold">Payment Type</div>
        <div class="checkbxtex">
               <input type="radio" name="radpayment" id="idradio_cash" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">Cash</div> 
        </div>
        
          <div class="checkbxtex">
               <input type="radio" name="radpayment" id="idradio_bank" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">Bank</div> 
        </div>
          <div class="checkbxtex">
               <input type="radio" name="radpayment" id="idradio_credit" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">Credit</div> 
        </div>
         
        <div class="checkbxtex">
               <input type="radio" name="radpayment" id="idradio_creditcard" checked="checked" value="1" style="margin:0px;margin-left:11px">
               <div style="width:100%;height: 18%;float: left;color: black;position: relative;top:-53%;font-size:13px">C/D Card</div> 
        </div>
        
        </div>
      
   <div class="controls">
       <label for="search" class="labeclass"><i class="fa fa-search" style="color:black"></i></label>
       <input type="search" id="search" name="search" style="height: 35px;width: 90%;border-radius: 6px;border:1px solid black;" />
    </div>
  </header>
  <!--//Header-->
  
   
  <div class="content" style="width:100%;height:100%">
      <aside>
       <nav class='site-nav'> 
           <div>
               <div class="ul_1"><div style="" class="ul_1_1"><img src="Icons\home.png" class="width0p"/></div><div style="" class="ul_1_2">Home</div></div>
               <div class="ul_1">
               <div style="" class="ul_1_1"><img src="Icons\dashboard.png" class="width0p"/></div><div class="ul_1_2">Dashboard</div>
               </div>
               
               <div class="ul_1" id="opensubmen1">
                   <div class="submodule" id="submen1">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item Category</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Party</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Party Wise Rate</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\1.png" class="width0p"/></div><div class="ul_1_2">Master</div>
               </div>
               <div class="ul_1" id="opensubmen2">
                   <div class="submodule" id="submen2">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Purchase Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\2.png" class="width0p"/></div><div class="ul_1_2">Purchase</div>
               </div>
               <div class="ul_1" id="opensubmen3">
                   <div class="submodule" id="submen3">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="/saleinvoice">Sales Invoice</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Return</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Sales Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\3.png" class="width0p"/></div><div class="ul_1_2">Sales</div>
               </div>
               <div class="ul_1" id="opensubmen4">
                   <div class="submodule" id="submen4">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Stock in Hand</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Weekly Stock</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Item Movement</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\4.png" class="width0p"/></div><div class="ul_1_2">Stock</div>
               </div>
               <div class="ul_1" id="opensubmen5">
                   <div class="submodule" id="submen5">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="">Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Register</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\5.png" class="width0p"/></div><div class="ul_1_2">GST</div>
               </div>
               <div class="ul_1" id="opensubmen6">
                   <div class="submodule" id="submen6">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="jjhas">Company Setting</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="google.com">Sales Invoice</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\6.png" class="width0p"/></div><div class="ul_1_2">Setting</div>
               </div>
               <div class="ul_1" id="opensubmen7">
                   <div class="submodule" id="submen7">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="">Account Group</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Journal Voucher</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Trial Balance</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Profit & Loss</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Balance Sheet</a></div>    
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Ledgers</a></div>    
                       </div>

                   </div>
                   <div class="ul_1_1"><img src="Icons\7.png" class="width0p"/></div><div class="ul_1_2">Account</div>
               </div>
               <div class="ul_1" id="opensubmen8">
                   <div class="submodule" id="submen8">
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Create User</a></div>
                       </div>
                       <div class="submen_maindiv"><div class="submen_imgdiv" style=""><i class="fa fa-clone" aria-hidden="true"></i></div>
                           <div style="" class="submen_texdiv"><a href="#">Modify User</a></div>    
                       </div>
                   </div>
                   <div class="ul_1_1"><img src="Icons\8.png" class="width0p"/></div><div class="ul_1_2">User</div>
               </div>
               </div>
       </nav>
   
      </aside>  
      <!--//sidebar-->
<section style="width:100%;height:100%">     
      <article>
<div style="width:100%;height:100%;">


<script>
    $(document).ready(function(){
$(function(){
  $("#menu").on("click" , function(){
    $(this).toggleClass("active");
    $("body").toggleClass("nav-open");   
  });
  $('#iderrorclose').click(function () {                        //-----error message close-------
      $('#globalmessage').css('display', 'none');
  })
 });
 });
</script>
<!--- <script>
    $(document).ready(function() {
    $("#opensubmen").click(function(){
    $("#submen1").toggle();
        });
});
</script>--->
<script>
    $(document).ready(function() {
    $("#opensubmen1").click(function(){    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen1").toggle();});

    $("#opensubmen2").click(function(){
    $('#submen1').hide();    
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen2").toggle();});

    $("#opensubmen3").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen3").toggle();});

    $("#opensubmen4").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen4").toggle();});

    $("#opensubmen5").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen6').hide();
    $('#submen7').hide();
    $('#submen8').hide();
    $("#submen5").toggle();});

    $("#opensubmen6").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen7').hide();
    $('#submen8').hide();        
    $("#submen6").toggle();});

    $("#opensubmen7").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen8').hide();        
    $("#submen7").toggle();});

    $("#opensubmen8").click(function(){
    $('#submen1').hide();    
    $('#submen2').hide();
    $('#submen3').hide();
    $('#submen4').hide();
    $('#submen5').hide();
    $('#submen6').hide();
    $('#submen7').hide();        
    $("#submen8").toggle();});
});

</script>




