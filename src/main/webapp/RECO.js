var globalmsginterval;	
function numCorrect(val)
{
if(val==""||val==null)
	{
	return val=0;
	}
else{
	return val;
}
}
function getNum(val) {
		   var values=parseFloat(val)
		   if (isNaN(values)) {
		     return 0;
		   }
		   return parseFloat(values.toFixed(2));
		}
	function getRound(val){
		var num=0;
		num=parseFloat(parseFloat(val).toFixed(2));
		return num;
	}
	function doConfirm(msg, yesFn, noFn) {//yesFn yes function,noFn no function pass from form
	    var modal1 = document.getElementById('md1');/*confirmation box div id*/
	    modal1.style.display = "block";//display div
	    var confirmBox = $("#confirmBox");
	    confirmBox.find(".message").html(msg);
	    confirmBox.find(".yes,.no").unbind().click(function () {
	        $('#md1').hide();
	    });
	    confirmBox.find(".yes").click(yesFn);
	    confirmBox.find(".no").click(noFn);
	    $('.yes').focus();
	    confirmBox.show();   
	}
	function errormicon(msg) {          //error message display
	    if (msg != "") {
	        clearInterval(globalmsginterval);
	        globalmsginterval = setInterval(function () {
	            $('#globalmessage').slideUp();
	        }, 8000);
	        //$("globalmessage").hide();
	        //$("globalmessage").show(1000);
	        $('#globalmessage').css("color", "#000");                               //gp
	        $('#globalmessage').css("background-color", "#fff");                   //gp
	        $('#globalmessage').css("border-color", "#fff");                      //gp
	        $('#globalmessage').css("margin-top", "22px");                          //gp
	        $('#globalmessage').css("border-radius", "0px");                      //gp
	        $('#iderrorclose').css("color", "0f6aaf");                            //gp

	        //$('#globalmessage').removeClass('alert-success messagebordererror');
	        //$('#globalmessage').addClass('alert-danger')
	        //$('#iderror').remove(); //remove previous add icon
	        //$('#iconmsgsucer').append('<img id="iderror" src="/Content/image/error.png"/>')
	        $('#iderror').attr('src', "/Content/image/error.png");
	        $('#msgsucer').html(msg)
	        $('#globalmessage').css('display', 'none');
	        $('#globalmessage').css('display', 'block');
	    }
	}
	function completeString(val)
	{
		
		if(val==""||val==null){
			
			return val=null;
			
		}
		else{return val;}
	}
	
	function successicon(msg) {    //success message display
	    if (msg != "") {
	        clearInterval(globalmsginterval);
	        globalmsginterval = setInterval(function () {
	            $('#globalmessage').slideUp();
	        }, 8000);
	        //$("globalmessage").hide();
	        //$("globalmessage").show(1000);
	        //$('#globalmessage').addClass('alert-success messageborder');
	        //$('#globalmessage').removeClass('alert-danger');
	        $('#iderrorclose').css("color", "#3c763d");                                //gp
	        $('#globalmessage').css("background-color", "#fff");                   //gp
	        $('#globalmessage').css("border-color", "#fff");                      //gp
	        $('#globalmessage').css("margin-top", "22px");                          //gp
	        $('#globalmessage').css("color", "#000");                           //gp
	        $('#globalmessage').css("border-radius", "0px");                     //gp
	        //$('#iderror').attr('src', '/Content/image/success.png');
	        //$('#iderror').remove();
	        //$('#iconmsgsucer').append('<img id="iderror" src="/Content/image/success.png"/>')
	        $('#msgsucer').html(msg)
	        $('#globalmessage').css('display', 'none');
	        $('#globalmessage').css('display', 'block');
	    }
	}