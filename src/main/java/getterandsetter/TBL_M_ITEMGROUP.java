package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;

public class TBL_M_ITEMGROUP {
	@NotNull(message = "ITEM_NAME_VC")
	public String itemname;
	public  int id;
	public TBL_M_ITEMGROUP(String itemname) {
		
		this.itemname = itemname;
	}
	
	public TBL_M_ITEMGROUP(String itemname, int id) {
		this.itemname = itemname;
		this.id = id;
	}

	public TBL_M_ITEMGROUP() {
		
	}

	public TBL_M_ITEMGROUP( int id) {
		
		this.id = id;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public ArrayList checkvalidate(TBL_M_ITEMGROUP t1)
	{
	
		ArrayList al=new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		TBL_M_ITEMGROUP t=new TBL_M_ITEMGROUP(t1.getItemname());
		
		Set<ConstraintViolation<TBL_M_ITEMGROUP>> violations = validator.validate(t);
		for (ConstraintViolation<TBL_M_ITEMGROUP> violation : violations) {
		    al.add(violation.getMessage()); 
		}
		
		return al;}	

}