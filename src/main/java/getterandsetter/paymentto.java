package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class paymentto {

	public int id;

	@Min(value = 1, message = "SUBGROUP_ID_N")
	int SUBGROUP_ID_N;
	@Min(value = 1, message = "Cannot be Zero")
	public double V_NO_N;
	@Min(value = 1, message = "AMOUNT_N")
	public double AMOUNT_N;
	@NotNull(message = "CHEQUE_NO_VC")
	public String CHEQUE_NO_VC;
	@NotNull(message = "V_DATE_D")
	public String V_DATE_D;
	@NotNull(message = "CHEQUE_DATE_VC")
	public String CHEQUE_DATE_VC;
	@NotNull(message = "NARRATION_VC")
	public String NARRATION_VC;
	@NotNull(message = "BANK_vc")
	public String BANK_vc;
	@NotNull(message = "MODE_N")
	public String MODE_N;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSUBGROUP_ID_N() {
		return SUBGROUP_ID_N;
	}

	public void setSUBGROUP_ID_N(int sUBGROUP_ID_N) {
		SUBGROUP_ID_N = sUBGROUP_ID_N;
	}

	public String getNARRATION_VC() {
		return NARRATION_VC;
	}

	public void setNARRATION_VC(String nARRATION_VC) {
		NARRATION_VC = nARRATION_VC;
	}

	public String getMODE_N() {
		return MODE_N;
	}

	public void setMODE_N(String mODE_N) {
		MODE_N = mODE_N;
	}

	public String getBANK_vc() {
		return BANK_vc;
	}

	public void setBANK_vc(String bANK_vc) {
		BANK_vc = bANK_vc;
	}

	public double getAMOUNT_N() {
		return AMOUNT_N;
	}

	public void setAMOUNT_N(double aMOUNT_N) {
		AMOUNT_N = aMOUNT_N;
	}

	public double getV_NO_N() {
		return V_NO_N;
	}

	public void setV_NO_N(double v_NO_N) {
		V_NO_N = v_NO_N;
	}

	public String getCHEQUE_NO_VC() {
		return CHEQUE_NO_VC;
	}

	public void setCHEQUE_NO_VC(String cHEQUE_NO_VC) {
		CHEQUE_NO_VC = cHEQUE_NO_VC;
	}

	public String getV_DATE_D() {
		return V_DATE_D;
	}

	public void setV_DATE_D(String v_DATE_D) {
		V_DATE_D = v_DATE_D;
	}

	public String getCHEQUE_DATE_VC() {
		return CHEQUE_DATE_VC;
	}

	public void setCHEQUE_DATE_VC(String cHEQUE_DATE_VC) {
		CHEQUE_DATE_VC = cHEQUE_DATE_VC;
	}

	public ArrayList checkvalidate(paymentto t1) {

		ArrayList al = new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		paymentto payment = new paymentto();
		payment.setMODE_N(t1.getMODE_N());
		payment.setSUBGROUP_ID_N(t1.getSUBGROUP_ID_N());
		payment.setV_NO_N(t1.getV_NO_N());
		payment.setAMOUNT_N(t1.getAMOUNT_N());

		payment.setBANK_vc(t1.getBANK_vc());
		;
		payment.setV_DATE_D(t1.getV_DATE_D());
		payment.setCHEQUE_DATE_VC(t1.getCHEQUE_DATE_VC());
		payment.setCHEQUE_NO_VC(t1.getCHEQUE_NO_VC());
		payment.setNARRATION_VC(t1.getNARRATION_VC());

		Set<ConstraintViolation<paymentto>> violations = validator.validate(payment);
		for (ConstraintViolation<paymentto> violation : violations) {
			al.add(violation.getMessage());
		}

		return al;
	}

}
