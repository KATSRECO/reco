package getterandsetter;

public class userMaster {

	int USER_ID_N;
	String USER_NAMC_VC,PASSWORD_VC;
	int C_USER_ID_N;
	
	
	
	public userMaster() {
		super();
	}

	public userMaster(int uSER_ID_N, String uSER_NAMC_VC, String pASSWORD_VC, int c_USER_ID_N) {
		super();
		USER_ID_N = uSER_ID_N;
		USER_NAMC_VC = uSER_NAMC_VC;
		PASSWORD_VC = pASSWORD_VC;
		C_USER_ID_N = c_USER_ID_N;
	}
	
	public int getUSER_ID_N() {
		return USER_ID_N;
	}
	public void setUSER_ID_N(int uSER_ID_N) {
		USER_ID_N = uSER_ID_N;
	}
	public String getUSER_NAMC_VC() {
		return USER_NAMC_VC;
	}
	public void setUSER_NAMC_VC(String uSER_NAMC_VC) {
		USER_NAMC_VC = uSER_NAMC_VC;
	}
	public String getPASSWORD_VC() {
		return PASSWORD_VC;
	}
	public void setPASSWORD_VC(String pASSWORD_VC) {
		PASSWORD_VC = pASSWORD_VC;
	}
	public int getC_USER_ID_N() {
		return C_USER_ID_N;
	}
	public void setC_USER_ID_N(int c_USER_ID_N) {
		C_USER_ID_N = c_USER_ID_N;
	}
	
	
}
