package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class gst_SettingEntry {

	int gstid;
	@NotNull(message = "gstdesc")
public String gstdesc;
	 @Min(value = 1, message = "gstPercent")
	public double gstPercent ;
	public int getGstid() {
		return gstid;
	}

	public void setGstid(int gstid) {
		this.gstid = gstid;
	}

	

	
	
	public gst_SettingEntry() {
		super();
	}

	public gst_SettingEntry(String gstdesc, double gstPercent) {
		super();
		this.gstdesc = gstdesc;
		this.gstPercent = gstPercent;
	}

	public String getGstdesc() {
		return gstdesc;
	}

	public void setGstdesc(String gstdesc) {
		this.gstdesc = gstdesc;
	}

	public double getGstPercent() {
		return gstPercent;
	}

	public void setGstPercent(double gstPercent) {
		this.gstPercent = gstPercent;
	}
	
	
	public ArrayList checkvalidate(gst_SettingEntry t1)
	{
	
		ArrayList al=new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		gst_SettingEntry gst=new gst_SettingEntry();
		gst.setGstPercent(t1.getGstPercent());
		gst.setGstdesc(t1.getGstdesc());
		
		
		Set<ConstraintViolation<gst_SettingEntry>> violations = validator.validate(gst);
		for (ConstraintViolation<gst_SettingEntry> violation : violations) {
		    al.add(violation.getMessage()); 
		}
		
		return al;}
	
}
