package getterandsetter;

public class TBL_O_SALE_BILL {
int INVOICE_NO_N,INVOICE_TYPE_N,M_ITEM_ID;
double ITEM_QUANTITY_N,RATE_N,SGST_AMOUNT_N,SGST_PERCENT_N,CGST_AMOUNT_N,CGST_PERCENT_N,IGST_AMOUNT_N,IGST_PERCENT_N,DISC_PERCENT_N,DISC_AMOUNT_N,AMOUNT_N;
String ITEM_DESC_VC;
public int getINVOICE_NO_N() {
	return INVOICE_NO_N;
}
public void setINVOICE_NO_N(int iNVOICE_NO_N) {
	INVOICE_NO_N = iNVOICE_NO_N;
}
public int getINVOICE_TYPE_N() {
	return INVOICE_TYPE_N;
}
public void setINVOICE_TYPE_N(int iNVOICE_TYPE_N) {
	INVOICE_TYPE_N = iNVOICE_TYPE_N;
}
public int getM_ITEM_ID() {
	return M_ITEM_ID;
}
public void setM_ITEM_ID(int m_ITEM_ID) {
	M_ITEM_ID = m_ITEM_ID;
}
public double getITEM_QUANTITY_N() {
	return ITEM_QUANTITY_N;
}
public void setITEM_QUANTITY_N(double iTEM_QUANTITY_N) {
	ITEM_QUANTITY_N = iTEM_QUANTITY_N;
}
public double getRATE_N() {
	return RATE_N;
}
public void setRATE_N(double rATE_N) {
	RATE_N = rATE_N;
}
public double getSGST_AMOUNT_N() {
	return SGST_AMOUNT_N;
}
public void setSGST_AMOUNT_N(double sGST_AMOUNT_N) {
	SGST_AMOUNT_N = sGST_AMOUNT_N;
}
public double getSGST_PERCENT_N() {
	return SGST_PERCENT_N;
}
public void setSGST_PERCENT_N(double sGST_PERCENT_N) {
	SGST_PERCENT_N = sGST_PERCENT_N;
}
public double getCGST_AMOUNT_N() {
	return CGST_AMOUNT_N;
}
public void setCGST_AMOUNT_N(double cGST_AMOUNT_N) {
	CGST_AMOUNT_N = cGST_AMOUNT_N;
}
public double getCGST_PERCENT_N() {
	return CGST_PERCENT_N;
}
public void setCGST_PERCENT_N(double cGST_PERCENT_N) {
	CGST_PERCENT_N = cGST_PERCENT_N;
}
public double getIGST_AMOUNT_N() {
	return IGST_AMOUNT_N;
}
public void setIGST_AMOUNT_N(double iGST_AMOUNT_N) {
	IGST_AMOUNT_N = iGST_AMOUNT_N;
}
public double getIGST_PERCENT_N() {
	return IGST_PERCENT_N;
}
public void setIGST_PERCENT_N(double iGST_PERCENT_N) {
	IGST_PERCENT_N = iGST_PERCENT_N;
}
public double getDISC_PERCENT_N() {
	return DISC_PERCENT_N;
}
public void setDISC_PERCENT_N(double dISC_PERCENT_N) {
	DISC_PERCENT_N = dISC_PERCENT_N;
}
public double getDISC_AMOUNT_N() {
	return DISC_AMOUNT_N;
}
public void setDISC_AMOUNT_N(double dISC_AMOUNT_N) {
	DISC_AMOUNT_N = dISC_AMOUNT_N;
}
public double getAMOUNT_N() {
	return AMOUNT_N;
}
public void setAMOUNT_N(double aMOUNT_N) {
	AMOUNT_N = aMOUNT_N;
}
public String getITEM_DESC_VC() {
	return ITEM_DESC_VC;
}
public void setITEM_DESC_VC(String iTEM_DESC_VC) {
	ITEM_DESC_VC = iTEM_DESC_VC;
}



}
