package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;

public class TBL_M_SUBGROUP {
	
	//COMPANYNAME,MOBILE
	@NotNull(message = "COMPANY_NAME_VC")
	public String COMPANY_NAME_VC;
	 String CONTACT_PERSON_VC,ADDRESS1_VC,       
	 ADDRESS2_VC,       
	 PHONE_NO_VC ;
	 @NotNull(message = "COMPANY_NAME_VC")
	public String MOBILE_NO_VC ;     
	public String EMAIL_ID_VC,  
	 STATE_VC,      
	 CITY_VC ,          
	 GST_NO_VC,
	 NATURE_VC,REMARKS_VC,OP_BAL_TYPE_VC,MODIFY_DATE_D,CREATE_DATE_D;
	 
	 int SUBGROUP_ID,ACC_GROUP_ID,
	 CREDIT_DAYS_N, OPENING_BALANCE_N, USER_ID_N   ;
	 
	 
	 
public String getCOMPANY_NAME_VC() {
		return COMPANY_NAME_VC;
	}



	public void setCOMPANY_NAME_VC(String cOMPANY_NAME_VC) {
		COMPANY_NAME_VC = cOMPANY_NAME_VC;
	}



	public String getCONTACT_PERSON_VC() {
		return CONTACT_PERSON_VC;
	}



	public void setCONTACT_PERSON_VC(String cONTACT_PERSON_VC) {
		CONTACT_PERSON_VC = cONTACT_PERSON_VC;
	}



	public String getADDRESS1_VC() {
		return ADDRESS1_VC;
	}



	public void setADDRESS1_VC(String aDDRESS1_VC) {
		ADDRESS1_VC = aDDRESS1_VC;
	}



	public String getADDRESS2_VC() {
		return ADDRESS2_VC;
	}



	public void setADDRESS2_VC(String aDDRESS2_VC) {
		ADDRESS2_VC = aDDRESS2_VC;
	}



	public String getPHONE_NO_VC() {
		return PHONE_NO_VC;
	}



	public void setPHONE_NO_VC(String pHONE_NO_VC) {
		PHONE_NO_VC = pHONE_NO_VC;
	}



	public String getMOBILE_NO_VC() {
		return MOBILE_NO_VC;
	}



	public void setMOBILE_NO_VC(String mOBILE_NO_VC) {
		MOBILE_NO_VC = mOBILE_NO_VC;
	}



	public String getEMAIL_ID_VC() {
		return EMAIL_ID_VC;
	}



	public void setEMAIL_ID_VC(String eMAIL_ID_VC) {
		EMAIL_ID_VC = eMAIL_ID_VC;
	}



	public String getSTATE_VC() {
		return STATE_VC;
	}



	public void setSTATE_VC(String sTATE_VC) {
		STATE_VC = sTATE_VC;
	}



	public String getCITY_VC() {
		return CITY_VC;
	}



	public void setCITY_VC(String cITY_VC) {
		CITY_VC = cITY_VC;
	}



	public String getGST_NO_VC() {
		return GST_NO_VC;
	}



	public void setGST_NO_VC(String gST_NO_VC) {
		GST_NO_VC = gST_NO_VC;
	}



	public String getNATURE_VC() {
		return NATURE_VC;
	}



	public void setNATURE_VC(String nATURE_VC) {
		NATURE_VC = nATURE_VC;
	}



	public String getREMARKS_VC() {
		return REMARKS_VC;
	}



	public void setREMARKS_VC(String rEMARKS_VC) {
		REMARKS_VC = rEMARKS_VC;
	}



	public String getOP_BAL_TYPE_VC() {
		return OP_BAL_TYPE_VC;
	}



	public void setOP_BAL_TYPE_VC(String oP_BAL_TYPE_VC) {
		OP_BAL_TYPE_VC = oP_BAL_TYPE_VC;
	}



	public String getMODIFY_DATE_D() {
		return MODIFY_DATE_D;
	}



	public void setMODIFY_DATE_D(String mODIFY_DATE_D) {
		MODIFY_DATE_D = mODIFY_DATE_D;
	}



	public String getCREATE_DATE_D() {
		return CREATE_DATE_D;
	}



	public void setCREATE_DATE_D(String cREATE_DATE_D) {
		CREATE_DATE_D = cREATE_DATE_D;
	}



	public int getSUBGROUP_ID() {
		return SUBGROUP_ID;
	}



	public void setSUBGROUP_ID(int sUBGROUP_ID) {
		SUBGROUP_ID = sUBGROUP_ID;
	}



	public int getACC_GROUP_ID() {
		return ACC_GROUP_ID;
	}



	public void setACC_GROUP_ID(int aCC_GROUP_ID) {
		ACC_GROUP_ID = aCC_GROUP_ID;
	}



	public int getCREDIT_DAYS_N() {
		return CREDIT_DAYS_N;
	}



	public void setCREDIT_DAYS_N(int cREDIT_DAYS_N) {
		CREDIT_DAYS_N = cREDIT_DAYS_N;
	}



	public int getOPENING_BALANCE_N() {
		return OPENING_BALANCE_N;
	}



	public void setOPENING_BALANCE_N(int oPENING_BALANCE_N) {
		OPENING_BALANCE_N = oPENING_BALANCE_N;
	}



	public int getUSER_ID_N() {
		return USER_ID_N;
	}



	public void setUSER_ID_N(int uSER_ID_N) {
		USER_ID_N = uSER_ID_N;
	}


	public ArrayList checkvalidate(TBL_M_SUBGROUP ts)
	{
	
		ArrayList al=new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		TBL_M_SUBGROUP t=new TBL_M_SUBGROUP();
	t.setCOMPANY_NAME_VC(ts.getCOMPANY_NAME_VC());
	t.setMOBILE_NO_VC(ts.getMOBILE_NO_VC());
		Set<ConstraintViolation<TBL_M_SUBGROUP>> violations = validator.validate(t);
		for (ConstraintViolation<TBL_M_SUBGROUP> violation : violations) {
		    al.add(violation.getMessage()); 
		}
		
		return al;}
	/*constructor*/
	 public TBL_M_SUBGROUP() {
	
	
}

}
