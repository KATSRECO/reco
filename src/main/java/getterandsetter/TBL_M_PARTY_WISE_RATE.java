package getterandsetter;

public class TBL_M_PARTY_WISE_RATE {

	int RATE_ID,SUBGROUP_ID,M_ITEM_ID,USER_ID_N;
	float RATE_N;
	String CREATE_DATE_D,MODIFY_DATE_D;
	public int getRATE_ID() {
		return RATE_ID;
	}
	public void setRATE_ID(int rATE_ID) {
		RATE_ID = rATE_ID;
	}
	public int getSUBGROUP_ID() {
		return SUBGROUP_ID;
	}
	public void setSUBGROUP_ID(int sUBGROUP_ID) {
		SUBGROUP_ID = sUBGROUP_ID;
	}
	public int getM_ITEM_ID() {
		return M_ITEM_ID;
	}
	public void setM_ITEM_ID(int m_ITEM_ID) {
		M_ITEM_ID = m_ITEM_ID;
	}
	public int getUSER_ID_N() {
		return USER_ID_N;
	}
	public void setUSER_ID_N(int uSER_ID_N) {
		USER_ID_N = uSER_ID_N;
	}
	public float getRATE_N() {
		return RATE_N;
	}
	public void setRATE_N(float rATE_N) {
		RATE_N = rATE_N;
	}
	public String getCREATE_DATE_D() {
		return CREATE_DATE_D;
	}
	public void setCREATE_DATE_D(String cREATE_DATE_D) {
		CREATE_DATE_D = cREATE_DATE_D;
	}
	public String getMODIFY_DATE_D() {
		return MODIFY_DATE_D;
	}
	public void setMODIFY_DATE_D(String mODIFY_DATE_D) {
		MODIFY_DATE_D = mODIFY_DATE_D;
	}
	
}
