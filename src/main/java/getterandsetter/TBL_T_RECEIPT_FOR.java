package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

public class TBL_T_RECEIPT_FOR {
	
	
	public int RECEIPT_ID;

	 @Min(value = 1, message = "Cannot be Zero")
	public int SUBGROUP_ID_N;

	 @Min(value = 1, message = "Cannot be Zero")
	public int MODE_N;
	
	 @Min(value = 1, message = "Cannot be Zero")
	public int USER_ID_N;
	
	@NotNull(message = "CARD_NO_VC")
	public String CARD_NO_VC;
	
	@NotNull(message = "TRANSACTION_REF_VC")
	public String TRANSACTION_REF_VC;
	
	@NotNull(message = "TRANSACTION_DATE_D")
	public String TRANSACTION_DATE_D;
	
	@NotNull(message = "V_DATE_D")
	public String V_DATE_D;
	
	@NotNull(message = "CREATE_DATE_D")
	public String CREATE_DATE_D;
	
	@NotNull(message = "MODIFY_DATE_D")
	public String MODIFY_DATE_D;
	
	 @Min(value = 1, message = "Cannot be Zero")
	public double V_NO_N;
	
	 @Min(value = 1, message = "Cannot be Zero")
	public double AMOUNT_N;
	
	@NotNull(message = "NARRATION_VC")
	public String NARRATION_VC;

	public int getRECEIPT_ID() {
		return RECEIPT_ID;
	}

	public void setRECEIPT_ID(int rECEIPT_ID) {
		RECEIPT_ID = rECEIPT_ID;
	}

	public int getSUBGROUP_ID_N() {
		return SUBGROUP_ID_N;
	}

	public void setSUBGROUP_ID_N(int sUBGROUP_ID_N) {
		SUBGROUP_ID_N = sUBGROUP_ID_N;
	}

	public int getMODE_N() {
		return MODE_N;
	}

	public void setMODE_N(int mODE_N) {
		MODE_N = mODE_N;
	}

	public int getUSER_ID_N() {
		return USER_ID_N;
	}

	public void setUSER_ID_N(int uSER_ID_N) {
		USER_ID_N = uSER_ID_N;
	}

	public String getCARD_NO_VC() {
		return CARD_NO_VC;
	}

	public void setCARD_NO_VC(String cARD_NO_VC) {
		CARD_NO_VC = cARD_NO_VC;
	}

	public String getTRANSACTION_REF_VC() {
		return TRANSACTION_REF_VC;
	}

	public void setTRANSACTION_REF_VC(String tRANSACTION_REF_VC) {
		TRANSACTION_REF_VC = tRANSACTION_REF_VC;
	}

	public String getTRANSACTION_DATE_D() {
		return TRANSACTION_DATE_D;
	}

	public void setTRANSACTION_DATE_D(String tRANSACTION_DATE_D) {
		TRANSACTION_DATE_D = tRANSACTION_DATE_D;
	}

	public String getV_DATE_D() {
		return V_DATE_D;
	}

	public void setV_DATE_D(String v_DATE_D) {
		V_DATE_D = v_DATE_D;
	}

	public String getNARRATION_VC() {
		return NARRATION_VC;
	}

	public void setNARRATION_VC(String nARRATION_VC) {
		NARRATION_VC = nARRATION_VC;
	}

	public String getCREATE_DATE_D() {
		return CREATE_DATE_D;
	}

	public void setCREATE_DATE_D(String cREATE_DATE_D) {
		CREATE_DATE_D = cREATE_DATE_D;
	}

	public String getMODIFY_DATE_D() {
		return MODIFY_DATE_D;
	}

	public void setMODIFY_DATE_D(String mODIFY_DATE_D) {
		MODIFY_DATE_D = mODIFY_DATE_D;
	}

	public double getV_NO_N() {
		return V_NO_N;
	}

	public void setV_NO_N(double v_NO_N) {
		V_NO_N = v_NO_N;
	}

	public double getAMOUNT_N() {
		return AMOUNT_N;
	}

	public void setAMOUNT_N(double aMOUNT_N) {
		AMOUNT_N = aMOUNT_N;
	}
	
	public ArrayList checkvalidate(TBL_T_RECEIPT_FOR t1)
	{
	
		ArrayList al=new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		TBL_T_RECEIPT_FOR recfor=new TBL_T_RECEIPT_FOR();
		recfor.setMODE_N(t1.getMODE_N());
		recfor.setSUBGROUP_ID_N(t1.getSUBGROUP_ID_N());

		recfor.setV_NO_N(t1.getV_NO_N());
		recfor.setV_DATE_D(t1.getV_DATE_D());
		recfor.setAMOUNT_N(t1.getAMOUNT_N());
		recfor.setCARD_NO_VC(t1.getCARD_NO_VC());
		recfor.setTRANSACTION_REF_VC(t1.getTRANSACTION_REF_VC());
		recfor.setTRANSACTION_DATE_D(t1.getTRANSACTION_DATE_D());
		recfor.setNARRATION_VC(t1.getNARRATION_VC());
		
		
		Set<ConstraintViolation<TBL_T_RECEIPT_FOR>> violations = validator.validate(recfor);
		for (ConstraintViolation<TBL_T_RECEIPT_FOR> violation : violations) {
		    al.add(violation.getMessage()); 
		}
		
		return al;}

}
