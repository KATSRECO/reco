package getterandsetter;

public class TBL_T_SALE_BILL {

	public TBL_T_SALE_BILL() {
		
	}
	int GST_CATEGORY_ID_N,INVOICE_TYPE_N,SUBGROUP_ID,SHIPTO_ID_N;
	String INVOICE_DATE_D,PAYMENT_TYPE_VC;
	double INVOICE_NO_N,TOTAL_AMOUNT_N,DISC_PERCENT_N,DISC_AMOUNT_N,CGST_AMOUNT_N,CGST_PERCENT_N,SGST_AMOUNT_N,SGST_PERCENT_N,CARTAGE_N,ROUND_OFF_PERCENT_N,ROUND_OFF_AMOUNT_N,GRAND_TOTAL_N,IGST_PERCENT_N,IGST_AMOUNT_N;

	public int getGST_CATEGORY_ID_N() {
		return GST_CATEGORY_ID_N;
	}
	public void setGST_CATEGORY_ID_N(int intGstCategoryId)
	{
		GST_CATEGORY_ID_N=intGstCategoryId;
	}
	public String getPAYMENT_TYPE_VC() {
		return PAYMENT_TYPE_VC;
	}
	public void setPAYMENT_TYPE_VC(String strPAYMENT_TYPE_VC)
	{
		PAYMENT_TYPE_VC=strPAYMENT_TYPE_VC;
	}
	public int getINVOICE_TYPE_N() {
		return INVOICE_TYPE_N;
	}
	public void setINVOICE_TYPE_N(int intInvoiceType)
	{
		INVOICE_TYPE_N=intInvoiceType;
	}
	public int getSUBGROUP_ID() {
		return SUBGROUP_ID;
	}
	public void setSUBGROUP_ID(int intSubGroupId)
	{
		SUBGROUP_ID=intSubGroupId;
	}
	public int getSHIPTO_ID_N() {
		return SHIPTO_ID_N;
	}
	public void setSHIPTO_ID_N(int intShipToId)
	{
		SHIPTO_ID_N=intShipToId;
	}
	public String getINVOICE_DATE_D() {
		return INVOICE_DATE_D;
	}
	public void setINVOICE_DATE_D(String InvoiceDate) {
		 INVOICE_DATE_D=InvoiceDate;
	}
	public double getINVOICE_NO_N() {
		return INVOICE_NO_N;
	}
	public void setINVOICE_NO_N(double dblInvoiceNo) {
		INVOICE_NO_N=dblInvoiceNo;
	}
	public double getTOTAL_AMOUNT_N() {
		return TOTAL_AMOUNT_N;
	}
	public void setTOTAL_AMOUNT_N(double dblTotalAmount) {
		TOTAL_AMOUNT_N=dblTotalAmount;
	}
	public double getDISC_PERCENT_N() {
		return DISC_PERCENT_N;
	}
	public void setDISC_PERCENT_N(double dblDiscPercent) {
		DISC_PERCENT_N=dblDiscPercent;
	}
	public double getDISC_AMOUNT_N() {
		return DISC_AMOUNT_N;
	}
	public void setDISC_AMOUNT_N(double dblDiscAmount) {
		DISC_AMOUNT_N=dblDiscAmount;
	}
	
	public double getCGST_AMOUNT_N() {
			return CGST_AMOUNT_N;
	}
	public void setCGST_AMOUNT_N(double dblcgstAmount) {
		CGST_AMOUNT_N=dblcgstAmount;
	}
	public double getCGST_PERCENT_N() {
		return CGST_PERCENT_N;
	}
	public void setCGST_PERCENT_N(double dblCgstPercent) {
		CGST_PERCENT_N=dblCgstPercent;
	}
	public double getSGST_AMOUNT_N() {
		return SGST_AMOUNT_N;
	}
	public void setSGST_AMOUNT_N(double dblSgstAmount) {
		SGST_AMOUNT_N=dblSgstAmount;
	}
	public double getSGST_PERCENT_N() {
		return SGST_PERCENT_N;
	}
	public void setSGST_PERCENT_N(double dblSgstPercent) {
		SGST_PERCENT_N=dblSgstPercent;
	}
	public double getCARTAGE_N() {
		return CARTAGE_N;
	}
	public void setCARTAGE_N(double dblCartage) {
		CARTAGE_N=dblCartage;
	}
	public double getROUND_OFF_PERCENT_N() {
		return ROUND_OFF_PERCENT_N;
	}
	public void setROUND_OFF_PERCENT_N(double dblRoundOffPercent) {
		ROUND_OFF_PERCENT_N=dblRoundOffPercent;
	}
	public double getROUND_OFF_AMOUNT_N() {
		return ROUND_OFF_AMOUNT_N;
	}
	public void setROUND_OFF_AMOUNT_N(double dblRoundOffAmont) {
		ROUND_OFF_AMOUNT_N=dblRoundOffAmont;
	}
	public double getGRAND_TOTAL_N() {
		return GRAND_TOTAL_N;
	}
	public void setGRAND_TOTAL_N(double dblGrandTotal) {
		GRAND_TOTAL_N=dblGrandTotal;
	}
	public double getIGST_PERCENT_N() {
		return IGST_PERCENT_N;
	}
	public void setIGST_PERCENT_N(double dblIgstPercent) {
		IGST_PERCENT_N=dblIgstPercent;
	}
	public double getIGST_AMOUNT_N() {
		return IGST_AMOUNT_N;
	}
	public void setIGST_AMOUNT_N(double dblIgstAmount) {
		IGST_AMOUNT_N=dblIgstAmount;
	}
}
