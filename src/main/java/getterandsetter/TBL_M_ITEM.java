package getterandsetter;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class TBL_M_ITEM {


	public int M_ITEM_ID;
	
	public int PRODUCT_TYPE_N;
	
	public int USER_ID_N;

	public int M_ITEMGROUPID;
	@Min(value = 1, message = "M_GST_CATID")
	public int M_GST_CATID;
	@NotNull(message = "ITEM_NAME_VC")
	public String ITEM_NAME_VC;
	
	public String ITEM_DESC_VC;
	
	public String HSN_CODE_VC;
	
	public String CREATE_DATE_D;

	public String MODIFY_DATE_D;
	@NotNull(message = "UNIT_NAME_VC")
	public String UNIT_NAME_VC;
	

	public double SALE_RATE_DECI;
	
	public double SALE_DISC_D;
	
	public double PURC_RATE_D;
	
	public double OPEN_QTY_D;
	
	public double MAX_LEVEL_D;
	
	public double MIN_LEVEL_D;
	
	public double REORDER_LEVEL_D;

	public double PURC_DIS_D;

	public double getPURC_DIS_D() {
		return PURC_DIS_D;
	}

	public void setPURC_DIS_D(double pURC_DIS_D) {
		PURC_DIS_D = pURC_DIS_D;
	}

	public String getUNIT_NAME_VC() {
		return UNIT_NAME_VC;
	}

	public void setUNIT_NAME_VC(String uNIT_NAME_VC) {
		UNIT_NAME_VC = uNIT_NAME_VC;
	}

	public TBL_M_ITEM() {

	}

	public int getM_ITEM_ID() {
		return M_ITEM_ID;
	}

	public void setM_ITEM_ID(int m_ITEM_ID) {
		M_ITEM_ID = m_ITEM_ID;
	}

	public int getPRODUCT_TYPE_N() {
		return PRODUCT_TYPE_N;
	}

	public void setPRODUCT_TYPE_N(int pRODUCT_TYPE_N) {
		PRODUCT_TYPE_N = pRODUCT_TYPE_N;
	}

	public int getUSER_ID_N() {
		return USER_ID_N;
	}

	public void setUSER_ID_N(int uSER_ID_N) {
		USER_ID_N = uSER_ID_N;
	}

	public int getM_ITEMGROUPID() {
		return M_ITEMGROUPID;
	}

	public void setM_ITEMGROUPID(int m_ITEMGROUPID) {
		M_ITEMGROUPID = m_ITEMGROUPID;
	}

	public int getM_GST_CATID() {
		return M_GST_CATID;
	}

	public void setM_GST_CATID(int m_GST_CATID) {
		M_GST_CATID = m_GST_CATID;
	}

	public String getITEM_NAME_VC() {
		return ITEM_NAME_VC;
	}

	public void setITEM_NAME_VC(String iTEM_NAME_VC) {
		ITEM_NAME_VC = iTEM_NAME_VC;
	}

	public String getITEM_DESC_VC() {
		return ITEM_DESC_VC;
	}

	public void setITEM_DESC_VC(String iTEM_DESC_VC) {
		ITEM_DESC_VC = iTEM_DESC_VC;
	}

	public String getHSN_CODE_VC() {
		return HSN_CODE_VC;
	}

	public void setHSN_CODE_VC(String hSN_CODE_VC) {
		HSN_CODE_VC = hSN_CODE_VC;
	}

	public String getCREATE_DATE_D() {
		return CREATE_DATE_D;
	}

	public void setCREATE_DATE_D(String cREATE_DATE_D) {
		CREATE_DATE_D = cREATE_DATE_D;
	}

	public String getMODIFY_DATE_D() {
		return MODIFY_DATE_D;
	}

	public void setMODIFY_DATE_D(String mODIFY_DATE_D) {
		MODIFY_DATE_D = mODIFY_DATE_D;
	}

	public double getSALE_RATE_DECI() {
		return SALE_RATE_DECI;
	}

	public void setSALE_RATE_DECI(double sALE_RATE_DECI) {
		SALE_RATE_DECI = sALE_RATE_DECI;
	}

	public double getSALE_DISC_D() {
		return SALE_DISC_D;
	}

	public void setSALE_DISC_D(double sALE_DISC_D) {
		SALE_DISC_D = sALE_DISC_D;
	}

	public double getPURC_RATE_D() {
		return PURC_RATE_D;
	}

	public void setPURC_RATE_D(double pURC_RATE_D) {
		PURC_RATE_D = pURC_RATE_D;
	}

	public double getOPEN_QTY_D() {
		return OPEN_QTY_D;
	}

	public void setOPEN_QTY_D(double oPEN_QTY_D) {
		OPEN_QTY_D = oPEN_QTY_D;
	}

	public double getMAX_LEVEL_D() {
		return MAX_LEVEL_D;
	}

	public void setMAX_LEVEL_D(double mAX_LEVEL_D) {
		MAX_LEVEL_D = mAX_LEVEL_D;
	}

	public double getMIN_LEVEL_D() {
		return MIN_LEVEL_D;
	}

	public void setMIN_LEVEL_D(double mIN_LEVEL_D) {
		MIN_LEVEL_D = mIN_LEVEL_D;
	}

	public double getREORDER_LEVEL_D() {
		return REORDER_LEVEL_D;
	}

	public void setREORDER_LEVEL_D(double rEORDER_LEVEL_D) {
		REORDER_LEVEL_D = rEORDER_LEVEL_D;
	}
	public ArrayList checkvalidate(TBL_M_ITEM t1)
	{
	
		ArrayList al=new ArrayList();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		TBL_M_ITEM t=new TBL_M_ITEM();
		t.setITEM_NAME_VC(t1.getITEM_NAME_VC());
		t.setHSN_CODE_VC(t1.getHSN_CODE_VC());
		t.setM_GST_CATID(t1.getM_GST_CATID());
	t.setUNIT_NAME_VC(t1.getUNIT_NAME_VC());
		Set<ConstraintViolation<TBL_M_ITEM>> violations = validator.validate(t);
		for (ConstraintViolation<TBL_M_ITEM> violation : violations) {
		    al.add(violation.getMessage()); 
		}
		
		return al;}

}