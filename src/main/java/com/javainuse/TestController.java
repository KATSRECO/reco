package com.javainuse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Dal.gst.gstAllMethods;
import Dal.Bank.allBankMethod;
import Dal.Saleinvoice.*;
import Dal.fillCombo.*;

import Dal.itemGroup.funtionGrpItem;

import Dal.itemMaster.itemmasterallmethod;
import Dal.partywise.partyWiseAllMethods;
import Dal.paymentto.paymenttoallmethods;
import Dal.recieptto.reciptAllMethods;
import Dal.subgroup.Insert_Ledger_Reg;
import Dal.userMaster.userMasterallmethod;
import Util.utils;
import Validator.itemgroupvalidate;
import Validator.itemmastervalidator;
import getterandsetter.TBL_M_ITEM;
import getterandsetter.TBL_M_ITEMGROUP;
import getterandsetter.TBL_M_SUBGROUP;
import getterandsetter.TBL_T_RECEIPT_FOR;
import getterandsetter.gst_SettingEntry;
import getterandsetter.pagination;
import getterandsetter.paymentto;
import getterandsetter.userMaster;

@Controller
public class TestController {
	gst_SettingEntry gstgg = new gst_SettingEntry();
	
	userMasterallmethod user=new userMasterallmethod();
	userMaster userge =new userMaster();

	gstAllMethods gstm = new gstAllMethods();
	partyWiseAllMethods pwa = new partyWiseAllMethods();
	saleinvoiceallmethods objsale = new saleinvoiceallmethods();
	pagination page=new pagination();
	fillCombo objfill = new fillCombo();
	itemgroupvalidate itemvalid;
	TBL_M_ITEM itemgetter = new TBL_M_ITEM();
	utils uti=new utils();
	TBL_M_ITEMGROUP tbitem = new TBL_M_ITEMGROUP();
	itemmasterallmethod itm = new itemmasterallmethod();
	String json=null;
	allBankMethod bank=new allBankMethod();
	paymentto payment = new paymentto();
	paymenttoallmethods paymentmethod = new paymenttoallmethods();
	itemmastervalidator im;
	itemmasterallmethod it = new itemmasterallmethod();
	funtionGrpItem itemgrpmethod = new funtionGrpItem();
	@SuppressWarnings("rawtypes")
	ArrayList al;
	TBL_T_RECEIPT_FOR recfor = new TBL_T_RECEIPT_FOR();
	reciptAllMethods rs = new reciptAllMethods();

	@RequestMapping("/")

	public String firstPage() {

		return "Itemgrpentry2";
	}

	@RequestMapping("/ledgerEntry")
	
	public String ledgerentry() {

		return "ledgerEntry";
	}

	@RequestMapping("/payemnt")

	public String paymentto() {

		return "paymentto";
	}

	@RequestMapping("/receipt")

	public String receipt() {

		return "Recieptfor";
	}

	@RequestMapping("/item")

	public String thirdpage() {

		return "itemMaster1";
	}

	@RequestMapping("/gst")

	public String gstPage() {

		return "gstView";
	}

	@RequestMapping("/gstreg")

	public String gstreg() {

		return "gstView";
	}

	@RequestMapping("/party")

	public String party() {

		return "partyWiseRate";
	}

	@RequestMapping("/test")

	public String testPage() {

		return "NewFile";
	}

	@RequestMapping(value = "/getgstlistforedit", params = { "id" }, method = RequestMethod.GET)
	public @ResponseBody String getgstlistforedit(@RequestParam("id") int id) throws IOException {
		gst_SettingEntry g = new gst_SettingEntry();
		g.setGstid(id);
		gstAllMethods gs = new gstAllMethods();
		String s3 = utils.correct(gs.getgstnamebyid(g));

		return s3;
	}

	@RequestMapping(value = "/getitemmasterlist", method = RequestMethod.GET)
	public @ResponseBody String getitemmasterlist() throws IOException {

		String s3 = it.getitemmasterjson();

		return s3;
	}

	@RequestMapping(value = "/getgstlist", method = RequestMethod.GET)
	public @ResponseBody String getgst() throws IOException {
		gstAllMethods gt = new gstAllMethods();
		String s3 = gt.getgstjson();

		return s3;
	}
	@RequestMapping(value = "/createlogin",params = {"uSername","password","conpassword"}, method = RequestMethod.GET)
	public @ResponseBody String checkavailability(@RequestParam("uSername")String username,@RequestParam("password")String password,@RequestParam("conpassword")String conpassword) throws IOException {
		
	
System.out.println(username+password+conpassword);
		return username;
	}

	/*
	 * Trying message @RequestMapping(value = "/profile", method =
	 * RequestMethod.GET) public @ResponseBody String processAJAXRequest(
	 * 
	 * @RequestParam("firstname") String firstname,
	 * 
	 * @RequestParam("lastname") String lastname ) { String response = "sasasass";
	 * 
	 * return response; }
	 */

	@RequestMapping(value = "/itemmasteredit", params = { "id", "checked", "itemname", "itemdesc", "unitName",
			"hsnCode", "itemgrp", "gstcat", "salerate", "salediscount", "purchaserate", "purchasediscount", "openqty",
			"max", "min", "record" }, method = RequestMethod.POST)

	public @ResponseBody String itemmasteredit(@RequestParam("id") int id, @RequestParam("checked") int checked,
			@RequestParam("itemname") String itemname, @RequestParam("itemdesc") String itemdesc,
			@RequestParam("unitName") String unitName, @RequestParam("hsnCode") String hsnCode,
			@RequestParam("itemgrp") int itemgrp, @RequestParam("gstcat") int gstcat,
			@RequestParam("salerate") double salerate, @RequestParam("salediscount") double salediscount,
			@RequestParam("purchaserate") double purchaserate, @RequestParam("purchasediscount") float purchasediscount,
			@RequestParam("openqty") float openqty, @RequestParam("max") float max, @RequestParam("min") float min,
			@RequestParam("record") float record) throws IOException {
		if (itemname.equalsIgnoreCase("")) {

			itemname = null;
		}
		if (unitName.equalsIgnoreCase("")) {

			unitName = null;
		}

		itemgetter.setM_ITEM_ID(id);

		itemgetter.setPRODUCT_TYPE_N(checked);
		itemgetter.setITEM_NAME_VC(itemname);
		itemgetter.setITEM_DESC_VC(itemdesc);
		itemgetter.setUNIT_NAME_VC(unitName);
		itemgetter.setHSN_CODE_VC(hsnCode);
		itemgetter.setM_ITEMGROUPID(itemgrp);
		itemgetter.setM_GST_CATID(gstcat);
		itemgetter.setSALE_RATE_DECI(salerate);
		itemgetter.setSALE_DISC_D(salediscount);
		itemgetter.setPURC_RATE_D(purchaserate);
		itemgetter.setPURC_DIS_D(purchasediscount);
		itemgetter.setOPEN_QTY_D(openqty);
		itemgetter.setMAX_LEVEL_D(max);
		itemgetter.setMIN_LEVEL_D(min);
		itemgetter.setREORDER_LEVEL_D(record);

		al = itemgetter.checkvalidate(itemgetter);

		if (al.toString().equalsIgnoreCase("[]")) {
			itm.edititemmaster(itemgetter);

		}
		return al.toString();

	}

	@RequestMapping(value = "/itemmasteradd", params = { "checked", "itemname", "itemdesc", "unitName", "hsnCode",
			"itemgrp", "gstcat", "salerate", "salediscount", "purchaserate", "purchasediscount", "openqty", "max",
			"min", "record" }, method = RequestMethod.POST)

	public @ResponseBody String itemmasteradd(@RequestParam("checked") int checked,
			@RequestParam("itemname") String itemname, @RequestParam("itemdesc") String itemdesc,
			@RequestParam("unitName") String unitName, @RequestParam("hsnCode") String hsnCode,
			@RequestParam("itemgrp") int itemgrp, @RequestParam("gstcat") int gstcat,
			@RequestParam("salerate") double salerate, @RequestParam("salediscount") double salediscount,
			@RequestParam("purchaserate") double purchaserate, @RequestParam("purchasediscount") float purchasediscount,
			@RequestParam("openqty") float openqty, @RequestParam("max") float max, @RequestParam("min") float min,
			@RequestParam("record") float record) throws IOException, IllegalArgumentException, IllegalAccessException {
		long startTime = System.currentTimeMillis();

		if (itemname.equalsIgnoreCase("")) {

			itemname = null;
		}

		if (unitName.equalsIgnoreCase("")) {

			unitName = null;
		}

		if (hsnCode.equalsIgnoreCase("")) {

			hsnCode = null;

		}

		itemgetter.setPRODUCT_TYPE_N(checked);
		itemgetter.setITEM_NAME_VC(itemname);
		itemgetter.setITEM_DESC_VC(itemdesc);
		itemgetter.setUNIT_NAME_VC(unitName);
		itemgetter.setHSN_CODE_VC(hsnCode);
		itemgetter.setM_ITEMGROUPID(itemgrp);
		itemgetter.setM_GST_CATID(gstcat);
		itemgetter.setSALE_RATE_DECI(salerate);
		itemgetter.setSALE_DISC_D(salediscount);
		itemgetter.setPURC_RATE_D(purchaserate);
		itemgetter.setPURC_DIS_D(purchasediscount);
		itemgetter.setOPEN_QTY_D(openqty);
		itemgetter.setMAX_LEVEL_D(max);
		itemgetter.setMIN_LEVEL_D(min);
		itemgetter.setREORDER_LEVEL_D(record);

		al = itemgetter.checkvalidate(itemgetter);

		if (al.toString().equalsIgnoreCase("[]")) {
			itm.sendtodbitemmaster(itemgetter);
			long endTime = System.currentTimeMillis();

			System.out.println("That took " + (endTime - startTime) + " milliseconds");
		}
		;
		return al.toString();
	}

	@RequestMapping(value = "/deletitemmaster", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody void deleteItemMaster(@RequestParam("id") int id) throws IOException {

		itemgetter.setM_ITEM_ID(id);
		itm.deleteitemmaster(itemgetter);

	}

	@RequestMapping(value = "/itemMasteradd", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody void addItemMaster(@RequestParam("id") int id) throws IOException {
		TBL_M_ITEMGROUP t = new TBL_M_ITEMGROUP(id);
		funtionGrpItem fg = new funtionGrpItem();
		fg.deleteById(t);

	}

	@RequestMapping(value = "/ItemMasterlistforedit", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody String ItemMasterlistforedit(@RequestParam("id") int id) throws IOException {

		itemgetter.setM_ITEM_ID(id);
		String name = utils.correct(itm.edititemmastergetlist(itemgetter));

		return name;
	}

	/*
	 * @RequestMapping(value = "/gstView", method = RequestMethod.GET)
	 * 
	 * public @ResponseBody String gstView() throws IOException {
	 * System.out.println("success"); gstAllMethods gs = new gstAllMethods(); String
	 * json = utils.correct(gs.getgstjson());
	 * 
	 * return "";
	 * 
	 * }
	 */

	@RequestMapping(value = "/getitemmasterlist", params = { "id" }, method = RequestMethod.GET)

	public @ResponseBody String gstView(@RequestParam("id") int id) throws IOException {
		System.out.println(id);
		String everything = it.getitemmasterjson(id);
		System.out.println(everything);

		return everything;

	}

	@RequestMapping(value = "/gstDelete", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody String deleteGst(@RequestParam("id") int id) throws IOException {

		gstgg.setGstid(id);
		gstm.deletegst(gstgg);

		return "";
	}

	@RequestMapping(value = "/gstlistedit", params = { "id", "gstdesc", "gstper" }, method = RequestMethod.POST)

	public @ResponseBody String gstedit(@RequestParam("id") int id, @RequestParam("gstdesc") String gstdesc,
			@RequestParam("gstper") int gstper) throws IOException {

		gstgg.setGstid(id);
		gstgg.setGstdesc(gstdesc);
		gstgg.setGstPercent(gstper);
		gstm.editgst(gstgg);

		return "success";

	}

	@RequestMapping(value = "/gstadddata", params = { "desc", "gst" }, method = RequestMethod.POST)

	public @ResponseBody String gstadd(@RequestParam("desc") String gstdesc, @RequestParam("gst") int gstper)
			throws IOException {
		System.out.println("success");
		gstgg.setGstdesc(gstdesc);
		gstgg.setGstPercent(gstper);
		gstm.sendtodbgst(gstgg);

		return "success";

	}

	// ALL ITEMGROUP METHOD NAME

	@RequestMapping(value = "/ajaxDeleteGrpName", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody String deleteGrpEntry(@RequestParam("id") int id)
			throws IOException, IllegalArgumentException, IllegalAccessException {
		TBL_M_ITEMGROUP t = new TBL_M_ITEMGROUP(id);

		itemgrpmethod.deleteById(t);
		return al.toString();

	}

	@RequestMapping(value = "/JqueryServlet", method = RequestMethod.GET)
	public @ResponseBody String sendEditgrp() throws IOException {
System.out.println("num");
		String s3 = itemgrpmethod.getitemgrpentryjson(50);

		System.out.println("this is" + s3);

		return s3;
	}

	@RequestMapping(value = "/AjaxServlet", params = { "message" }, method = RequestMethod.POST)

	public @ResponseBody String createGrpEntry(@RequestParam("message") String itemname) throws IOException {
		long startTime = System.currentTimeMillis();
		if (itemname.equalsIgnoreCase("")) {
			itemname = null;
		}

		tbitem.setItemname(itemname);
		@SuppressWarnings("rawtypes")
		ArrayList al = tbitem.checkvalidate(tbitem);

		if (al.toString().equalsIgnoreCase("[]")) {

			itemgrpmethod.sendtodb(tbitem);
			long endTime = System.currentTimeMillis();

			System.out.println("That took " + (endTime - startTime) + " milliseconds");
		}
		return al.toString();

	}

	@RequestMapping(value = "/editloadname", params = { "id" }, method = RequestMethod.GET)
	public @ResponseBody String Editgrpname(@RequestParam("id") int id) throws IOException {
		TBL_M_ITEMGROUP t = new TBL_M_ITEMGROUP(id);

		String s1 = utils.correct(itemgrpmethod.getitemnamebyid(t));

		return s1;

	}

	@RequestMapping(value = "/ajaxEditGrpName", params = { "name", "id" }, method = RequestMethod.POST)

	public @ResponseBody String editGrpEntry(@RequestParam("name") String itemname, @RequestParam("id") int id)
			throws IOException, IllegalArgumentException, IllegalAccessException {
		if (itemname.equalsIgnoreCase("")) {
			itemname = null;
		}

		tbitem.setId(id);
		tbitem.setItemname(itemname);

		al = tbitem.checkvalidate(tbitem);

		if (al.toString().equalsIgnoreCase("[]")) {
			itemgrpmethod.editGroup(tbitem);
		}
		return al.toString();

	}

	// END HERE

	@RequestMapping(value = "/fillComboPartyCustomer", method = RequestMethod.GET)
	public @ResponseBody String fillComboPartyCustomer() throws IOException {
		String strParty = objfill.fillComboPartyCustomer();
		return strParty;
	}

	@RequestMapping(value = "/loadpartywiseRatelist", params = { "id" }, method = RequestMethod.POST)

	public @ResponseBody String loadpartywiseRatelist(@RequestParam("id") int id) throws IOException {

		String json = utils.correct(pwa.partywisegetrate(id));
		return json;

	}

	@RequestMapping(value = "/bulkinsertwiseRatelist", params = { "json", "id" }, method = RequestMethod.POST)

	public @ResponseBody String sendbulkdata(@RequestParam("json") String json, @RequestParam("id") int id)
			throws IOException {
		String s2 = json.replaceAll("\\s+", "");
		pwa.bulkinsertdata(id, s2);

		return "as";

	}
	@RequestMapping(value = "/paymentdelete", params = {"id"}, method = RequestMethod.POST)

	public @ResponseBody String paymentdelete( @RequestParam("id") int id)
			throws IOException {
	payment.setV_NO_N(id);
json=paymentmethod.Paymenttdelete(payment);
		return json;

	}


	@RequestMapping(value = "/paymentto", params = { "checked", "partyname", "voucher", "date", "amount", "bankname",
			"checknum", "checkdate", "naration" }, method = RequestMethod.POST)

	public @ResponseBody String paymenttodb(@RequestParam("checked") String checked,
			@RequestParam("partyname") int partyid, @RequestParam("voucher") double voucher,
			@RequestParam("date") String date, @RequestParam("amount") double amount,
			@RequestParam("bankname") String bankname, @RequestParam("checknum") String checknum,
			@RequestParam("checkdate") String checkdate, @RequestParam("naration") String naration) throws IOException {
		String checknum1 = utils.filldata(checknum);
		String checkdate1 = utils.filldata(checkdate);
		String bankname1 = utils.filldata(bankname);
		payment.setMODE_N(checked);
		payment.setSUBGROUP_ID_N(partyid);
		payment.setV_NO_N(voucher);
		payment.setAMOUNT_N(amount);

		payment.setBANK_vc(bankname1);
		;
		payment.setV_DATE_D(date);
		payment.setCHEQUE_DATE_VC(checkdate1);
		payment.setCHEQUE_NO_VC(checknum1);
		payment.setNARRATION_VC(naration);
		paymentmethod.sendtodbpaymentto(payment);
		return "as";

	}
	
	
	@RequestMapping(value = "/paymenttype", params = { "checked", "partyname", "voucher", "date", "amount", "bankname",
			"checknum", "checkdate", "naration" }, method = RequestMethod.POST)

	public @ResponseBody String paymenttype(@RequestParam("checked") String checked,
			@RequestParam("partyname") int partyid, @RequestParam("voucher") double voucher,
			@RequestParam("date") String date, @RequestParam("amount") double amount,
			@RequestParam("bankname") String bankname, @RequestParam("checknum") String checknum,
			@RequestParam("checkdate") String checkdate, @RequestParam("naration") String naration) throws IOException {
		String checknum1 = utils.filldata(checknum);
		String checkdate1 = utils.filldata(checkdate);
		String bankname1 = utils.filldata(bankname);
		payment.setMODE_N(checked);
		payment.setSUBGROUP_ID_N(partyid);
		payment.setV_NO_N(voucher);
		payment.setAMOUNT_N(amount);

		payment.setBANK_vc(bankname1);
		;
		payment.setV_DATE_D(date);
		payment.setCHEQUE_DATE_VC(checkdate1);
		payment.setCHEQUE_NO_VC(checknum1);
		payment.setNARRATION_VC(naration);
		paymentmethod.sendtodbpaymentto(payment);
		return "as";

	}
	

	@RequestMapping(value = "/getdatapayment",params = { "id" }, method = RequestMethod.GET)

	public @ResponseBody String getdatapaymentto(@RequestParam("id")int id) throws IOException {
		System.out.println(id);
		json=paymentmethod.paymentForlist(id);
		
		return json;

	}
	
	
	@RequestMapping(value = "/getdatapaymentype",params = { "id" }, method = RequestMethod.GET)

	public @ResponseBody String getdatapaymentype(@RequestParam("id")int pagesize) throws IOException {
		page.setRange(pagesize);
	
		json=paymentmethod.paymentTypeList(page);
		return json;

	}
	
	
	
	@RequestMapping(value = "/getPaymentToEdit", params = { "V_NO_N" }, method = RequestMethod.GET)
	public @ResponseBody String getPaymentToEdit(@RequestParam("V_NO_N") int V_NO_N) throws IOException {
		String name = utils.correct(paymentmethod.PaymentForGet(V_NO_N));
		System.out.println(name);
		return name;
	}
	@RequestMapping(value = "/paymentedit", params = { "id","checked", "partyname", "voucher", "date", "amount", "bankname",
			"checknum", "checkdate", "naration" }, method = RequestMethod.POST)

	public @ResponseBody String paymenttoedit(@RequestParam("id") int id,@RequestParam("checked") String checked,
			@RequestParam("partyname") int partyid, @RequestParam("voucher") double voucher,
			@RequestParam("date") String date, @RequestParam("amount") double amount,
			@RequestParam("bankname") String bankname, @RequestParam("checknum") String checknum,
			@RequestParam("checkdate") String checkdate, @RequestParam("naration") String naration) throws IOException {
	payment.setId(id);
	System.out.println("asas");
		payment.setMODE_N(checked);
		payment.setSUBGROUP_ID_N(partyid);
		payment.setV_NO_N(voucher);
		payment.setAMOUNT_N(amount);

		payment.setBANK_vc(bankname);
		
		payment.setV_DATE_D(date);
		payment.setCHEQUE_DATE_VC(checkdate);
		payment.setCHEQUE_NO_VC(checknum);
		payment.setNARRATION_VC(naration);
		
		
		String url=paymentmethod.Paymenttoedit(payment);
	
		
		
		return url;

	}

	@RequestMapping(value = "/Receiptfortodb", params = { "checked", "partyname", "voucher", "date", "amount", "CardNo",
			"Transactionref", "Transactiondate", "naration" }, method = RequestMethod.POST)

	public @ResponseBody String Receiptfordb(@RequestParam("checked") int checked,
			@RequestParam("partyname") int partyid, @RequestParam("voucher") double voucher,
			@RequestParam("date") String date, @RequestParam("amount") double amount,
			@RequestParam("CardNo") String cardnumber, @RequestParam("Transactionref") String transactionref,
			@RequestParam("Transactiondate") String transactiondate, @RequestParam("naration") String naration)
			throws IOException {
		recfor.setMODE_N(checked);
		recfor.setSUBGROUP_ID_N(partyid);

		recfor.setV_NO_N(voucher);
		recfor.setV_DATE_D(date);
		recfor.setAMOUNT_N(amount);
		recfor.setCARD_NO_VC(cardnumber);
		recfor.setTRANSACTION_REF_VC(transactionref);
		recfor.setTRANSACTION_DATE_D(transactiondate);
		recfor.setNARRATION_VC(naration);
		rs.sendtodbRECEIPTto(recfor);
		return "as";

	}

	@RequestMapping(value = "/getreceiptforlist", method = RequestMethod.GET)
	public @ResponseBody String getreceiptforlist() throws IOException {
		String receiptjson = utils.correct(rs.recieptForlist());
		return receiptjson;
	}

	@RequestMapping(value = "/ajaxledger", params = { "company_name_vc", "contact_person_vc", "opening_balance_n",
			"gst_no_vc", "credit_days_n", "address1_vc", "city_vc", "state_vc", "phone_no_vc", "mobile_no_vc",
			"email_id_vc", "remarks_vc", "op_bal_type_vc" }, method = RequestMethod.POST)

	public @ResponseBody String createLegEntry(@RequestParam("company_name_vc") String company_name_vc,
			@RequestParam("contact_person_vc") String contact_person_vc,
			@RequestParam("opening_balance_n") int opening_balance_n, @RequestParam("gst_no_vc") String gst_no_vc,
			@RequestParam("credit_days_n") int credit_days_n, @RequestParam("address1_vc") String address1_vc,
			@RequestParam("city_vc") String city_vc, @RequestParam("state_vc") String state_vc,
			@RequestParam("phone_no_vc") String phone_no_vc, @RequestParam("mobile_no_vc") String mobile_no_vc,
			@RequestParam("email_id_vc") String email_id_vc, @RequestParam("remarks_vc") String remarks_vc,
			@RequestParam("op_bal_type_vc") String op_bal_type_vc

	) throws IOException {

		TBL_M_SUBGROUP tc = new TBL_M_SUBGROUP();

		tc.setCOMPANY_NAME_VC(company_name_vc);
		tc.setCONTACT_PERSON_VC(contact_person_vc);
		tc.setOPENING_BALANCE_N(opening_balance_n);
		tc.setGST_NO_VC(gst_no_vc);
		tc.setCREDIT_DAYS_N(credit_days_n);
		tc.setADDRESS1_VC(address1_vc);
		tc.setCITY_VC(city_vc);
		tc.setSTATE_VC(state_vc);
		tc.setPHONE_NO_VC(phone_no_vc);
		tc.setMOBILE_NO_VC(mobile_no_vc);
		tc.setEMAIL_ID_VC(email_id_vc);
		tc.setREMARKS_VC(remarks_vc);
		tc.setREMARKS_VC(op_bal_type_vc);

		Insert_Ledger_Reg cr = new Insert_Ledger_Reg();

		cr.insertLedger(tc);

		return "hello";

	}
	
	@RequestMapping(value = "/banklist", method = RequestMethod.GET)

	public @ResponseBody String banklist() throws IOException {
		
		
json =bank.getBankList();
		
		return json;

	}

	
}
