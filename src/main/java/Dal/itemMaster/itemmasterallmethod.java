package Dal.itemMaster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import getterandsetter.TBL_M_ITEM;
import getterandsetter.TBL_M_ITEMGROUP;
import getterandsetter.gst_SettingEntry;
import getterandsetter.loadproperties;

public class itemmasterallmethod {
	String url;
	Client client;

	public itemmasterallmethod() {
		client=client.create();
		url=loadproperties.geturl();
		
	}
	// ('/insertItemMaster/:PRODUCT_TYPE_N/:ITEM_NAME_VC/:ITEM_DESC_VC/:HSN_CODE_VC/:M_ITEMGROUPID/:M_GST_CATID/:SALE_RATE_DECI/:SALE_DISC_D/:PURC_RATE_D/:OPEN_QTY_D/:MAX_LEVEL_D/:MIN_LEVEL_D/:REORDER_LEVEL_D/:USER_ID_N/'

	public void sendtodbitemmaster(TBL_M_ITEM t) {
		int id = 1;
		
		WebResource webResource = client.resource(url+"insertItemMaster/" + t.getPRODUCT_TYPE_N()
				+ "/" + t.getPURC_DIS_D() + "/" + t.getUNIT_NAME_VC() + "/" + t.getITEM_NAME_VC() + "/"
				+ t.getITEM_DESC_VC() + "/" + t.getHSN_CODE_VC() + "" + "/" + t.getM_ITEMGROUPID() + "" + "/"
				+ t.getM_GST_CATID() + "" + "/" + t.getSALE_RATE_DECI() + "" + "/" + t.getSALE_DISC_D() + "" + "/"
				+ t.getPURC_RATE_D() + "" + "/" + t.getOPEN_QTY_D() + "" + "/" + t.getMAX_LEVEL_D() + "" + "/"
				+ t.getMIN_LEVEL_D() + "" + "/" + t.getREORDER_LEVEL_D() + "/" + id + "");
		

		
String input = "";

 webResource.type("application/json").post(ClientResponse.class, input);
		

	}

	public void edititemmaster(TBL_M_ITEM t) {
		int id = 1;
		client=client.create();
		WebResource webResource = client.resource(url+"updateItemMaster/" + t.getM_ITEM_ID() + "/"
				+ t.getPRODUCT_TYPE_N() + "/" + t.getPURC_DIS_D() + "/" + t.getUNIT_NAME_VC() + "/"
				+ t.getITEM_NAME_VC() + "/" + t.getITEM_DESC_VC() + "/" + t.getHSN_CODE_VC() + "/"
				+ t.getM_ITEMGROUPID() + "/" + t.getM_GST_CATID() + "/" + t.getSALE_RATE_DECI() + "/"
				+ t.getSALE_DISC_D() + "/" + t.getPURC_RATE_D() + "/" + t.getOPEN_QTY_D() + "/" + t.getMAX_LEVEL_D()
				+ "/" + t.getMIN_LEVEL_D() + "/" + t.getREORDER_LEVEL_D() + "/" + id + "");
		
		String input = "";
		
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);

		

	}

	public void deleteitemmaster(TBL_M_ITEM t) {
		int id = 1;
		client=client.create();
		WebResource webResource = client.resource(url+"deleteItemMaster/" + t.getM_ITEM_ID() + "");
		
		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);

		

	}

	public String edititemmastergetlist(TBL_M_ITEM t) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url+"getItemForEdit/" + t.getM_ITEM_ID() + "");
		
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		
		while ((output = br.readLine()) != null) {
			name = output;
		}

		return name;

	}

	public String getitemmasterjson(int to) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url+"getItemList"+"/"+50+"/"+to+"");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	
		while ((output = br.readLine()) != null) {
			name = output;
		}
		
		return name;

	}

	
	
	public String getitemmasterjson() throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url+"getItemList");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	
		while ((output = br.readLine()) != null) {
			name = output;
		}
		
		return name;

	}

}
