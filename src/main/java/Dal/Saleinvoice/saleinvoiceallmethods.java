package Dal.Saleinvoice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class saleinvoiceallmethods {
	public static String UrlPrefix="http://localhost:8081";
	public String getsaleinvoicejson() throws IOException{
		String output;
		String name = null;
		URL url = new URL(UrlPrefix+"/getItemForFillCombo");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			name=output;
		}
		return name;
	}
	public String getMaxPurchaseRateItemWise(int intItemId) throws IOException{
		String output;
		String maxRate = null;
		URL url = new URL(UrlPrefix+"/getMaxPurchaseRate/"+intItemId);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			maxRate=output;
		}
		return maxRate;
	}
}
