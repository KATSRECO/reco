package Dal.Bank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import getterandsetter.loadproperties;

public class allBankMethod {
	String urlprefix;
public allBankMethod() {
	urlprefix=loadproperties.geturl();
}

	public String getBankList() throws IOException
	{
		String output;
		String name = null;
		URL url1 = new URL(urlprefix+"getFillBankList");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	
		while ((output = br.readLine()) != null) {
			name = output;
		}
		
		return name;

		
		
		
	}
	
	
}
