package Dal.fillCombo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import getterandsetter.loadproperties;

public class fillCombo {
	String url;
	public fillCombo() {
		url=loadproperties.geturl();
	}

	public String fillComboPartyCustomer () throws IOException{
		String output;
		String strPartyCustormer = null;
		URL url1 = new URL(url+"getFillComboPartyCustomer/");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			strPartyCustormer=output;
		}
		return strPartyCustormer;
	}
}
