package Dal.partywise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import getterandsetter.loadproperties;

public class partyWiseAllMethods {
	String UrlPrefix;
	public partyWiseAllMethods() {
		UrlPrefix=loadproperties.geturl();
	}
	int num=1;
	
	public String partywisegetrate (int id) throws IOException{
		String output;
		String strPartyCustormer = null;
		URL url = new URL(UrlPrefix+"getPartyWiseRate/"+id+"");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			strPartyCustormer=output;
		}
		return strPartyCustormer;
	}
	
	
	public String bulkinsertdata (int id,String json) throws IOException{
		String output;
		String strPartyCustormer = null;
		URL url = new URL(UrlPrefix+"insertPartyWiseRate/"+json+"/"+id+"/"+num+"");
		System.out.println(url);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			strPartyCustormer=output;
		}
		return strPartyCustormer;
	}
	
	
	
	
}
