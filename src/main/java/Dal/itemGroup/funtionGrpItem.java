package Dal.itemGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import java.net.URL;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import getterandsetter.TBL_M_ITEMGROUP;
import getterandsetter.loadproperties;

public class funtionGrpItem {
	Client client;
	String url;

	public funtionGrpItem() {
		client = Client.create();
		url = loadproperties.geturl();
		System.out.println("hgg");
	}

	public String getitemgrpentryjson(int number) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url + "getitemcategory"+"/"+50+"/"+number+"");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		
		while ((output = br.readLine()) != null) {
			name = output;
		}
	

		return name;

	}

	public String getitemnamebyid(TBL_M_ITEMGROUP t) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url + "getitemcategoryForEdit/" + t.getId() + "");
	
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		
		while ((output = br.readLine()) != null) {
			name = output;
		}
		
		return name;

	}

	public String deleteById(TBL_M_ITEMGROUP t) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url + "deleteitemcategory/" + t.getId() + "");
	
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		
		while ((output = br.readLine()) != null) {
			name = output;
		}
	
		return name;

	}

	public void editGroup(TBL_M_ITEMGROUP t) {
		client = Client.create();

		WebResource webResource = client.resource(url + "updateitemcategory/" + t.getItemname() + "/" + t.getId() + "");
		
		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
	}

	public void sendtodb(TBL_M_ITEMGROUP t) {

		
		WebResource webResource = client
				.resource(url + "insertitemcategory/" + t.getItemname()+ "");
		
		
		String input = "";

		 webResource.type("application/json").post(ClientResponse.class, input);
		 client.destroy();

	}

	
}
