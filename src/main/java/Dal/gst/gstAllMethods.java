package Dal.gst;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import getterandsetter.TBL_M_ITEMGROUP;
import getterandsetter.gst_SettingEntry;
import getterandsetter.loadproperties;

public class gstAllMethods {
	int id = 1;
	String url;

	public gstAllMethods() {
		
		url = loadproperties.geturl();
	}

	public void sendtodbgst(gst_SettingEntry g) {
		Client client = Client.create();

		WebResource webResource = client
				.resource(url + "insertGST/" + g.getGstdesc() + "/" + g.getGstPercent() + "/" + id + "");

		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);

	}

	public String getgstjson() throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url + "getGSTList");
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			name = output;
		}

		return name;

	}

	public String getgstnamebyid(gst_SettingEntry g) throws IOException {
		String output;
		String name = null;
		URL url1 = new URL(url + "getGSTForEdit/" + g.getGstid() + "");
		System.out.println(url);
		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			name = output;
		}

		return name;

	}

	public void editgst(gst_SettingEntry t) {

		Client client = Client.create();

		WebResource webResource = client.resource(
				url + "updateGST/" + t.getGstid() + "/" + t.getGstdesc() + "/" + t.getGstPercent() + "/" + id + "");
		System.out.println(webResource);
		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
	}

	public void deletegst(gst_SettingEntry t) {

		Client client = Client.create();

		WebResource webResource = client.resource(url + "deleteGST/" + t.getGstid() + "");

		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
	}

}
