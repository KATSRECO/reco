package Dal.paymentto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import Util.utils;
import getterandsetter.gst_SettingEntry;
import getterandsetter.loadproperties;
import getterandsetter.pagination;
import getterandsetter.paymentto;

public class paymenttoallmethods {
	int id = 1;
	String urlprefix;
	String param;
	String status;
	utils ut=new utils();
	public paymenttoallmethods() {
		urlprefix=loadproperties.geturl();
	}

	public void sendtodbpaymentto(paymentto p) {
		Client client = Client.create();

		WebResource webResource = client
				.resource(urlprefix+"insertPaymentTo/" + p.getSUBGROUP_ID_N() + "/" + p.getMODE_N() + "/"
						+ p.getBANK_vc() + "/" + p.getV_NO_N() + "/" + p.getCHEQUE_NO_VC() + "/" + p.getV_DATE_D() + "/"
						+ p.getCHEQUE_DATE_VC() + "/" + p.getAMOUNT_N() + "/" + p.getNARRATION_VC() + "/" + id);
		System.out.println(webResource);
		String input = "";

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);

		System.out.println(response);
	}

	public String paymentForlist(int pagesize) throws IOException {
		String output;
		String name = null;
		URL url = new URL(urlprefix+"getPaymetntToList/50/"+pagesize+"");
	
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			name = output;
		}
		return name;
	}

	public String PaymentForGet(int intVoucherNo) throws IOException {
		String output;
		String name = null;
		URL url = new URL(urlprefix+"getPaymetntTo/" + intVoucherNo);
		System.out.println(url);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			name = output;
		}

		return name;
	}
	//'/UpdatePaymentTo/:idPaymentTo/:SUBGROUP_ID_N/:MODE_VC/:BANK_ID_N/:V_NO_N/:CHEQUE_NO_VC/:V_DATE_D/:CHEQUE_DATE_VC/:AMOUNT_N/:NARRATION_VC/:USER_ID_N/
	public String Paymenttoedit(paymentto pay) throws IOException {
		 param="UpdatePaymentTo/"+pay.getId()+"/"+pay.getSUBGROUP_ID_N()+"/"+pay.getMODE_N()+"/"+pay.getBANK_vc()+"/"+pay.getV_NO_N()+"/"+pay.getCHEQUE_NO_VC()+"/"+pay.getV_DATE_D()+"/"+pay.getCHEQUE_DATE_VC()+"/"+pay.getAMOUNT_N()+"/"+pay.getNARRATION_VC()+"/"+1+"";
	System.out.println(param);
	 status=ut.postResponsefromServer(param);
	return status;
	}
	public String Paymenttdelete(paymentto pay) throws IOException {
		param="DeletePaymetntTo/"+pay.getV_NO_N()+"";

	 status=ut.postResponsefromServer(param);
	return status;
	}
	
public String paymentTypeList(pagination pay) throws IOException
{
	param="getPaymentTypeForList/"+pay.getSize()+"/"+pay.getRange();
	status=ut.getResponsefromServer(param);
	
	return status;
	


}
	
	
}
