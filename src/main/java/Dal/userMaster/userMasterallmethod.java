package Dal.userMaster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import getterandsetter.loadproperties;

public class userMasterallmethod {

	String UrlPrefix;
	public userMasterallmethod ()

	{
		
		UrlPrefix=loadproperties.geturl();
		
		
	}
	
	
	
	public String checkAvailability(String username) throws IOException
	{
		
		
		String output;
		String strPartyCustormer = null;
		URL url = new URL(UrlPrefix+"checkusernameavailable/"+username+"");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));
		while ((output = br.readLine()) != null) {
			strPartyCustormer=output;
		}
		return strPartyCustormer;
		
	}
	
	
}
